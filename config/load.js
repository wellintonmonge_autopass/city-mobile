const fs = require('fs');
const path = require('path');
const ReplaceValues = require('./ReplaceValues');

const rootDir = path.join(__dirname, '..');

const configFile = require('./config');

ReplaceValues.betweenBatch(
  `${rootDir}/android/app/src/main/AndroidManifest.xml`,
  [
    {
      left: 'package="',
      right: '"',
      value: configFile.appId,
    },
    {
      left: '<data android:host="',
      right: '" android:scheme="http"/>',
      value: configFile.host,
    },
    {
      left: '<data android:host="',
      right: '" android:scheme="https"/>',
      value: configFile.host,
    },
  ],
);

ReplaceValues.betweenBatch(`${rootDir}/android/app/BUCK`, [
  {
    left: 'package = "',
    right: '",',
    value: configFile.appId,
  },
]);

ReplaceValues.betweenBatch(
  `${rootDir}/android/app/src/main/res/values/strings.xml`,
  [
    {
      left: '<string name="app_name">',
      right: '</string>',
      value: configFile.appName,
    },
    {
      left: '<string name="version_code">',
      right: '</string>',
      value: configFile.buildNumber,
    },
    {
      left: '<string name="version_name">',
      right: '</string>',
      value: configFile.versionName,
    },
    {
      left: '<string name="facebook_app_id">',
      right: '</string>',
      value: configFile.facebook.id,
    },
    {
      left: '<string name="google_app_key">',
      right: '</string>',
      value: configFile.googleMaps[configFile.environment].androidKey,
    },
  ],
);

ReplaceValues.betweenBatch(`${rootDir}/android/app/build.gradle`, [
  {
    left: 'applicationId "',
    right: '"',
    value: configFile.appId,
  },
  {
    left: 'versionName "',
    right: '"',
    value: configFile.versionName,
  },
  {
    left: 'versionCode ',
    right: ' ',
    value: configFile.buildNumber,
  },
]);

ReplaceValues.jsonBatch(`${rootDir}/android/app/google-services.json`, [
  {
    key: 'client.client_info.android_client_info.package_name',
    value: configFile.appId,
  },
  {
    key: 'project_info.project_number',
    value: configFile.firebase[configFile.environment].project_number,
  },
  {
    key: 'project_info.firebase_url',
    value: configFile.firebase[configFile.environment].firebase_url,
  },
  {
    key: 'project_info.project_id',
    value: configFile.firebase[configFile.environment].project_id,
  },
  {
    key: 'project_info.storage_bucket',
    value: configFile.firebase[configFile.environment].storage_bucket,
  },
  {
    key: 'client[0].client_info.mobilesdk_app_id',
    value: configFile.firebase[configFile.environment].android_google_api_id,
  },
  {
    key: 'client[0].oauth_client[0].client_id',
    value: configFile.firebase[configFile.environment].android_client_id,
  },
  {
    key: 'client[0].oauth_client[0].client_type',
    value: configFile.firebase[configFile.environment].client_type,
  },
  {
    key: 'client[0].api_key[0].current_key',
    value: configFile.firebase[configFile.environment].android_api_key,
  },
  {
    key: 'client[0].client_info.android_client_info.package_name',
    value: configFile.appId,
  },
]);

ReplaceValues.jsonBatch(`${rootDir}/app.json`, [
  {
    key: 'name',
    value: configFile.appName,
  },
  {
    key: 'displayName',
    value: configFile.appName,
  },
]);

ReplaceValues.xmlBatch(`${rootDir}/android/.project`, [
  {
    path: 'projectDescription.name[0]',
    value: configFile.appName,
  },
]);

ReplaceValues.plistBatch(`${rootDir}/ios/GoogleService-Info.plist`, [
  {
    key: 'BUNDLE_ID',
    value: configFile.appId,
  },
  {
    key: 'DATABASE_URL',
    value: configFile.firebase[configFile.environment].firebase_url,
  },
  {
    key: 'CLIENT_ID',
    value: configFile.firebase[configFile.environment].ios_client_id,
  },
  {
    key: 'REVERSED_CLIENT_ID',
    value: configFile.firebase[configFile.environment].ios_reversed_client_id,
  },
  {
    key: 'API_KEY',
    value: configFile.firebase[configFile.environment].ios_api_key,
  },
  {
    key: 'GCM_SENDER_ID',
    value: configFile.firebase[configFile.environment].project_number,
  },
  {
    key: 'PROJECT_ID',
    value: configFile.firebase[configFile.environment].project_id,
  },
  {
    key: 'STORAGE_BUCKET',
    value: configFile.firebase[configFile.environment].storage_bucket,
  },
  {
    key: 'GOOGLE_APP_ID',
    value: configFile.firebase[configFile.environment].ios_google_api_id,
  },
]);

ReplaceValues.plistBatch(`${rootDir}/ios/CityMais/Info.plist`, [
  {
    key: 'CFBundleVersion',
    value: configFile.buildNumber,
  },
  {
    key: 'CFBundleDisplayName',
    value: configFile.appName,
  },
  {
    key: 'FacebookDisplayName',
    value: configFile.appName,
  },
  {
    key: 'NSLocationWhenInUseUsageDescription',
    value: `Autorize o acesso ao serviço de geolocalização do seu aparelho para que o ${
      configFile.appName
    } te direcione para o ponto de recarga mais próximo de você!`,
  },
  {
    key: 'FacebookAppID',
    value: configFile.facebook.id,
  },
  {
    key: 'CFBundleShortVersionString',
    value: configFile.versionName,
  },
  {
    key: 'CFBundleURLTypes[0].CFBundleURLSchemes[0]',
    value: configFile.facebook.urlTypeSchemes,
  },
]);

ReplaceValues.betweenBatch(`${rootDir}/ios/CityMais/AppDelegate.m`, [
  {
    left: 'deepLinkURLScheme = @"',
    right: '";',
    value: configFile.appId,
  },
  {
    left: '[GMSServices provideAPIKey:@"',
    right: '"];',
    value: configFile.googleMaps[configFile.environment].iosKey,
  },
]);

ReplaceValues.betweenBatch(
  `${rootDir}/ios/CityMais.xcodeproj/project.pbxproj`,
  [
    {
      left: 'PRODUCT_BUNDLE_IDENTIFIER = ',
      right: ';',
      value: configFile.appId,
    },
  ],
);

console.log('\x1b[36m', 'Settings loaded with success!');

// process.exit(1);
