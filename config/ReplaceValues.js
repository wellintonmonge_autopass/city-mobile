const fs = require('fs');
const pathManager = require('path');
const plist = require('plist');
const rootDir = pathManager.join(__dirname, '..');
const xml2js = require('xml2js');

class ReplaceValues {
  regexScape(str = '') {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }

  /**
   * @param {string} filePath - path to the file
   * @param {string} left - left argument to find something between
   * @param {string} right - right argument to find something between
   * @param {string} value - a value to replace with
   */
  between(filePath, left, right, value) {
    let file = fs.readFileSync(filePath).toString();
    const regex = new RegExp(
      `${this.regexScape(left)}(.*?)${this.regexScape(right)}`,
      'g',
    );
    const match = file.match(regex);
    let updateCount = 0;

    console.log('\x1b[0m', `[UPDATING] - ${filePath.replace(rootDir, '')}`);

    if (match) {
      if (typeof value === 'function') {
        file = file.replace(regex, `${left}${value(match[1])}${right}`);
        updateCount += 1;
      } else if (typeof value === 'string' || typeof value === 'number') {
        file = file.replace(regex, `${left}${value}${right}`);
        updateCount += 1;
      }
    }

    if (updateCount > 0) {
      fs.writeFileSync(filePath, file);
    }

    console.log('\x1b[35m', `Updated 1/${updateCount} line\n`);
  }

  /**
   * @param {string} filePath - path to the file
   * @param {Object[]} batchCmd - Array of object with values to the replacement
   * @param {string} batchCmd[].left - left argument to find something between
   * @param {string} batchCmd[].right - right argument to find something between
   * @param {string} batchCmd[].value - a value to replace with
   */
  betweenBatch(filePath, batchCmd) {
    let file = fs.readFileSync(filePath).toString();
    let updateCount = 0;

    console.log('\x1b[0m', `[UPDATING] - ${filePath.replace(rootDir, '')}`);

    batchCmd.forEach(cmds => {
      const match = file.match(
        new RegExp(
          `${this.regexScape(cmds.left)}(.*?)${this.regexScape(cmds.right)}`,
        ),
      );
      const regex = new RegExp(
        `${this.regexScape(cmds.left)}(.*?)${this.regexScape(cmds.right)}`,
        'g',
      );

      if (match) {
        // DEBUG - console.log(`:: ${cmds.left} ::`);
        if (typeof cmds.value === 'function') {
          file = file.replace(
            regex,
            `${cmds.left}${cmds.value(match[1])}${cmds.right}`,
          );
          updateCount += 1;
        } else if (
          typeof cmds.value === 'string' ||
          typeof cmds.value === 'number'
        ) {
          file = file.replace(regex, `${cmds.left}${cmds.value}${cmds.right}`);
          updateCount += 1;
        }
      }
    });

    if (updateCount > 0) {
      fs.writeFileSync(filePath, file);
    }

    console.log(
      '\x1b[35m',
      `Updated ${batchCmd.length}/${updateCount} lines\n`,
    );
  }

  /**
   * @param {string} filePath - path to the file
   * @param {Object[]} batchCmd - Array of object with values to the replacement
   * @param {string} batchCmd[].key - left argument to find something between
   * @param {string} batchCmd[].value - a value to replace with
   */
  jsonBatch(filePath, batchCmd) {
    let file = JSON.parse(fs.readFileSync(filePath));
    let updateCount = 0;

    console.log('\x1b[0m', `[UPDATING] - ${filePath.replace(rootDir, '')}`);

    batchCmd.forEach(cmds => {
      const valueFromKey = this._getObjectPropertieByString(file, cmds.key);

      if (typeof cmds.value === 'function') {
        file = this._changeObjectValueByStringKey(
          file,
          cmds.key,
          cmds.value(valueFromKey),
        );
        updateCount += 1;
      } else if (
        typeof cmds.value === 'string' ||
        typeof cmds.value === 'number'
      ) {
        file = this._changeObjectValueByStringKey(file, cmds.key, cmds.value);
        updateCount += 1;
      }
    });

    if (updateCount > 0) {
      fs.writeFileSync(filePath, JSON.stringify(file, null, 2));
    }

    console.log(
      '\x1b[35m',
      `Updated ${batchCmd.length}/${updateCount} lines\n`,
    );
  }

  _getObjectPropertieByString(object, path) {
    let splited = path
      .replace(/\[/g, '.')
      .replace(/\]/g, '')
      .replace(/\/\./g, ':>')
      .split('.');
    splited = splited.map(item => {
      return item.replace(/\:\>/g, '.');
    });
    let value = null;

    splited.forEach(keyPart => {
      if (object[keyPart]) {
        value = object[keyPart];
      }
    });

    return value;
  }

  _changeObjectValueByStringKey(object, path, value, replaceKey = false) {
    let splited = path
      .replace(/\[/g, '.')
      .replace(/\]/g, '')
      .replace(/\/\./g, ':>')
      .split('.');
    splited = splited.map(item => {
      return item.replace(/\:\>/g, '.');
    });

    let objVal = object;

    splited.forEach((keyPart, index) => {
      if (objVal[keyPart] !== undefined && objVal[keyPart] !== null) {
        if (index < splited.length - 1) {
          objVal = objVal[keyPart];
        } else if (splited.length - 1 === index) {
          if (replaceKey) {
            const handle = objVal[keyPart];
            delete objVal[keyPart];

            objVal[value] = handle;
          } else {
            objVal[keyPart] = value;
          }
        }
      }
    });

    return object;
  }

  /**
   * @param {string} filePath - path to the file
   * @param {Object[]} batchCmd - Array of object with values to the replacement
   * @param {string} batchCmd[].key - left argument to find something between
   * @param {string} batchCmd[].newKey - left argument to find something between
   * @param {string} batchCmd[].value - a value to replace with
   */
  plistBatch(filePath, batchCmd) {
    const file = fs.readFileSync(filePath).toString();
    const plistObj = plist.parse(file);
    let updateCount = 0;

    console.log('\x1b[0m', `[UPDATING] - ${filePath.replace(rootDir, '')}`);

    batchCmd.forEach(tag => {
      if (tag.newKey && !tag.value) {
        this._changeObjectValueByStringKey(plistObj, tag.key, tag.newKey, true);
      } else {
        this._changeObjectValueByStringKey(plistObj, tag.key, tag.value, false);
      }

      updateCount += 1;
    });

    const newPlist = plist.build(plistObj);
    fs.writeFileSync(filePath, newPlist);

    console.log(
      '\x1b[35m',
      `Updated ${batchCmd.length}/${updateCount} lines\n`,
    );
  }

  /**
   * @param {string} filePath - path to the file
   * @param {string} replace - left argument to find something between
   * @param {string} value - a value to replace with
   */
  replace(filePath, replace, value) {
    let file = fs.readFileSync(filePath).toString();

    console.log('\x1b[0m', `[UPDATING] - ${filePath.replace(rootDir, '')}`);

    const occurrences = file.match(
      new RegExp(`/${this.regexScape(replace)}/g`),
    );

    file = file.replace(new RegExp(`/${this.regexScape(replace)}/g`), value);

    if (occurrences > 0) {
      fs.writeFileSync(filePath, file);
    }

    console.log('\x1b[35m', `Updated ${occurrences} occurrences\n`);
  }

  /**
   * Replace a property from a xml file
   * @param {string} filePath - path to the file
   * @param {Object[]} batchCmd - Array of object with values to the replacement
   * @param {string} batchCmd[].path - the path to reach a certain property
   * @param {string} batchCmd[].value - a value to replace with
   */
  xmlBatch(filePath, batchCmd) {
    let xml = fs.readFileSync(filePath).toString();
    let updateCount = 0;

    console.log('\x1b[0m', `[UPDATING] - ${filePath.replace(rootDir, '')}`);

    xml2js.parseString(xml, (err, xmlObj) => {
      var builder = new xml2js.Builder();
      let newXmlObj = xmlObj;

      batchCmd.forEach(cmd => {
        newXmlObj = this._changeObjectValueByStringKey(
          newXmlObj,
          cmd.path,
          cmd.value,
        );
      });

      fs.writeFileSync(filePath, builder.buildObject(newXmlObj));
    });

    console.log('\x1b[35m', `Updated 1/1 occurrences\n`);
  }
}

module.exports = new ReplaceValues();
