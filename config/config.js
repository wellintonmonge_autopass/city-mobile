module.exports = {
  environment: 'prod',
  appName: 'City +',
  appId: 'br.com.autopass.citymais',
  appStoreUrl: 'itms://itunes.apple.com/br/app/id1448495091?mt=8',
  versionName: '1.2.0',
  buildNumber: '20',
  email: 'contato@citymais.com.br',
  api: {
    dev: {
      channel: 'GUARUJA',
      baseUrl: 'http://192.168.88.88:8082/voud',
    },
    hml: {
      channel: 'GUARUJA',
      baseUrl: 'https://homolog-api.voud.com.br:8084/voud',
    },
    prod: {
      channel: 'GUARUJA',
      baseUrl: 'https://api.voud.com.br:8084/voud',
    },
  },
  host: 'citymais.com.br',
  facebook: {
    id: '221507922075077',
    urlTypeSchemes: 'fb221507922075077',
  },
  firebase: {
    channel: {
      id: 'citymais-channel',
      name: 'CityMais channel',
    },
    dev: {
      project_number: '228713337501',
      firebase_url: 'https://autopass-cityplus-dev.firebaseio.com',
      storage_bucket: 'autopass-cityplus-dev.appspot.com',
      project_id: 'autopass-cityplus-dev',
      android_google_api_id: '1:228713337501:android:be463f367a5fc97c',
      ios_google_api_id: '1:228713337501:ios:be463f367a5fc97c',
      android_client_id:
        '228713337501-klsh2cvefe6fjva234ta11hbqfga7c4a.apps.googleusercontent.com',
      ios_client_id:
        '228713337501-2gclt9tjmrlu6805u051t45nqnf0e887.apps.googleusercontent.com',
      ios_reversed_client_id:
        'com.googleusercontent.apps.228713337501-2gclt9tjmrlu6805u051t45nqnf0e887',
      client_type: 3,
      android_api_key: 'AIzaSyC2xJMXtQp0gTRxI7wtKrsqiBFkH9RHyuU',
      ios_api_key: 'AIzaSyCbBT2TVbFRBO8AF_o6wnXhf3TqdC9EEBg',
    },
    hml: {
      project_number: '149434103518',
      firebase_url: 'https://autopass-cityplus.firebaseio.com',
      storage_bucket: 'autopass-cityplus.appspot.com',
      project_id: 'autopass-cityplus',
      android_google_api_id: '1:149434103518:android:be463f367a5fc97c',
      ios_google_api_id: '1:149434103518:ios:be463f367a5fc97c',
      android_client_id:
        '149434103518-anhkm0d5itmku2kcj0idi4koc88okceb.apps.googleusercontent.com',
      ios_client_id:
        '149434103518-ktvgrga79sdgmqk6c4mdq8ipqlho67dn.apps.googleusercontent.com',
      ios_reversed_client_id:
        'com.googleusercontent.apps.149434103518-ktvgrga79sdgmqk6c4mdq8ipqlho67dn',
      client_type: 3,
      android_api_key: 'AIzaSyA3Kz_S1Tq8oKQwPW-EwAsKzRYWzSyqYSg',
      ios_api_key: 'AIzaSyDgEVHB6nazJN-DAJ1D0RMvxCVSh3HNug8',
    },
    prod: {
      project_number: '149434103518',
      firebase_url: 'https://autopass-cityplus.firebaseio.com',
      storage_bucket: 'autopass-cityplus.appspot.com',
      project_id: 'autopass-cityplus',
      android_google_api_id: '1:149434103518:android:be463f367a5fc97c',
      ios_google_api_id: '1:149434103518:ios:be463f367a5fc97c',
      android_client_id:
        '149434103518-anhkm0d5itmku2kcj0idi4koc88okceb.apps.googleusercontent.com',
      ios_client_id:
        '149434103518-ktvgrga79sdgmqk6c4mdq8ipqlho67dn.apps.googleusercontent.com',
      ios_reversed_client_id:
        'com.googleusercontent.apps.149434103518-ktvgrga79sdgmqk6c4mdq8ipqlho67dn',
      client_type: 3,
      android_api_key: 'AIzaSyA3Kz_S1Tq8oKQwPW-EwAsKzRYWzSyqYSg',
      ios_api_key: 'AIzaSyDgEVHB6nazJN-DAJ1D0RMvxCVSh3HNug8',
    },
  },
  googleMaps: {
    dev: {
      iosKey: 'AIzaSyCbBT2TVbFRBO8AF_o6wnXhf3TqdC9EEBg',
      androidKey: 'AIzaSyC2xJMXtQp0gTRxI7wtKrsqiBFkH9RHyuU',
      baseUrl: 'http://test-api.voud.com.br:8086/maps/api',
    },
    hml: {
      iosKey: 'AIzaSyDgEVHB6nazJN-DAJ1D0RMvxCVSh3HNug8',
      androidKey: 'AIzaSyA3Kz_S1Tq8oKQwPW-EwAsKzRYWzSyqYSg',
      baseUrl: 'https://api.voud.com.br:8091/maps/api',
    },
    prod: {
      iosKey: 'AIzaSyDgEVHB6nazJN-DAJ1D0RMvxCVSh3HNug8',
      androidKey: 'AIzaSyA3Kz_S1Tq8oKQwPW-EwAsKzRYWzSyqYSg',
      baseUrl: 'https://api.voud.com.br:8091/maps/api',
    },
  },
  colors: {
    BRAND_PRIMARY: '#242C63',
    BRAND_PRIMARY_LIGHT: '#65729F',
    BRAND_PRIMARY_LIGHTER: '#CFDAFF',
    BRAND_PRIMARY_DARKER: '#191F47',
    BRAND_SECONDARY: '#1EBAED',
    BRAND_SECONDARY_DARKER: '#0095BB',
    BRAND_SUCCESS: '#5dda74',
    BRAND_SUCCESS_LIGHT: 'rgba(93,218,116, .5)',
    BRAND_SUCCESS_LIGHTER: '#d3d800',
    BRAND_ERROR: '#ef4123',
    GRAY: '#707070',
    GRAY_DARKER: '#1d1d1d',
    GRAY_LIGHT: '#c0c0c0',
    GRAY_LIGHT2: '#dadada',
    GRAY_LIGHTER: '#eaeaea',
    CARD_C: '#1EBAED',
    CARD_E: '#D9AF8B',
    CARD_VT: '#ffd385',
    CARD_BU: '#e2001a',
    WHITE: '#FFF',
    BLACK_ALPHA1: 'rgba(0, 0, 0, .1)',
    OVERLAY: 'rgba(0, 0, 0, .4)',
    OVERLAY_LIGHT: 'rgba(0, 0, 0, .1)',
    OVERLAY_DARK: 'rgba(0, 0, 0, .8)',
  },
};
