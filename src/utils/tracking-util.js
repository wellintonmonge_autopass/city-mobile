import { GATrackPurchaseEvent } from "../shared/analytics";
import { FBLogPurchase } from "../shared/facebook";
import { productTypes } from "../redux/financial";

const getProductId = productType => {
  const typesMap = {
    [productTypes.CITY_PLUS]: '1',
    [productTypes.PHONE_RECHARGE]: '3',
  } 
  return typesMap[productType];  
};

export const trackPurchaseEvent = (transactionId, revenue, productType) => {
  const revenueAsNumber = Number(revenue);

  GATrackPurchaseEvent(
    {
      id: getProductId(productType),
      name: productType,
    },
    {
      id: `${transactionId}`,
      revenue: revenueAsNumber,
    },
    'Ecommerce',
    'transaction',
  );

  FBLogPurchase(revenueAsNumber);
}
