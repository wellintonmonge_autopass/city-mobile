import {sha256} from 'js-sha256';
import {Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';

// utils

/**
 * returns app build number
 * @return {Number} build number
 */
export const getBuildNumber = () => {
  const isAndroid = Platform.OS === 'android';
  let buildNumber = DeviceInfo.getBuildNumber();
  if (isAndroid) {
    const digitArchitecture = buildNumber.toString().slice(0, 1);
    const number =
      (buildNumber / digitArchitecture - 1048576) * digitArchitecture;
    buildNumber = Math.round(number);
  }
  return Number(buildNumber);
};

export const getDeviceId = async fcmToken => {
  if (Platform.OS === 'ios') {
    return fcmToken;
  }

  try {
    if (__DEV__) {
      console.tron.log('Android ID: ' + DeviceInfo.getUniqueID());
      console.tron.log('Serial Number: ' + DeviceInfo.getSerialNumber());
    }
    return sha256(`${DeviceInfo.getUniqueID()}${DeviceInfo.getSerialNumber()}`);
  } catch (error) {
    if (__DEV__) {
      console.tron.log(error, true);
    }
    throw error;
  }
};

export const isEmulator = () => {
  if (__DEV__) {
    return false;
  }
  return DeviceInfo.isEmulator();
};
