export const addEllipsis = (string, charLimit = 100) => {
  if (string && string.length > charLimit) {
    return string.slice(0, charLimit) + '...';
  }
  return string;
}

export const capitalizeFirstLetter = string => {
  return string ? string.charAt(0).toUpperCase() + string.slice(1) : '';
}

export const capitalizeLetters = string => string ?
  string.toLowerCase().replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); }) : '';

const acronymWords = ['Cptm'];

export const voudCapitalizeLetters = string => {
  const capitalizeString = capitalizeLetters(string);
  return acronymWords.reduce((acc, cur) => {
    return acc.replace(cur, cur.toUpperCase());
  }, capitalizeString);
}
