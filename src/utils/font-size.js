import {PixelRatio} from 'react-native';

export const GetFontSizeRatio = () => {
  const PIXEL_RATIO = PixelRatio.get();
  const FONT_SCALE = PixelRatio.getFontScale();
  const pixelRatioParsed = parseFloat(parseFloat(PIXEL_RATIO).toPrecision(2));

  let FONT_TITLE = 14 / FONT_SCALE;
  let FONT_MAX = 12 / FONT_SCALE;
  let FONT_AVERAGE = 10 / FONT_SCALE;
  let FONT_MIN = 8 / FONT_SCALE;

  if (pixelRatioParsed > 1 && pixelRatioParsed < 2) {
    FONT_TITLE = 16 / FONT_SCALE;
    FONT_MAX = 14 / FONT_SCALE;
    FONT_AVERAGE = 12 / FONT_SCALE;
    FONT_MIN = 10 / FONT_SCALE;
  } else if (pixelRatioParsed >= 2 && pixelRatioParsed < 2.5) {
    FONT_TITLE = 18 / FONT_SCALE;
    FONT_MAX = 16 / FONT_SCALE;
    FONT_AVERAGE = 14 / FONT_SCALE;
    FONT_MIN = 12 / FONT_SCALE;
  } else if (pixelRatioParsed >= 2.5 && pixelRatioParsed < 3) {
    FONT_TITLE = 20 / FONT_SCALE;
    FONT_MAX = 18 / FONT_SCALE;
    FONT_AVERAGE = 16 / FONT_SCALE;
    FONT_MIN = 14 / FONT_SCALE;
  } else if (pixelRatioParsed >= 3) {
    FONT_TITLE = 22 / FONT_SCALE;
    FONT_MAX = 20 / FONT_SCALE;
    FONT_AVERAGE = 18 / FONT_SCALE;
    FONT_MIN = 16 / FONT_SCALE;
  }

  return {
    FONT_TITLE,
    FONT_MAX,
    FONT_AVERAGE,
    FONT_MIN,
    PIXEL_RATIO,
  };
};
