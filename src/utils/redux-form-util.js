import { destroy } from 'redux-form';
import { AppState, Platform } from 'react-native';

export const clearReduxForm = (dispatch, formName) => {
  // Note - It keeps redux form data if app is in background on Android, because in this scenario
  // the app can be restarted and the form data aren't lost.
  if (Platform.OS === 'ios' || AppState.currentState === 'active') {
    dispatch(destroy(formName));
  }
}