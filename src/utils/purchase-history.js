// consts
const productCategories = {
  MOBILE: 'mobile',
  TRANSPORT_CARD: 'transportCard'
};

export const acquirerNames = {
  CIELO: 'CIELO',
  ADYEN: 'ADYEN',
}

// functions
export const isMobileProduct = productCategory => productCategory === productCategories.MOBILE;

export const isTransportCardProduct = productCategory => productCategory === productCategories.TRANSPORT_CARD;

export const getPurchaseStatusText = (status, isDebit, isMobile) => {
  switch (status) {
    case 'ERROR':
    case 'CANCELED':
      return isMobile ? 'Recarga não realizada' : 'Carga não realizada';
    case 'PROCESSING':
      return isDebit ? 'Aguardando confirmação' : `Aguardando liberação da ${isMobile ? 'recarga': 'carga'}`;
    case 'PROCCESSED':
      return isMobile ? 'Recarga de celular confirmada' : 'Pagamento confirmado';
  }
}
