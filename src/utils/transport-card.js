import { colors } from '../styles';
import { transportCardTypes, walletApplicationId, issuerTypes, transportCardWalletStatus } from '../redux/transport-card';

// consts
const cityPlusLogoImg = require('../images/transport-cards/city-plus-sm.png');

// utils

export const getColorForLayoutType = (layoutType) => {
  if (layoutType && (layoutType === transportCardTypes.CITY_PLUS_VT)) {
    return colors.CARD_VT;
  }

  if (layoutType && (layoutType === transportCardTypes.CITY_PLUS_ESCOLAR || layoutType === transportCardTypes.CITY_PLUS_ESCOLAR_GRATUIDADE)) {
    return colors.CARD_E;
  }

  return colors.CARD_C;
};

export const getLogoSmForLayoutType = layoutType => {
  return cityPlusLogoImg;
}

export const getTransportCardIssuerLabel = issuerType => {
  switch (issuerType) {
    case issuerTypes.CITY_PLUS:
      return 'City +';
    default:
      return '';
  }
}

// Card Status

export const isTransportCardScholarNotRevalidated = cardData =>  {
  const wallets = cardData && cardData.wallets ? cardData.wallets : [];
  const hasScholarNotRevalidated = wallets.some(wallet => (
    (wallet.applicationId === walletApplicationId.CITY_PLUS_ESCOLAR && 
      wallet.validityStatus === transportCardWalletStatus.NOT_REVALIDATED)));
  const hasScholarGratuityNotRevalidated = wallets.some(wallet => (
    (wallet.applicationId === walletApplicationId.CITY_PLUS_ESCOLAR_GRATUIDADE && 
      wallet.validityStatus === transportCardWalletStatus.NOT_REVALIDATED)));

  return hasScholarNotRevalidated && hasScholarGratuityNotRevalidated;
};

// BOM

export const isBOMEscolar = layoutType => layoutType === transportCardTypes.CITY_PLUS_ESCOLAR || layoutType == transportCardTypes.CITY_PLUS_ESCOLAR_GRATUIDADE;

const getBOMEscolarMaxCredit = (cardData, creditValueRange) => {
  const wallets = cardData && cardData.wallets ? cardData.wallets : [];
  const application = wallets.find(wallet => wallet.applicationId === walletApplicationId.CITY_PLUS_ESCOLAR);
  const quoteValueAvailable = application.quoteValueAvailable ? application.quoteValueAvailable * 100 : 0;

  return quoteValueAvailable > creditValueRange.maxCreditValue ? creditValueRange.maxCreditValue : quoteValueAvailable;
}

export const getBOMMaxCredit = (cardData, creditValueRange) => {
  const layoutType = cardData ? cardData.layoutType : '';
  return isBOMEscolar(layoutType) ? getBOMEscolarMaxCredit(cardData, creditValueRange) : creditValueRange.maxCreditValue;
}

// BU

// consts
const buApplicationIds = {
  COMUM: 691,
  ESCOLAR: 687,
  DIARIO_INTEGRADO: 853,
  DIARIO_TRILHO: 850,
  DIARIO_ONIBUS: 847,
  SEMANAL_INTEGRADO: 854,
  SEMANAL_TRILHO: 851,
  SEMANAL_ONIBUS: 848,
  MENSAL_INTEGRADO: 855,
  MENSAL_TRILHO: 852,
  MENSAL_ONIBUS: 849,
};

export const buCreditTypeLabels = {
  COMUM: 'Comum',
  ESCOLAR: 'Escolar',
  TEMPORAL: 'Temporal',
};

export const buPeriodTypeLabels = {
  DIARIO: 'Diário',
  SEMANAL: 'Semanal',
  MENSAL: 'Mensal',
};

export const buTransportTypeLabels = {
  ONIBUS: 'Ônibus',
  TRILHO: 'Trilho',
  INTEGRADO: 'Integrado',
};

export const getBUCreditTypeLabel = applicationId => {
  switch (applicationId) {
    case buApplicationIds.COMUM:
      return buCreditTypeLabels.COMUM;

    case buApplicationIds.ESCOLAR:
      return buCreditTypeLabels.ESCOLAR;

    case buApplicationIds.DIARIO_INTEGRADO:
    case buApplicationIds.DIARIO_TRILHO:
    case buApplicationIds.DIARIO_ONIBUS:
    case buApplicationIds.SEMANAL_INTEGRADO:
    case buApplicationIds.SEMANAL_TRILHO:
    case buApplicationIds.SEMANAL_ONIBUS:
    case buApplicationIds.MENSAL_INTEGRADO:
    case buApplicationIds.MENSAL_TRILHO:
    case buApplicationIds.MENSAL_ONIBUS:
      return buCreditTypeLabels.TEMPORAL;

    default:
      return '';
  }
}

export const getBUPeriodTypeLabel = applicationId => {
  switch (applicationId) {
    case buApplicationIds.DIARIO_INTEGRADO:
    case buApplicationIds.DIARIO_TRILHO:
    case buApplicationIds.DIARIO_ONIBUS:
      return buPeriodTypeLabels.DIARIO;

    case buApplicationIds.SEMANAL_INTEGRADO:
    case buApplicationIds.SEMANAL_TRILHO:
    case buApplicationIds.SEMANAL_ONIBUS:
      return buPeriodTypeLabels.SEMANAL;

    case buApplicationIds.MENSAL_INTEGRADO:
    case buApplicationIds.MENSAL_TRILHO:
    case buApplicationIds.MENSAL_ONIBUS:
      return buPeriodTypeLabels.MENSAL;

    default:
      return '';
  }
}

export const getBUTransportTypeLabel = applicationId => {
  switch (applicationId) {
    case buApplicationIds.DIARIO_ONIBUS:
    case buApplicationIds.SEMANAL_ONIBUS:
    case buApplicationIds.MENSAL_ONIBUS:
      return buTransportTypeLabels.ONIBUS;
    
    case buApplicationIds.DIARIO_TRILHO:
    case buApplicationIds.SEMANAL_TRILHO:
    case buApplicationIds.MENSAL_TRILHO:
      return buTransportTypeLabels.TRILHO;
    
    case buApplicationIds.DIARIO_INTEGRADO:
    case buApplicationIds.SEMANAL_INTEGRADO:
    case buApplicationIds.MENSAL_INTEGRADO:
      return buTransportTypeLabels.INTEGRADO;
      
    default:
      return '';
  }
}

export const getBUSupportedCreditTypes = (cardData, ignoreList = []) => (
  Object.values(buCreditTypeLabels).filter(
    label => cardData && cardData.wallets && cardData.wallets.some(el => getBUCreditTypeLabel(el.applicationId) === label &&
    !ignoreList.some(el => el === label))
  )
)

export const getBUSupportedPeriodTypes = cardData => (
  Object.values(buPeriodTypeLabels).filter(
    label => cardData && cardData.wallets && cardData.wallets.some(el => getBUPeriodTypeLabel(el.applicationId) === label)
  )
)

export const getTemporalCreditValueLabel = (periodType, transportType, quotaQty) => {
  const quotaTypeLabel = periodType === '' ? 'Temporal' : `${periodType} ${transportType ? transportType.toLowerCase() : ''}`;
  return `${quotaTypeLabel} - ${quotaQty} ${quotaQty === 1 ? ' cota' : ' cotas'}`;
}

export const getDefaultCreditValueFieldLabel = (layoutType, buCreditType) => (
  `Valor do crédito${buCreditType && layoutType === transportCardTypes.BU ? ` ${buCreditType.toLowerCase()}` : ''}`
);

export const getCreditValueFieldLabel = (cardData, buCreditType, buPeriodType, buTransportType, buQuotaQty) => {
  const isBU = cardData && cardData.layoutType === transportCardTypes.BU;
  const layoutType = cardData && cardData.layoutType ? cardData.layoutType : '';
  return isBU && buCreditType === buCreditTypeLabels.TEMPORAL ?
    getTemporalCreditValueLabel(buPeriodType, buTransportType, buQuotaQty) : getDefaultCreditValueFieldLabel(layoutType, buCreditType);
}

export const findBUTemporalProduct = (wallets = [], periodType, transportType) => (
  wallets.find(el => (
    periodType !== '' && getBUPeriodTypeLabel(el.applicationId) === periodType &&
    transportType !== '' && getBUTransportTypeLabel(el.applicationId) === transportType
  ))
)

export const findBUProduct = (wallets = [], buCreditType, periodType = '', transportType = '') => (
  buCreditType === buCreditTypeLabels.TEMPORAL ?
    findBUTemporalProduct(wallets, periodType, transportType) :
    wallets.find(el => getBUCreditTypeLabel(el.applicationId) === buCreditType)
);

export const filterBUTransportTypeOptionsByPeriod = (wallets = [], periodType) => {
  const productsFilteredByPeriod = wallets.filter(el => getBUPeriodTypeLabel(el.applicationId) === periodType);
  return Object.values(buTransportTypeLabels).filter(
    label => productsFilteredByPeriod.some(el => getBUTransportTypeLabel(el.applicationId) === label)
  );
}