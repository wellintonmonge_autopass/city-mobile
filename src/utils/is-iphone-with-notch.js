import {hasNotch} from './hasNotch';

export function getPaddingForNotch() {
  return hasNotch() ? 24 : 0;
}
