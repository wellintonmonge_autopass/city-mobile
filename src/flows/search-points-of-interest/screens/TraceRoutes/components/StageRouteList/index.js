import React, {Component} from 'react';
import {View} from 'react-native';

import moment from 'moment';
import * as geolib from 'geolib';

//Import Voud

import StageRouteItemStartEnd from '../StageRouteItemStartEnd';
import StageLine from '../StageLine';
import StageWalk from '../StageWalk';
import StageTransit from '../StageTransit';
import VoudText from '../../../../../../components/VoudText';
import {calculateDistance} from '../../../../../../utils/haversine';
import styles from './style';
import {connect} from 'react-redux';

class StageRouteList extends Component {
  constructor(props) {
    super(props);
  }

  _formatFare(fare) {
    if (!fare) {
      return '-';
    }
    return String(fare.value.toFixed(2)).replace('.', ',');
  }

  _getHoursAndMinutesFromSeconds(durationInSeconds) {
    const hours = durationInSeconds / 3600;
    const minutes = (hours % 1) * 60;

    return {
      hours: Math.trunc(hours),
      minutes: Math.trunc(minutes),
    };
  }

  _getkilometersAndMetersFromDistance(distance) {
    const kilometers = distance.inMeters > 1000 ? distance.inMeters / 1000 : 0;
    const meters = distance.inMeters;

    return {
      kilometers,
      meters,
    };
  }

  _stageWalk(item, index) {
    const {hours, minutes} = this._getHoursAndMinutesFromSeconds(
      item.duration.inSeconds,
    );
    const {kilometers, meters} = this._getkilometersAndMetersFromDistance(
      item.distance,
    );

    let description = '';

    if (item.distance.inMeters === 0) {
      description = 'Baldeação';
    } else {
      let descriptionHour = '';

      if (hours === 0 && minutes === 0 && item.distance.inMeters > 0) {
        descriptionHour = `Ande ${item.duration.humanReadable}`;
      } else {
        descriptionHour = hours ? ` Ande ${hours}h` : `Ande ${minutes} min`;
      }

      const descriptionKm = kilometers
        ? `(${String(kilometers.toFixed(1)).replace('.', ',')} km)`
        : `(${meters} m)`;

      description = `${descriptionHour} ${descriptionKm}`;
    }

    return (
      <View key={index}>
        <StageWalk key={index} description={description} />
        <StageLine index={index} />
      </View>
    );
  }

  getHourFromDepartureTime = (hour, min) => {
    return moment(new Date().setHours(hour, min)).format('HH:mm');
  };

  getHourFromArrivalTime = (hour, min, inSeconds) => {
    return moment(new Date().setHours(hour, min))
      .add(inSeconds, 'seconds')
      .format('HH:mm');
  };

  _calculateDistance = (lat, lng, lat2, lng2) => {
    const distance = calculateDistance(lat, lng, lat2, lng2);

    if (distance > 1) {
      return `${distance}km`;
    }

    // if under 1km, convert to meters
    return `${distance * 1000}m`;
  };

  _caculateTotalDistancePoints = points => {
    const latLng = points.map(item => {
      return {
        lat: item.positionLatitude,
        lng: item.positionLongitude,
      };
    });
    var sumDistance = 0;
    latLng.reduceRight((previousValue, currentValue) => {
      const distance = geolib.getDistance(
        {
          latitude: previousValue.lat,
          longitude: previousValue.lng,
        },
        {
          latitude: currentValue.lat,
          longitude: currentValue.lng,
        },
      );

      sumDistance += parseFloat(distance);

      return currentValue;
    });
    return sumDistance;
  };

  getTotalMinutesByHour(hour) {
    return Math.floor(hour * 60);
  }

  getHourAndMinute(totalMinute) {
    const hour = Math.floor(totalMinute / 60);
    const min = totalMinute % 60;

    if (hour === 0) {
      return `${min} min`;
    }

    const hours = this.getHourFromDepartureTime(hour, min);
    return hours;
  }

  getSpeed = distance => {
    const time = Number(80000) - Number(10000);
    const metersPerSecond = (distance / time) * 1000;
    return Math.round(metersPerSecond);
  };

  render() {
    const {
      item,
      origin,
      destination,
      markEndTimeDistance,
      markStartTimeDistance,
    } = this.props;

    const {lineNumber, totalTripTime, description, points} = item;

    const totalPoints = points.length;
    const firstPoint = points[0];
    const lastPoint = points[totalPoints - 1];
    const lastPointName = lastPoint.name;

    const totalDis = this._caculateTotalDistancePoints(points);

    const totalDistanceLine = geolib.convertDistance(
      totalDis,
      totalDis > 1000 ? 'km' : 'm',
    );

    let totalDistance = 0;
    const isKilometer = totalDistanceLine > 1;

    if (isKilometer) {
      totalDistance = parseInt(totalDistanceLine);
    } else {
      const normalizedDistance = totalDistanceLine.toPrecision(3);
      totalDistance = normalizedDistance.toString().substr(0, 3);
    }

    const originLatLng = {
      latitude: origin.geometry.location.lat,
      longitude: origin.geometry.location.lng,
    };

    const destinationLatLng = {
      latitude: destination.geometry.location.lat,
      longitude: destination.geometry.location.lng,
    };

    const firstStopLatLng = {
      latitude: firstPoint.positionLatitude,
      longitude: firstPoint.positionLongitude,
    };

    const lastStopLatLng = {
      latitude: lastPoint.positionLatitude,
      longitude: lastPoint.positionLongitude,
    };

    const totalDistanceFirstWalk = geolib.getDistance(
      originLatLng,
      firstStopLatLng,
    );
    const totalTime = this.getHourAndMinute(totalTripTime);
    const totalTimeFirstWalk = this.getSpeed(totalDistanceFirstWalk);
    const totalDistanceLastWalk = geolib.getDistance(
      lastStopLatLng,
      destinationLatLng,
    );
    const totalTimeLastWalk = this.getSpeed(totalDistanceLastWalk);

    return (
      <View style={styles.container}>
        <StageRouteItemStartEnd address={origin.name} />
        <StageLine />
        <StageWalk
          description={`ande aproximadamente ${
            totalTimeFirstWalk === 0 ? '' : totalTimeFirstWalk
          } min - (${totalDistanceFirstWalk}m)`}
        />
        <StageLine />
        {points.map((item, index) => {
          if (index === 0) {
            return (
              <View key={index}>
                <StageTransit
                  item={item}
                  totalPoints={totalPoints}
                  totalTime={totalTime}
                  totalDistance={totalDistance}
                  lineNumber={lineNumber}
                  description={description}
                  index={index}
                  lastPointName={lastPointName}
                />
                <StageLine index={index} />
              </View>
            );
          }
        })}
        <StageWalk
          description={`ande aproximadamente ${
            totalTimeLastWalk === 0 ? '' : totalTimeLastWalk
          } min - (${totalDistanceLastWalk}m)`}
        />
        <StageLine />
        <StageRouteItemStartEnd address={destination.name} />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    markEndTimeDistance: state.searchPointsOfInterest.markEndTimeDistance,
    markStartTimeDistance: state.searchPointsOfInterest.markStartTimeDistance,
  };
};

export default connect(mapStateToProps)(StageRouteList);
