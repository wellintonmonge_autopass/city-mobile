import React, {Component, Fragment} from 'react';
import {View, Dimensions} from 'react-native';
import MapView, {Marker} from 'react-native-maps';

import styles from './style';
import {getPixelSize} from '../../utils';

const imageDestination = require('../../images/ic-end.png');
const imageOrigin = require('../../images/ic-start.png');
import mapStyle from '../../../../components/mapStyles';
import MapViewDirections from 'react-native-maps-directions';
import {colors} from '../../../../../../styles';
import {connect} from 'react-redux';
import {setMarkendTimeDistance} from '../../../../actions';

const imageMarker = require('../../images/ic-bus-Azul.png');
const imageMarkerBusStop = require('../../images/ic-bus-Amarela.png');

const {width, height} = Dimensions.get('window');

const APIKEY_DIRECTION = 'AIzaSyBfvv9lgW3Lisf478v8OxHjjDnxtKSYFZw';

class TraceRouteMapView extends Component {
  constructor(props) {
    super(props);

    const {data, origin, destination} = this.props;

    this.way = [];
    this.wayWalking = [];
    this.decodedPolyline = [];

    let coordinate = [];
    let coordinateStart = [];
    let coordinateEnd = [];

    var points = Object.keys(data).map(function(key) {
      return data[key];
    });

    if (data) {
      //Origin
      if (origin) {
        coordinateStart.push({
          longitude: parseFloat(origin.geometry.location.lng),
          latitude: parseFloat(origin.geometry.location.lat),
        });
        coordinateStart.push({
          longitude: parseFloat(points[0].positionLongitude),
          latitude: parseFloat(points[0].positionLatitude),
        });
      }

      points.map(item => {
        coordinate.push({
          longitude: parseFloat(item.positionLongitude),
          latitude: parseFloat(item.positionLatitude),
        });
      });

      if (destination) {
        // Destination
        coordinateEnd.push({
          longitude: parseFloat(destination.geometry.location.lng),
          latitude: parseFloat(destination.geometry.location.lat),
        });
        coordinateEnd.push({
          longitude: parseFloat(points[points.length - 1].positionLongitude),
          latitude: parseFloat(points[points.length - 1].positionLatitude),
        });
      }
    }

    this.state = {
      region: {
        latitude: origin.geometry.location.lat,
        longitude: origin.geometry.location.lng,
        latitudeDelta: 0.005,
        longitudeDelta: 0.005,
      },
      destination: {
        latitude: destination.geometry.location.lat,
        longitude: destination.geometry.location.lng,
        latitudeDelta: 0.005,
        longitudeDelta: 0.005,
      },
      markersCoordinates: coordinate,
      markerStart: coordinateStart,
      markerEnd: coordinateEnd,
    };

    this.mapRef = null;
  }

  componentDidMount() {
    this.props.onRef(this);
  }

  renderMapCenter = (rightSize, leftSize, topSize, bottomSize) => {
    if (this.mapRef) {
      this.mapRef.fitToCoordinates(this.markersCoordinates, {
        edgePadding: {
          right: getPixelSize(rightSize),
          left: getPixelSize(leftSize),
          top: getPixelSize(topSize),
          bottom: getPixelSize(bottomSize),
        },
        animated: true,
      });
    }
  };

  _handlerLocation = (right, left, top, bottom) => {
    this.renderMapCenter(right, left, top, bottom);
  };

  render() {
    const {markersCoordinates, markerEnd, markerStart} = this.state;

    const {origin, destination} = this.props;

    // const markerAnchorDestination = {
    //   x: Platform.OS === 'ios' ? 0 : 0.3,
    //   y: Platform.OS === 'ios' ? 0 : 0,
    // };

    // const markerAnchorStops = {
    //   x: Platform.OS === 'ios' ? 0 : 0.5,
    //   y: Platform.OS === 'ios' ? 0 : 0,
    // };

    // const right = Platform.OS === 'ios' ? 15 : 0;
    // const left = Platform.OS === 'ios' ? 15 : 0;

    const latitudeOrigin = origin.geometry.location.lat;
    const longitudeOrigin = origin.geometry.location.lng;
    const latitudeDestination = destination.geometry.location.lat;
    const longitudeDestination = destination.geometry.location.lng;

    return (
      <View style={styles.container}>
        <MapView
          provider={'google'}
          ref={ref => {
            this.mapRef = ref;
          }}
          minZoomLevel={9}
          maxZoomLevel={18}
          style={styles.containerMap}
          initialRegion={{
            latitude: latitudeOrigin,
            longitude: longitudeOrigin,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          customMapStyle={mapStyle}
          onLayout={event => {
            this._mapHeight = event.nativeEvent.layout.height;
          }}>
          <Marker
            coordinate={{
              latitude: markerStart[1].latitude,
              longitude: markerStart[1].longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            anchor={{x: 0, y: 0}}
            image={imageMarker}
            calloutOffset={{x: 0, y: 0}}
          />
          <Marker
            coordinate={{
              latitude: markerEnd[1].latitude,
              longitude: markerEnd[1].longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            anchor={{x: 0, y: 0}}
            image={imageMarker}
            calloutOffset={{x: 0, y: 0}}
          />
          <Marker
            coordinate={{
              latitude: latitudeOrigin,
              longitude: longitudeOrigin,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            anchor={{x: 0, y: 0}}
            image={imageOrigin}
            calloutOffset={{x: 0, y: 0}}
          />
          <Marker
            coordinate={{
              latitude: latitudeDestination,
              longitude: longitudeDestination,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            anchor={{x: 0, y: 0}}
            image={imageDestination}
            calloutOffset={{x: 0, y: 0}}
          />
          <MapViewDirections
            origin={markerStart[0]}
            destination={markerStart[1]}
            apikey={APIKEY_DIRECTION}
            strokeWidth={3}
            mode={'WALKING'}
            strokeColor={colors.BRAND_SECONDARY}
          />
          {markersCoordinates.map((item, index, coords) => {
            const isFirst = index === 0;
            const isLast = coords.length - 1 === index;
            return (
              <Fragment>
                <Marker
                  coordinate={{
                    latitude: item.latitude,
                    longitude: item.longitude,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                  }}
                  anchor={{x: 0, y: 0}}
                  image={isLast || isFirst ? imageMarker : imageMarkerBusStop}
                  calloutOffset={{x: 0, y: 0}}
                />
                <MapViewDirections
                  origin={item}
                  destination={coords[index + 1]}
                  apikey={APIKEY_DIRECTION}
                  mode={'CAR'}
                  strokeWidth={3}
                  strokeColor={colors.BRAND_PRIMARY}
                />
              </Fragment>
            );
          })}
          <MapViewDirections
            origin={markerEnd[0]}
            destination={markerEnd[1]}
            apikey={APIKEY_DIRECTION}
            strokeWidth={3}
            strokeColor={colors.BRAND_SECONDARY}
            mode={'WALKING'}
            waypoints={markerEnd}
            optimizeWaypoints={true}
          />
        </MapView>
      </View>
    );
  }
}

const mapTopProps = state => ({});

export default connect(mapTopProps)(TraceRouteMapView);
