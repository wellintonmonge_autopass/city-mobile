import React, {Component} from 'react';
import {View} from 'react-native';

//Import Voud

import VoudText from '../../../../../../components/VoudText';
import Icon from '../../../../../../components/Icon';
import {adaptColor} from '../../../../../search-points-of-interest/utils/adapt-route-json';

import styles from './style';
import {colors} from '../../../../../../styles';

class StageTransit extends Component {
  constructor(props) {
    super(props);
  }

  _getHoursAndMinutesFromSeconds(durationInSeconds) {
    const hours = durationInSeconds / 3600;
    const minutes = (hours % 1) * 60;

    return {
      hours: Math.trunc(hours),
      minutes: Math.trunc(minutes),
    };
  }

  _getVehicleFromType = type => {
    switch (type) {
      case 'heavy_rail':
        return 'train';

      default:
        return type;
    }
  };

  render() {
    const {
      item,
      index,
      totalPoints,
      totalTime,
      totalDistance,
      lineNumber,
      lastPointName,
    } = this.props;

    const {name, company} = item;
    const shortName = name.split('-')[0];
    const titleColor = '#fff';
    const barColor = colors.BRAND_PRIMARY;
    return (
      <View style={[styles.container]} key={index}>
        <View style={[styles.containerIcon]}>
          <Icon name="bus" style={styles.iconVoud} />
        </View>
        <View style={[styles.barStage, {backgroundColor: barColor}]}>
          <View style={styles.line} />
        </View>
        <View style={styles.containerDescription}>
          <View style={{marginLeft: 15}}>
            <View style={{marginTop: 0}}>
              <VoudText numberOfLines={2} style={styles.titleLine}>
                {shortName}
              </VoudText>
            </View>
            <View style={[styles.containerDescriptionLine]}>
              {!!lineNumber && (
                <View
                  style={[styles.descriptionLine, {backgroundColor: barColor}]}>
                  <VoudText style={[styles.textLine, {color: titleColor}]}>
                    {lineNumber}
                  </VoudText>
                </View>
              )}

              <View style={styles.textLineDescripiton}>
                {/* <VoudText style={styles.title}>{company}</VoudText> */}
              </View>
            </View>
            <View style={styles.containerTrace}>
              <View style={styles.trace} />
            </View>
            <View style={styles.descriptionOption}>
              <VoudText style={styles.title}>
                A {totalPoints} parada(s) {totalTime} - ({totalDistance}km)
              </VoudText>
            </View>
            <View style={styles.containerTrace}>
              <View style={styles.trace} />
            </View>
            <View style={{marginTop: 25}}>
              <VoudText style={styles.titleLine}>
                {lastPointName.split('-')[0]}
              </VoudText>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default StageTransit;
