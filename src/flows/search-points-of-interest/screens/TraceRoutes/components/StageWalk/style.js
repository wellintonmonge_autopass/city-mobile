import {StyleSheet} from 'react-native';
import {colors} from '../../../../../../styles';
import {GetFontSizeRatio} from '../../../../../../utils/font-size';
const fontSizaRatio = GetFontSizeRatio();


const Style = StyleSheet.create({
  container: {
    marginLeft: 10,
    margin: 0,
    flexDirection: 'row',
  },

  iconVoud: {
    fontSize: fontSizaRatio.FONT_MAX,
    marginRight: 0,
    color: colors.BRAND_SECONDARY,
  },

  text: {
    marginLeft: 15,
    fontSize: fontSizaRatio.FONT_MIN,
  },
});

export default Style;
