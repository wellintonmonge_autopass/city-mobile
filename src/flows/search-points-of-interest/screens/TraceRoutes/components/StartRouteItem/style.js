import {StyleSheet} from 'react-native';
import {colors} from '../../../../../../styles';
import {GetFontSizeRatio} from '../../../../../../utils/font-size';
const fontSizaRatio = GetFontSizeRatio();

// Styles
const Style = StyleSheet.create({
  resultSearchRouteListItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    backgroundColor: 'white',
  },
  itemWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: colors.GRAY_LIGHT2,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 0,
  },
  noBorder: {
    borderBottomWidth: 0,
  },
  contentLeft: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingVertical: 8,
    width: 48,
  },
  contentMiddle: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingVertical: 8,
    height: 56,
    marginHorizontal: 8,
    width: 24,
  },
  contentRight: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    paddingVertical: 5,
  },
  iconVoud: {
    fontSize: fontSizaRatio.FONT_AVERAGE,
    margin: 5,
    color: colors.BRAND_PRIMARY,
  },
  icon: {
    fontSize: fontSizaRatio.FONT_AVERAGE,
    color: colors.GRAY,
  },
  img: {
    maxWidth: 26,
  },
  bomImg: {
    height: 10,
  },
  buImg: {
    height: 19,
  },
  buImgLarge: {
    height: 24,
  },
  bomImgLarge: {
    height: 24,
  },
  routeStepsWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
    marginBottom: 4,
  },
  mb8: {
    marginBottom: 8,
  },
  containerPrice: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 5,
  },
  price: {
    fontWeight: 'bold',
    fontSize: fontSizaRatio.FONT_MIN,
    flex: 1,
  },
  img: {
    maxWidth: 24,
  },
  imgBom: {
    maxWidth: 24,
  },
  contentMiddle: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 0,
    height: 56,
    marginHorizontal: 5,
    width: 24,
  },
  contentDurationWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  contentIconDirections: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    width: 35,
  },
  middleContainerOuter: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: colors.BRAND_SECONDARY,
  },
  middleContainer: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    padding: 2,
    margin: 2,
    borderRadius: 5,
    backgroundColor: colors.BRAND_SECONDARY,
  },
  imgCityPlus: {
    width: 40,
    height: 20,
    alignSelf: 'center',
    padding: 5,
    marginStart: 5,
    marginEnd: 5,
  },
  blueLine: {
    height: 0,
    borderWidth: 1,
    borderColor: colors.BRAND_SECONDARY,
    alignSelf: 'center',
    width: 20,
  },
  walkStyle: {
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 5,
    padding: 5,
    borderColor: colors.BRAND_SECONDARY,
  },
});

export default Style;
