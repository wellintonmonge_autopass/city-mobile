// NPM Imports
import React, {Component} from 'react';
import {View, Image, Dimensions} from 'react-native';

// VouD Imports

import Icon from '../../../../../../components/Icon';

import DurationWrapper from '../../../SearchRoutes/ResultSearchRouteList/DurationWrapper';
import BiggestStepItem from '../../../SearchRoutes/ResultSearchRouteList/BiggestStepItem';
import SingleSubstepItem from '../../../SearchRoutes/ResultSearchRouteList/SingleSubstepItem';
import configRouteItemObj from '../../../../../search-points-of-interest/utils/config-route-item-obj';
import TwoSubstepsItem from '../../../SearchRoutes/ResultSearchRouteList/TwoSubstepsItem';
import InfiniteSubstepsItem from '../../../SearchRoutes/ResultSearchRouteList/InfiniteSubstepsItem';
import AbbreviatedSubstepItem from '../../../SearchRoutes/ResultSearchRouteList/AbbreviatedSubstepItem';
import VoudText from '../../../../../../components/VoudText';

// component
import adaptRouteJson from '../../../../../search-points-of-interest/utils/adapt-route-json';
import getStepWidthByCategory from '../../../../../search-points-of-interest/utils/get-step-width-by-category';
import getWidthForAbbreviatedSteps from '../../../../../search-points-of-interest/utils/get-width-for-abbreviated-steps';
import getStepsType from '../../../../../search-points-of-interest/utils/get-steps-type';

import styles from './style';
import { colors } from '../../../../../../styles';
import { GetFontSizeRatio } from '../../../../../../utils/font-size';

// Component
class StartRouteItem extends Component {
  _renderBiggestStepItem = ({steps, totalSubsteps}) => {
    const types = getStepsType(steps);
    return <BiggestStepItem totalSubsteps={totalSubsteps} types={types} />;
  };

  _renderStepsWithAbbreviation = ({type, substeps}, index) => {
    return (
      <AbbreviatedSubstepItem key={index} type={type} substeps={substeps} />
    );
  };

  _renderStepByCategory = ({type, substeps}, index) => {
    const {length} = substeps;
    if (length === 1) {
      return (
        <SingleSubstepItem
          key={index}
          type={type}
          color={substeps[0].color && substeps[0].color}
          shortName={substeps[0].shortName}
        />
      );
    }
    if (length === 2) {
      return <TwoSubstepsItem key={index} type={type} substeps={substeps} />;
    }
    return <InfiniteSubstepsItem key={index} type={type} substeps={substeps} />;
  };

  _renderRouteSteps = formattedSteps => {
    const {steps} = formattedSteps;
    const {width: deviceWidth} = Dimensions.get('window');
    const {TOTAL_UNAVAILABLE_WIDTH} = configRouteItemObj;
    const availableWidth = deviceWidth - TOTAL_UNAVAILABLE_WIDTH;
    const stepsWidthByCategory = steps.map(getStepWidthByCategory);
    const totalWidthByCategory = stepsWidthByCategory.reduce(
      (acc, item) => acc + item,
    );
    const totalWidthWithAbbreviation = getWidthForAbbreviatedSteps(steps);

    if (totalWidthByCategory <= availableWidth) {
      return steps.map(this._renderStepByCategory);
    }
    if (totalWidthWithAbbreviation <= availableWidth) {
      return steps.map(this._renderStepsWithAbbreviation);
    }
    return this._renderBiggestStepItem(formattedSteps);
  };

  _formatFare(fare) {
    if (!fare) {
      return '-';
    }
    return String(fare.value.toFixed(2)).replace('.', ',');
  }

  _renderSizeImageCard(card, acceptedCards) {
    if (card === 'BU') {
      return acceptedCards.length > 1 ? styles.buImg : styles.buImgLarge;
    } else {
      return acceptedCards.length > 1 ? styles.bomImg : styles.bomImgLarge;
    }
  }

  _getHoursAndMinutesFromSeconds(durationInSeconds) {
    const hours = durationInSeconds / 3600;
    const minutes = (hours % 1) * 60;

    if (Math.trunc(hours)) {
      return Math.trunc(hours);
    } else {
      return Math.trunc(minutes);
    }
  }

  renderSubsteps = shortName => {
    return (
      <View style={styles.middleContainerOuter}>
        <View style={styles.middleContainer}>
          <Icon name="ic-buss" style={styles.iconVoud} />
          <VoudText style={{padding: 5, fontWeight: 'bold'}}>
            {shortName}
          </VoudText>
        </View>
        <Image
          source={require('../../../../../../images/transport-cards/city-plus.png')}
          resizeMode="contain"
          style={styles.imgCityPlus}
        />
      </View>
    );
  };

  renderTitle = htmlInstructions => {
    return (
      <View style={{flexDirection: 'row', paddingVertical: 8}}>
        <View>
          <VoudText
            style={{
              fontSize: GetFontSizeRatio.FONT_MIN,
              color: colors.BRAND_PRIMARY,
            }}>
            {htmlInstructions}
          </VoudText>
        </View>
        <View style={{flex: 1}} />
      </View>
    );
  };

  render() {
    const {item} = this.props;
    const {lineNumber, description} = item;
    return (
      <View style={{flexDirection: 'column'}}>
        {this.renderTitle(description)}
        <View style={{flexDirection: 'row'}}>
          <View style={styles.walkStyle}>
            <Icon name="ic-walk" style={styles.iconVoud} />
          </View>
          <View style={styles.blueLine} />
          {this.renderSubsteps(lineNumber)}
          <View style={styles.blueLine} />
          <View style={styles.walkStyle}>
            <Icon name="ic-walk" style={styles.iconVoud} />
          </View>
        </View>
      </View>
    );
  }
}

export default StartRouteItem;
