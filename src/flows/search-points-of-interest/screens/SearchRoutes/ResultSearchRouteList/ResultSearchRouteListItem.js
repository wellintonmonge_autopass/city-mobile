// NPM Imports
import React, {Component} from 'react';
import {View, StyleSheet, Image, Dimensions, Text} from 'react-native';

// VouD Imports
import TouchableNative from '../../../../../components/TouchableNative';
import Icon from '../../../../../components/Icon';
import {colors} from '../../../../../styles';
import DurationWrapper from './DurationWrapper';
import BiggestStepItem from './BiggestStepItem';
import SingleSubstepItem from './SingleSubstepItem';
import configRouteItemObj from '../../../utils/config-route-item-obj';
import TwoSubstepsItem from './TwoSubstepsItem';
import InfiniteSubstepsItem from './InfiniteSubstepsItem';
import AbbreviatedSubstepItem from './AbbreviatedSubstepItem';

// component
import adaptRouteJson from '../../../utils/adapt-route-json';
import getStepWidthByCategory from '../../../utils/get-step-width-by-category';
import getWidthForAbbreviatedSteps from '../../../utils/get-width-for-abbreviated-steps';
import getStepsType from '../../../utils/get-steps-type';
import FareWrapper from './FareWrapper';
import NextPublicTransportArrival from './NextPublicTransportArrival';
import getFirstStop from '../../../utils/get-first-stop';
import BrandText from '../../../../../components/BrandText';
import SystemText from '../../../../../components/SystemText';
import {GetFontSizeRatio} from '../../../../../utils/font-size';
import moment from 'moment';

const fontSizeRatio = GetFontSizeRatio();
// Styles
const styles = StyleSheet.create({
  resultSearchRouteListItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 4,
    backgroundColor: 'white',
  },
  itemWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: colors.GRAY_LIGHT2,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 20,
    paddingHorizontal: 16,
  },
  noBorder: {
    borderBottomWidth: 0,
  },
  contentLeft: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingVertical: 8,
    width: 48,
  },
  contentMiddle: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8,
    height: 56,
    marginHorizontal: 4,
    width: 30,
  },
  contentRight: {
    flex: 1,
    flexDirection: 'column',
  },
  iconVoud: {
    fontSize: fontSizeRatio.FONT_AVERAGE,
    alignSelf: 'center',
    margin: 5,
    color: colors.BRAND_PRIMARY,
  },
  img: {
    maxWidth: 24,
  },
  bomImg: {
    height: 10,
  },
  buImg: {
    height: 19,
  },
  buImgLarge: {
    height: 24,
  },
  bomImgLarge: {
    height: 24,
  },
  routeStepsWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
    marginBottom: 4,
  },
  mb8: {
    marginBottom: 8,
  },
  contentRightPrice: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    width: 70,
  },
  arrowContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  durationContent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  middleContainerOuter: {
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: colors.BRAND_SECONDARY,
  },
  middleContainer: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    padding: 2,
    margin: 2,
    borderRadius: 5,
    backgroundColor: colors.BRAND_SECONDARY,
  },
  imgCityPlus: {
    width: 40,
    height: 20,
    alignSelf: 'center',
    padding: 5,
    marginStart: 5,
    marginEnd: 5,
  },
  blueLine: {
    height: 0,
    borderWidth: 1,
    borderColor: colors.BRAND_SECONDARY,
    alignSelf: 'center',
    width: 20,
  },
  walkStyle: {
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 5,
    padding: 5,
    borderColor: colors.BRAND_SECONDARY,
  },
});

// Component
class ResultSearchRouteListItem extends Component {
  _renderBiggestStepItem = ({steps, totalSubsteps}) => {
    const types = getStepsType(steps);
    return <BiggestStepItem totalSubsteps={totalSubsteps} types={types} />;
  };

  _renderStepsWithAbbreviation = ({type, substeps}, index) => {
    return (
      <AbbreviatedSubstepItem key={index} type={type} substeps={substeps} />
    );
  };

  _renderStepByCategory = ({type, substeps}, index) => {
    const {length} = substeps;
    if (length === 1) {
      return (
        <SingleSubstepItem
          key={index}
          type={type}
          color={substeps[0].color && substeps[0].color}
          shortName={substeps[0].shortName}
        />
      );
    }
    if (length === 2) {
      return <TwoSubstepsItem key={index} type={type} substeps={substeps} />;
    }
    return <InfiniteSubstepsItem key={index} type={type} substeps={substeps} />;
  };

  _renderRouteSteps = formattedSteps => {
    const {steps} = formattedSteps;
    const {width: deviceWidth} = Dimensions.get('window');
    const {TOTAL_UNAVAILABLE_WIDTH} = configRouteItemObj;
    const availableWidth = deviceWidth - TOTAL_UNAVAILABLE_WIDTH;
    const stepsWidthByCategory = steps.map(getStepWidthByCategory);
    const totalWidthByCategory = stepsWidthByCategory.reduce(
      (acc, item) => acc + item,
    );
    const totalWidthWithAbbreviation = getWidthForAbbreviatedSteps(steps);
    if (totalWidthByCategory <= availableWidth) {
      return steps.map(this._renderStepByCategory);
    }
    if (totalWidthWithAbbreviation <= availableWidth) {
      return steps.map(this._renderStepsWithAbbreviation);
    }
    return this._renderBiggestStepItem(formattedSteps);
  };

  _renderSizeImageCard(card, acceptedCards) {
    if (card === 'BU') {
      return acceptedCards.length > 1 ? styles.buImg : styles.buImgLarge;
    } else {
      return acceptedCards.length > 1 ? styles.bomImg : styles.bomImgLarge;
    }
  }

  _renderAcceptedCards(acceptedCards) {
    return acceptedCards.sort().map((card, index) => {
      return (
        <Image
          source={card === 'BU'}
          style={StyleSheet.flatten([
            styles.img,
            this._renderSizeImageCard(card, acceptedCards),
            acceptedCards.length > 1 && index !== acceptedCards.length - 1
              ? styles.mb8
              : {},
          ])}
          resizeMode="contain"
          key={index}
        />
      );
    });
  }

  convertToHour = min => {
    return Math.abs(Math.round(min / 60));
  };

  renderTitle = (htmlInstructions, arriveTime, totalTripTime, status) => {
    const arrivalTimeFormatted = moment()
      .add(arriveTime, 'm')
      .format('HH:mm');

    const img =
      status === 'ONLINE'
        ? require('../../../../../images/ic-conect.png')
        : require('../../../../../images/ic-disconect.png');

    return (
      <View style={{flexDirection: 'row', paddingVertical: 8}}>
        <View>
          <View
            style={{
              flexDirection: 'row',
              alignContent: 'flex-start',
              alignItems: 'center',
              paddingVertical: 3,
            }}>
            <Image
              source={img}
              resizeMode={'contain'}
            />
            <BrandText
              style={{
                fontSize: fontSizeRatio.FONT_MAX,
                fontWeight: 'bold',
                padding: 5,
              }}>
              {`${totalTripTime} min`}
            </BrandText>
            <BrandText
              style={{
                fontSize: fontSizeRatio.FONT_MIN,
                padding: 5,
              }}>
              {`(você chegará às ${moment()
                .add(arriveTime, 'm')
                .add(totalTripTime, 'm')
                .format('HH:mm')})`}
            </BrandText>
          </View>
          <BrandText
            style={{
              fontSize: fontSizeRatio.FONT_MIN,
              color: colors.BRAND_PRIMARY,
              paddingVertical: 3,
            }}>
            {`Próximo ônibus passará às ${arrivalTimeFormatted} na parada`}
          </BrandText>
          <BrandText
            style={{
              fontSize: fontSizeRatio.FONT_MIN,
              color: colors.BRAND_PRIMARY,
              paddingVertical: 3,
            }}>
            {htmlInstructions}
          </BrandText>
        </View>
        <View style={{flex: 1}} />
      </View>
    );
  };

  renderSubsteps = shortName => {
    return (
      <View style={styles.middleContainerOuter}>
        <View style={styles.middleContainer}>
          <Icon name="ic-buss" style={styles.iconVoud} />
          <Text style={{padding: 5, fontWeight: 'bold'}}>{shortName}</Text>
        </View>
        <Image
          source={require('../../../../../images/transport-cards/city-plus.png')}
          resizeMode="contain"
          style={styles.imgCityPlus}
        />
      </View>
    );
  };

  renderLineContent = item => {
    const {lineNumber, description, arriveTime, totalTripTime, status} = item;
    return (
      <View style={{flexDirection: 'column'}}>
        {this.renderTitle(description, arriveTime, totalTripTime, status)}
        <View style={{flexDirection: 'row'}}>
          <View style={styles.walkStyle}>
            <Icon name="ic-walk" style={styles.iconVoud} />
          </View>
          <View style={styles.blueLine} />
          {this.renderSubsteps(lineNumber)}
          <View style={styles.blueLine} />
          <View style={styles.walkStyle}>
            <Icon name="ic-walk" style={styles.iconVoud} />
          </View>
        </View>
      </View>
    );
  };

  render() {
    const {item, onPress, isLast} = this.props;
    return (
      <TouchableNative
        onPress={onPress}
        style={styles.resultSearchRouteListItem}>
        <View
          style={StyleSheet.flatten([
            styles.itemWrapper,
            isLast ? styles.noBorder : {},
          ])}>
          {this.renderLineContent(item)}
          <View style={{flex: 1}} />
        </View>
      </TouchableNative>
    );
  }
}

export default ResultSearchRouteListItem;
