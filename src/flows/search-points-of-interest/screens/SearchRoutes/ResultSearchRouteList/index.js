import React, {Component} from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import VoudText from '../../../../../components/VoudText';
import ContentPlaceholder from '../../../../../components/ContentPlaceholder';
import {colors} from '../../../../../styles';
import RequestError from '../../../../../components/RequestError';
import ResultSearchRouteListItem from './ResultSearchRouteListItem';
import {GetFontSizeRatio} from '../../../../../utils/font-size';
import {ScrollView} from 'react-native-gesture-handler';

const fontSizeRatio = GetFontSizeRatio();

class ResultSearchRouteList extends Component {
  constructor(props) {
    super(props);
  }

  _renderResultSearchRouteListItem = ({item, index}) => {
    const {
      routeSearch: {data},
      onToggleRouteInfoModal,
    } = this.props;
    const routes = data;
    return (
      <ResultSearchRouteListItem
        onPress={() => onToggleRouteInfoModal(item)}
        item={item}
        isLast={index === routes.length - 1}
      />
    );
  };

  render() {
    const {routeSearch, onRetry} = this.props;
    const {data, isFetching, error} = routeSearch;

    const isNotToConsider = data && data.origin ? true : false;

    let routes = [];

    if (!isNotToConsider) {
      routes = Object.keys(data).map(function(key) {
        return data[key];
      });
    }

    const isEmptyList = routes.length === 0;

    if (isFetching) {
      return (
        <View style={styles.routeListContainer}>
          <VoudText style={styles.listTitle}>Sugestões de rotas</VoudText>
          <View style={styles.placeholderContainer}>
            <ContentPlaceholder
              duration={1000}
              style={styles.placeholderLeft}
            />
            <ContentPlaceholder
              duration={1000}
              style={styles.placeholderMiddle}
            />
            <View style={styles.placeholderWrapperRight}>
              <ContentPlaceholder
                duration={1000}
                style={styles.placeholderRightTop}
              />
              <ContentPlaceholder
                duration={1000}
                style={styles.placeholderRightBottom}
              />
            </View>
          </View>
        </View>
      );
    }
    if (!isFetching && !isEmptyList && error === '') {
      routes.pop();

      return (
        <View style={styles.routeListContainer}>
          <View style={styles.routeListHeader}>
            <VoudText style={styles.listTitle}>Sugestões de rotas</VoudText>
          </View>
          <FlatList
            data={routes}
            renderItem={this._renderResultSearchRouteListItem}
            keyExtractor={(item, index) => index}
          />
        </View>
      );
    }
    if (!isFetching && error !== '') {
      return (
        <View style={styles.routeListContainer}>
          <RequestError
            style={StyleSheet.flatten([
              styles.requestErrorContainer,
              isEmptyList ? styles.mt0 : {},
            ])}
            error={`Endereço não encontrado`}
            onRetry={onRetry}
          />
        </View>
      );
    }
    return (
      <View
        style={StyleSheet.flatten([
          styles.routeListContainer,
          styles.emptyState,
        ])}>
        <VoudText style={styles.emptyStateText}>
          Não encontramos nenhuma rota para sua jornada.
        </VoudText>
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  requestErrorContainer: {
    marginHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mt0: {
    marginTop: 0,
  },
  emptyState: {
    justifyContent: 'center',
    alignContent: 'center',
    paddingVertical: 48,
    paddingHorizontal: 16,
  },
  emptyStateText: {
    textAlign: 'center',
    fontSize: fontSizeRatio.FONT_AVERAGE,
    color: colors.GRAY,
  },
  listTitle: {
    fontSize: fontSizeRatio.FONT_MIN,
    color: colors.GRAY,
    fontWeight: 'bold',
    paddingTop: 10,
    paddingBottom: 6,
    paddingLeft: 16,
  },
  routeListContainer: {
    flex: 100,
    backgroundColor: '#fff',
  },
  routeListHeader: {
    backgroundColor: colors.GRAY_LIGHTEST,
  },
  placeholderLeft: {
    flex: 2,
    height: 32,
    marginRight: 4,
  },
  placeholderMiddle: {
    flex: 1,
    height: 32,
    marginRight: 4,
  },
  placeholderWrapperRight: {
    flex: 7,
  },
  placeholderRightTop: {
    flex: 2,
    marginBottom: 4,
  },
  placeholderRightBottom: {
    flex: 1,
  },
  placeholderContainer: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 5,
    paddingBottom: 8,
    flexDirection: 'row',
  },
});

export default ResultSearchRouteList;
