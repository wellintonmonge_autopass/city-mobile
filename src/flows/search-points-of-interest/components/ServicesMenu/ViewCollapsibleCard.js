import React, {Component, Fragment} from 'react';
import {Animated, StyleSheet} from 'react-native';

import CollapsibleCard from '../../components/CollapsibleCard';
import TransportCardBadgesList from '../../../../components/TransportCardBadgesList';
import {colors} from '../../../../styles';
import PendingRegistration from './PendingRegistration';
import VoudText from '../../../../components/VoudText';

class ViewCollapsibleCard extends Component {
  constructor(props) {
    super(props);
  }
  ///DO THE CODE HERE

  renderCollapsible = animValue => {
    const {
      cardsData,
      cardListUi,
      onViewCardDetails,
      onAddCard,
      onRetryCardList,
      hasAlert,
      onCompleteRegistration,
      serviceMenuType,
      onScooterHelp,
      onScooterReport,
      onScooterBegin,
    } = this.props;

    const animStyle = {
      opacity: animValue.interpolate(
        {
          inputRange: [0, 1],
          outputRange: [1, 0],
          extrapolate: 'clamp',
        },
        {
          useNativeDriver: true,
        },
      ),
    };

    if (hasAlert)
      return (
        <Animated.View
          style={StyleSheet.flatten([styles.collapsibleContent, animStyle])}>
          <PendingRegistration
            onCompleteRegistration={onCompleteRegistration}
            serviceMenuType={serviceMenuType}
          />
        </Animated.View>
      );

    if (serviceMenuType !== 'card') {
      return (
        <Animated.View style={StyleSheet.flatten([animStyle])}>
          <Fragment>
            {/* <View style={[styles.containerPatinete, {height: 30}]}>
              <VoudText style={styles.listTitle}> {'VouD Patinete'}</VoudText>
              <IconButton
                iconName="help-outline"
                style={{padding: 20}}
                iconStyle={{marginTop: -20}}
                onPress={onScooterHelp}
              />
            </View> */}
            {/* <ScooterMenu
              onScooterHelp={onScooterHelp}
              onScooterReport={onScooterReport}
              onScooterBegin={onScooterBegin}
            /> */}
          </Fragment>
        </Animated.View>
      );
    } else {
      return (
        <Animated.View
          style={StyleSheet.flatten([styles.collapsibleContent, animStyle])}>
          <Fragment>
            <VoudText style={styles.listTitle}>
              {'Cartões de transporte'}
            </VoudText>
            <TransportCardBadgesList
              data={cardsData}
              ui={cardListUi}
              onCardPress={onViewCardDetails}
              onAddPress={onAddCard}
              onRetry={onRetryCardList}
              style={styles.listContainer}
              contentStyle={styles.badgesListContent}
            />
          </Fragment>
        </Animated.View>
      );
    }
  };

  render() {
    const {
      style,
      renderStatic,
      onToggle,
      isServiceMenuCollapsed,
      serviceMenuType,
      showProgressBar,
    } = this.props;
    return (
      <CollapsibleCard
        startCollapsed={isServiceMenuCollapsed}
        type={serviceMenuType}
        renderCollapsibleContent={this.renderCollapsible}
        renderFixedContent={renderStatic}
        style={style}
        onToggle={onToggle}
        showProgressBar={showProgressBar}
      />
    );
  }
}

const styles = StyleSheet.create({
  collapsibleContent: {
    paddingVertical: 8,
  },
  staticContent: {
    paddingTop: 16,
    paddingBottom: 8,
    height: 142,
  },
  hr: {
    alignSelf: 'stretch',
    height: 1,
    marginHorizontal: 16,
    backgroundColor: colors.GRAY_LIGHTER,
  },
  listTitle: {
    marginHorizontal: 16,
    fontSize: 12,
    fontWeight: 'bold',
    color: colors.GRAY_DARK,
  },
  listContainer: {
    alignSelf: 'stretch',
  },
  badgesListContent: {
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  listContainerContent: {
    flexGrow: 1,
    flexDirection: 'row',
    paddingVertical: 8,
    paddingLeft: 16,
    paddingRight: 8,
  },
  actionTile: {
    width: 90,
    height: 84,
    borderWidth: 1,
    borderColor: colors.BRAND_PRIMARY,
    borderRadius: 4,
    marginRight: 8,
  },
  containerPatinete: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    marginRight: 0,
  },
});

export default ViewCollapsibleCard;
