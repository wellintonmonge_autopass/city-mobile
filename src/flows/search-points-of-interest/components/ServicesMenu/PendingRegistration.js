import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';

import {colors} from '../../../../styles';
import VoudText from '../../../../components/VoudText';
import GradientButton from '../../components/GradientButton';

class PendingRegistration extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (this.props.serviceMenuType !== 'card') {
      return (
        <View style={styles.container}>
          <VoudText style={styles.title}>Voud Patinete</VoudText>
          <VoudText style={styles.description}>
            <VoudText>Para iniciar uma corrida,</VoudText>
            <VoudText
              style={
                styles.bold
              }>{` confirme seu telefone e e-mail.`}</VoudText>
          </VoudText>
          <GradientButton
            text="Completar cadastro"
            onPress={this.props.onCompleteRegistration}
          />
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <VoudText style={styles.title}>Cartões de transporte</VoudText>
          <VoudText style={styles.description}>
            <VoudText>
              Para cadastrar e carregar cartões de transporte,
            </VoudText>
            <VoudText
              style={
                styles.bold
              }>{` confirme seu telefone e e-mail.`}</VoudText>
          </VoudText>
          <GradientButton
            text="Completar cadastro"
            onPress={this.props.onCompleteRegistration}
          />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    paddingBottom: 8,
  },
  title: {
    fontSize: 12,
    lineHeight: 18,
    fontWeight: 'bold',
    color: colors.GRAY_DARK,
    opacity: 0.5,
    paddingBottom: 16,
  },
  description: {
    fontSize: 12,
    color: colors.GRAY_DARK,
    marginBottom: 16,
    textAlign: 'center',
  },
  bold: {
    fontWeight: 'bold',
  },
});

export default PendingRegistration;
