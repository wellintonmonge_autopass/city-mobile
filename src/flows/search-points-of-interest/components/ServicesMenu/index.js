import React, {PureComponent, Fragment} from 'react';
import {
  View,
  StyleSheet,
  Animated,
  Platform,
  Easing,
  ScrollView,
  Alert,
  BackHandler,
} from 'react-native';
import {connect} from 'react-redux';

import PropTypes from 'prop-types';

// redux
import {routeNames} from '../../../../shared/route-names';
import {navigateToRoute, navigateFromHome} from '../../../../redux/nav';
import CardButton from '../../components/CardButton';
import {
  // getUserPhoneValidRecharge,
  getCardListUI,
  getHasConfigError,
  getBOMCreditValueRange,
  getIssuerConfig,
  getTransportCards,
  getHasProfileAlerts,
  getConfigContentUI,
} from '../../../../redux/selectors';
import {colors} from '../../../../styles';
import {GATrackEvent, GAEventParams} from '../../../../shared/analytics';
import {
  viewDetails,
  fetchCardList,
  fetchCardListRealBalance,
} from '../../../../redux/transport-card';
import Icon from '../../../../components/Icon';
import VoudTouchableOpacity from '../../../../components/TouchableOpacity';
import VoudText from '../../../../components/VoudText';
import TouchableNative from '../../../../components/TouchableNative';
import ViewCollapsibleCard from './ViewCollapsibleCard';
import PurchaseCollapsibleCard from './PurchaseCollapsibleCard';
// import {goToPurchaseCredit} from '../../../../utils/purchase-credit';
import BrandText from '../../../../components/BrandText';
import {setIsServiceMenuCollapsed} from '../../../../redux/config';
import {configurePosition} from '../../../../redux/profile';
import {showLocationWithoutPermissionAlert} from '../../../../utils/geolocation';
import ButtonIcon from '../ButtonIcon';

import {getJson, saveItem} from '../../../../utils/async-storage';
import {asyncStorageKeys} from '../../../../redux/init';
// import {fetchAuthRide} from '../../../scooter/store/ducks/authRide';
// import {showToast, toastStyles} from '../../../../redux/toast';
// import ScooterUsageTerms from '../../../scooter/screens/ScooterUsageTerms';

class ServicesMenu extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isDiscountModalVisible: false,
      // isInsuranceIcatuModalVisible: false,
      isAcceptUsageTerms: false,
    };
    this._CollapsibleCard = null;
    this._toastAnimation = new Animated.Value(0);
    this._toastPosition = this._toastAnimation.interpolate({
      inputRange: [0, 1],
      outputRange: [-70, 0],
    });
  }

  componentWillMount() {
    // this._toggleInsuranceIcatuModal();
    BackHandler.addEventListener('hardwareBackPress', this._backHandler);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler);
  }

  _backHandler = () => {
    const {nav} = this.props;

    if (nav === undefined) {
      const {
        isDiscountModalVisible,
        // isInsuranceIcatuModalVisible
      } = this.state;
      if (!isDiscountModalVisible) return false;

      this.setState({
        isDiscountModalVisible: !this.state.isDiscountModalVisible,
        // isInsuranceIcatuModalVisible: !this.state.isInsuranceIcatuModalVisible,
      });
    }
    return true;
  };

  componentWillUpdate(nextProps) {
    const {parentRequestsUi, positionError} = nextProps;

    if (parentRequestsUi.error || positionError) {
      this._showErrorToast();
    }

    if (!parentRequestsUi.error && !positionError) {
      this._dismissErrorToast();
    }
  }

  _refreshCardList = () => {
    this.props.dispatch(fetchCardList());
  };

  _fetchCardListRealBalance = cardId => {
    this.props.dispatch(fetchCardListRealBalance());
  };

  _navigateToRoute = (routeName, eventLabel = '') => {
    const {geolocPermGranted} = this.props;
    const {
      categories: {BUTTON},
      actions: {CLICK},
    } = GAEventParams;
    GATrackEvent(BUTTON, CLICK, eventLabel);

    if (routeName === routeNames.MOBILITY_SERVICES && !geolocPermGranted) {
      showLocationWithoutPermissionAlert();
      return;
    }

    this.props.dispatch(navigateFromHome(routeName));
  };

  _goToAddCard = issuerType => {
    const {
      categories: {BUTTON},
      actions: {CLICK},
      labels: {ADD_CARD},
    } = GAEventParams;
    GATrackEvent(BUTTON, CLICK, ADD_CARD);
    this.props.dispatch(navigateToRoute(routeNames.ADD_CARD, {issuerType}));
  };

  _viewCardDetails = ({id}) => {
    const {dispatch} = this.props;
    const {
      categories: {BUTTON},
      actions: {CLICK},
      labels: {TRANSPORT_CARD_DETAILS},
    } = GAEventParams;

    GATrackEvent(BUTTON, CLICK, TRANSPORT_CARD_DETAILS);
    dispatch(viewDetails(id));
    dispatch(navigateToRoute(routeNames.CARD_DETAILS));
  };

  //   _goToPurchaseCredit = cardData => {
  //     const {
  //       dispatch,
  //       hasConfigError,
  //       bomCreditValueRange: {minCreditValue},
  //       issuerConfig,
  //       configUi,
  //     } = this.props;

  //     goToPurchaseCredit(
  //       dispatch,
  //       cardData,
  //       minCreditValue,
  //       issuerConfig,
  //       hasConfigError,
  //       configUi,
  //     );
  //   };

  _toggleDiscountModal = () => {
    this.setState({
      isDiscountModalVisible: !this.state.isDiscountModalVisible,
    });
  };

  // _toggleInsuranceIcatuModal = () => {
  //   this.setState({
  //     isInsuranceIcatuModalVisible: !this.state.isInsuranceIcatuModalVisible,
  //   });
  // };

  // render methods
  renderStatic = (animValueCard, animValueScooter, toggle) => {
    return (
      <Fragment>
        <View style={styles.hr} />
        <View style={styles.staticContent}>
          <BrandText style={styles.listTitle}>Serviços</BrandText>
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            style={styles.listContainer}
            contentContainerStyle={styles.listContainerContent}>
            {this._renderCardButtons(animValueCard, animValueScooter, toggle)}
          </ScrollView>
        </View>
      </Fragment>
    );
  };

  _navigateToPhoneRecharge() {
    const {phoneIsConfirmed} = this.props;

    if (phoneIsConfirmed) {
      this._navigateToRoute(routeNames.PHONE_RECHARGE, 'RecargaCelularMenu');
      return;
    }
    Alert.alert(
      'Recarga de Celular',
      'O telefone celular não foi confirmado via SMS, favor confirmar!',
    );
  }

  _handlerScooInfo = async () => {
    const isRead = await getJson('readScooInfo');

    if (!isRead) {
      await saveItem('readScooInfo', 'true');
      this._handleScooterHelp();
    }
  };

  _renderCardButtons = (animValueCard, animValueScooter, toggle) => {
    const transportCardButtonAnimStyle = {
      width: animValueCard.interpolate(
        {
          inputRange: [0, 1],
          outputRange: [0, 98], // 98 = CardButton width (90) + margin (8)
          extrapolate: 'clamp',
        },
        {
          useNativeDriver: true,
        },
      ),
      opacity: animValueCard.interpolate(
        {
          inputRange: [0, 1],
          outputRange: [0, 1],
          extrapolate: 'clamp',
        },
        {
          useNativeDriver: true,
        },
      ),
    };
    const scooterCardButtonAnimStyle = {
      width: animValueScooter.interpolate(
        {
          inputRange: [0, 1],
          outputRange: [0, 98], // 98 = CardButton width (90) + margin (8)
          extrapolate: 'clamp',
        },
        {
          useNativeDriver: true,
        },
      ),
      opacity: animValueScooter.interpolate(
        {
          inputRange: [0, 1],
          outputRange: [0, 1],
          extrapolate: 'clamp',
        },
        {
          useNativeDriver: true,
        },
      ),
    };

    const {
      categories: {BUTTON},
      actions: {CLICK},
      labels: {
        MENU_REPORT,
        MENU_HELP,
        MENU_MOBILITY_SERVICES,
        MENU_SCOOTER_SERVICES,
        MENU_SMART_PURCHASE,
        MENU_TRANSPORT_CARDS,
        MENU_PHONE_RECHARGE,
      },
    } = GAEventParams;

    return (
      <Fragment>
        {__DEV__ && (
          <CardButton
            iconName="bus"
            text="Force Error"
            onPress={() => {
              throw new Error('Error');
            }}
            style={styles.cardButton}
          />
        )}

        {this.props.purchaseCredit ? null : (
          <Animated.View style={transportCardButtonAnimStyle}>
            <CardButton
              iconName="bus"
              text="Cartões de transporte"
              onPress={() => {
                GATrackEvent(BUTTON, CLICK, MENU_TRANSPORT_CARDS);
                toggle('card');
              }}
              style={styles.cardButton}
            />
          </Animated.View>
        )}

        <CardButton
          isNew
          iconName="ic-sorteio"
          text="VouD Sorteio"
          style={styles.cardButton}
          onPress={() => {
            const {
              categories: {BUTTON},
              actions: {CLICK},
              labels: {MENU_DISCOUNT_PAGE},
            } = GAEventParams;
            GATrackEvent(BUTTON, CLICK, MENU_DISCOUNT_PAGE);
            // this._toggleInsuranceIcatuModal();
          }}
        />

        <CardButton
          iconName="car"
          text="VouD Carro"
          style={styles.cardButton}
          onPress={() =>
            this._navigateToRoute(
              routeNames.MOBILITY_SERVICES,
              MENU_MOBILITY_SERVICES,
            )
          }
        />
        <CardButton
          iconName="car"
          text="VouD Carro"
          style={styles.cardButton}
          onPress={() =>
            this._navigateToRoute(
              routeNames.MOBILITY_SERVICES,
              MENU_MOBILITY_SERVICES,
            )
          }
        />

        <CardButton
          iconName="mobile"
          text="Recarga de celular"
          style={styles.cardButton}
          onPress={() =>
            this._navigateToRoute(
              routeNames.PHONE_RECHARGE,
              MENU_PHONE_RECHARGE,
            )
          }
        />
        <CardButton
          iconName="history"
          text="Compra programada"
          style={styles.cardButton}
          onPress={() =>
            this._navigateToRoute(
              routeNames.SMART_PURCHASE,
              MENU_SMART_PURCHASE,
            )
          }
        />
        <CardButton
          iconName="wallet-giftcard"
          text="Clube de Descontos"
          style={styles.cardButton}
          onPress={() => {
            const {
              categories: {BUTTON},
              actions: {CLICK},
              labels: {MENU_DISCOUNT_PAGE},
            } = GAEventParams;
            GATrackEvent(BUTTON, CLICK, MENU_DISCOUNT_PAGE);
            this._toggleDiscountModal();
          }}
        />

        <CardButton
          iconName="feedback"
          text="Denúncias"
          style={styles.cardButton}
          onPress={() => this._navigateToRoute(routeNames.REPORTS, MENU_REPORT)}
        />
        <CardButton
          iconName="help-outline"
          text="FAQ"
          style={styles.cardButton}
          onPress={() => this._navigateToRoute(routeNames.HELP, MENU_HELP)}
        />
      </Fragment>
    );
  };

  // error toast
  _showErrorToast = () => {
    Animated.timing(this._toastAnimation, {
      toValue: 1,
      duration: 500,
      easing: Easing.inOut(Easing.ease),
    }).start();
  };

  _dismissErrorToast = () => {
    Animated.timing(this._toastAnimation, {
      toValue: 0,
      duration: 500,
      easing: Easing.inOut(Easing.ease),
    }).start();
  };

  _hasError = () =>
    this.props.parentRequestsUi.error || this.props.positionError;

  _renderToast = () => {
    const {parentRequestsUi, positionError} = this.props;
    const parentRequestsError = parentRequestsUi.error;
    return (
      <Animated.View
        style={StyleSheet.flatten([
          styles.toastErrorContainer,
          {
            opacity: this._toastAnimation,
            bottom: this._toastPosition,
          },
        ])}
        pointerEvents={this._hasError() ? 'auto' : 'none'}>
        <TouchableNative
          style={styles.toastError}
          onPress={this._handleErrorToastPress}>
          <VoudText
            style={styles.toastErrorText}
            numberOfLines={2}
            ellipsizeMode="tail">
            {positionError ? (
              positionError
            ) : parentRequestsError ? (
              <Fragment>
                <VoudText>{`${parentRequestsError}${
                  parentRequestsError.endsWith('.') ? ' ' : '. '
                }`}</VoudText>
                <VoudText style={{fontWeight: 'bold'}}>
                  Tentar novamente.
                </VoudText>
              </Fragment>
            ) : (
              ''
            )}
          </VoudText>
        </TouchableNative>
      </Animated.View>
    );
  };

  _handleErrorToastPress = () => {
    const {
      dispatch,
      onParentRequestsRetry,
      positionError,
      onCenterLocation,
    } = this.props;
    this._dismissErrorToast();

    if (positionError) {
      dispatch(configurePosition(onCenterLocation));
    } else {
      if (onParentRequestsRetry) onParentRequestsRetry();
    }
  };

  _handleCardCollapse = (toggle, type) => {
    const {dispatch, purchaseCredit, onToggle,} = this.props;

    if (!purchaseCredit) {
      this.props.dispatch(setIsServiceMenuCollapsed({toggle, type}));
    }
    if (onToggle) onToggle(toggle);
  };

  _handleScooterHelp = () => {
    //GATrackEvent(BUTTON, CLICK, ADD_CARD);
    this.props.dispatch(navigateToRoute(routeNames.SCOOTER_INFO));
  };

  _handleScooterReport = () => {
    //GATrackEvent(BUTTON, CLICK, ADD_CARD);
    this.props.dispatch(navigateToRoute(routeNames.SCOOTER_REPORT));
  };

  _handlerQRCODE = async () => {
    const scooterAcceptTerm = await this._renderAcceptUsageTerms();
    if (scooterAcceptTerm) {
      this.props.dispatch(navigateToRoute(routeNames.SCOOTER_SERVICES_QRCODE));
    } else {
      this.setState({isAcceptUsageTerms: true});
    }
  };

  _handlerOnPressClose = () => {
    this.setState({isAcceptUsageTerms: false});
  };

  _handlerOnPressAccpetTerms = () => {
    this.setState({isAcceptUsageTerms: false});
    this.props.dispatch(navigateToRoute(routeNames.SCOOTER_SERVICES_QRCODE));
  };

  _renderToken = async () => {
    return await getJson(asyncStorageKeys.scooterToken);
  };

  _renderAcceptUsageTerms = async () => {
    return await getJson(asyncStorageKeys.scooterAcceptUsageTerms);
  };

  //   _handleScooterBegin = async () => {
  //     const {profileData} = this.props;
  //     const scooterToken = await this._renderToken();

  //     if (!scooterToken) {
  //       this.props
  //         .dispatch(
  //           fetchAuthRide(profileData.name, profileData.cpf, profileData.email),
  //         )
  //         .then(this._handlerQRCODE)
  //         .catch(error => {
  //           this.props.dispatch(showToast(error.message, toastStyles.ERROR));
  //         });
  //     } else {
  //       this._handlerQRCODE();
  //     }
  //   };

  render() {
    const {
      style,
      onCenterLocation,
      parentRequestsUi,
      purchaseCredit,
      cardsData,
      cardListUi,
      isServiceMenuCollapsed,
      serviceMenuType,
      isGettingPosition,
      hasAlert,
      onCompleteRegistration,
      shouldRenderCenterOnLocationButton = true,
      renderOnTop,
      shouldRenderToast = true,
      isMenuService,
      onBycicleLocation,
      isTembici,
      dispatch,
    } = this.props;
    const CollapsibleCardComponent = purchaseCredit
      ? PurchaseCollapsibleCard
      : ViewCollapsibleCard;
    const showProgressBar =
      !this._hasError() && (isGettingPosition || parentRequestsUi.isFetching);

    const iconBikeBus = !isTembici ? 'ic-tembici' : 'bus';

    return (
      <Fragment>
        <View
          style={StyleSheet.flatten([styles.container, style])}
          pointerEvents="box-none">
          {shouldRenderCenterOnLocationButton && (
            <Animated.View
              style={StyleSheet.flatten([
                styles.centerPosition,
                {bottom: this._toastPosition},
              ])}
              pointerEvents="box-none">
              <ButtonIcon
                style={{paddingBottom: 10}}
                onPress={() => {
                  this._CollapsibleCard && this._CollapsibleCard.toggle();
                  onBycicleLocation();
                }}
                icon={iconBikeBus}
                badge={false}
              />
              <VoudTouchableOpacity
                onPress={onCenterLocation}
                style={styles.centerPositionIconContainer}>
                <Icon
                  style={styles.centerPositionIcon}
                  name="my-location"
                  size={20}
                />
              </VoudTouchableOpacity>
            </Animated.View>
          )}
          {shouldRenderToast && this._renderToast()}
          {renderOnTop && renderOnTop()}
          <CollapsibleCardComponent
            style={styles.collapsible}
            cardListUi={cardListUi}
            cardsData={cardsData}
            renderStatic={this.renderStatic}
            onViewCardDetails={this._viewCardDetails}
            onAddCard={this._goToAddCard}
            onRetryCardList={this._refreshCardList}
            // onPurchaseCredit={this._goToPurchaseCredit}
            // onScooterHelp={this._handleScooterHelp}
            // onScooterReport={this._handleScooterReport}
            // onScooterBegin={this._handleScooterBegin}
            isServiceMenuCollapsed={isServiceMenuCollapsed}
            onToggle={this._handleCardCollapse}
            showProgressBar={showProgressBar}
            hasAlert={hasAlert}
            onCompleteRegistration={onCompleteRegistration}
            isMenuService={isMenuService}
            serviceMenuType={serviceMenuType}
            onRef={ref => {
              this._CollapsibleCard = ref;
            }}
          />
        </View>
      </Fragment>
    );
  }
}

export const COLLAPSIBLE_HEIGHT = 126;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
  },
  collapsible: {
    alignSelf: 'stretch',
  },
  collapsibleContent: {
    paddingVertical: 8,
  },
  staticContent: {
    paddingTop: 16,
    paddingBottom: 8,
    height: 142,
  },
  hr: {
    alignSelf: 'stretch',
    height: 1,
    marginHorizontal: 16,
    backgroundColor: colors.GRAY_LIGHTER,
  },
  listTitle: {
    marginHorizontal: 16,
    fontSize: 12,
    fontWeight: 'bold',
    color: colors.GRAY_DARK,
  },
  listContainer: {
    alignSelf: 'stretch',
  },
  badgesListContent: {
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  listContainerContent: {
    flexGrow: 1,
    flexDirection: 'row',
    paddingVertical: 8,
    paddingLeft: 16,
    paddingRight: 8,
  },
  actionTile: {
    width: 90,
    height: 84,
    borderWidth: 1,
    borderColor: colors.BRAND_PRIMARY,
    borderRadius: 4,
    marginRight: 8,
  },
  cardButton: {
    marginRight: 8,
  },
  // buy button
  buyButtonContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    padding: 16,
  },
  buyButtonTouchable: {
    borderRadius: 18,
  },
  buyButton: {
    alignItems: 'center',
    padding: 10,
    borderRadius: 18,
  },
  buyButtonText: {
    fontSize: 12,
    lineHeight: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
  },
  centerPosition: {
    alignItems: 'flex-end',
    // compensate shadow clipping on android with padding
    marginBottom: Platform.OS === 'android' ? 8 : 16,
    marginRight: Platform.OS === 'android' ? 8 : 16,
    padding: Platform.OS === 'android' ? 8 : 0,
  },
  centerPositionIconContainer: {
    backgroundColor: 'white',
    borderRadius: 40 / 2,
    padding: 8,
    elevation: 5,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {
      height: 2,
      width: 0,
    },
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },

  tembiciIconContainer: {
    backgroundColor: 'white',
    borderRadius: 60,
    padding: 8,
    elevation: 5,
    shadowColor: 'rgba(0, 0, 0, 0.25)',
    shadowOpacity: 1,
    shadowRadius: 6,
    shadowOffset: {
      height: 2,
      width: 0,
    },
  },

  toastErrorContainer: {
    backgroundColor: colors.BRAND_ERROR,
    height: 80,
    marginBottom: -10,
    bottom: -70,
  },
  toastError: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 10,
  },
  toastErrorText: {
    color: 'white',
    paddingHorizontal: 16,
  },
  debugView: {
    backgroundColor: 'black',
    flexDirection: 'row',
  },
  centerPositionIcon: {
    color: colors.BRAND_PRIMARY,
  },
});

ServicesMenu.propTypes = {
  onRef: PropTypes.func,
};
ServicesMenu.defaultProps = {};

function mapStateToProps(state) {
  return {
    // phoneIsConfirmed: getUserPhoneValidRecharge(state),
    cardsData: getTransportCards(state),
    cardListUi: getCardListUI(state),
    // pointsOfInterest: state.pointsOfInterest.data,
    hasConfigError: getHasConfigError(state),
    bomCreditValueRange: getBOMCreditValueRange(state),
    issuerConfig: getIssuerConfig(state),
    isGettingPosition: state.profile.position.isGettingPosition,
    hasAlert: getHasProfileAlerts(state),
    configUi: getConfigContentUI(state),
    geolocPermGranted: state.profile.position.geolocPermGranted,
    profileData: state.profile.data,
  };
}

export default connect(mapStateToProps)(ServicesMenu);
