// NPM Imports
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  FlatList,
  Text,
} from 'react-native';
import {connect} from 'react-redux';

// VouD Imports
import TouchableNative from '../../../../components/TouchableNative';
import VoudText from '../../../../components/VoudText';
import {colors} from '../../../../styles';
import {issuerTypes} from '../../../../redux/transport-card';
import {TYPES} from '../SwitchTypeSearch';
import {formatNumber} from '../../../../utils/parsers-formaters';
import {voudCapitalizeLetters} from '../../../../utils/string-util';
import Icon from '../../../../components/Icon';
import {GetFontSizeRatio} from '../../../../utils/font-size';

// PropTypes
const propTypes = {
  onPress: PropTypes.func.isRequired,
};

// component
const bomLogoImg = require('../../../../images/transport-cards/city-plus.png');
const buLogoImg = require('../../../../images/transport-cards/city-plus.png');

// Component
class ResultSearchListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isIntineraryVisible: false,
    };
    this._scrollView = null;
  }
  _renderPointsImage = data => {
    if (!data.acceptedCards) {
      return null;
    }
    return data.acceptedCards.map((el, index) => (
      <Image
        key={el}
        source={el === issuerTypes.BU ? buLogoImg : bomLogoImg}
        style={
          data.acceptedCards.length === 1
            ? styles.bigImg
            : StyleSheet.flatten([
                styles.img,
                el === issuerTypes.BU ? styles.buImg : styles.bomImg,
                index === data.acceptedCards.length - 1 ? styles.mb0 : {},
                ,
              ])
        }
        resizeMode="contain"
      />
    ));
  };

  _renderItineraryLines = ({item, index}) => {
    if (!item) {
      return null;
    }
    const isFirst = index === 0;
    const styleFirstLine = isFirst ? {backgroundColor: 'transparent'} : {};
    const desc = `${item.name.split('-')[0]}`;
    const styleConditionalCircle = !isFirst
      ? {backgroundColor: colors.GRAY}
      : {};
    return (
      <View style={styles.additionalContent}>
        <View style={[styles.containerCircle]}>
          <View style={[styles.line, styleFirstLine]} />
          <View style={styles.circle}>
            <View style={[styles.circleInner, styleConditionalCircle]} />
          </View>
          <View style={[styles.line]} />
        </View>
        <VoudText style={{margin: 5}}>{desc}</VoudText>
      </View>
    );
  };

  _onPressTransportLine = () => {
    const {isIntineraryVisible} = this.state;
    if (this._scrollView) {
      this._scrollView.scrollResponderScrollToEnd({animated: true});
    }
    this.setState({isIntineraryVisible: !isIntineraryVisible});
  };
  _renderFooterComponent = item => {
    const desc = `${item.name.split('-')[0]}`;
    return (
      <View style={styles.additionalContent}>
        <View style={[styles.containerCircle]}>
          <View style={[styles.line]} />
          <View style={styles.circle}>
            <View
              style={[
                styles.circleInner,
                {backgroundColor: colors.BRAND_ERROR},
              ]}
            />
          </View>
          <View style={[styles.line, {backgroundColor: 'transparent'}]} />
        </View>
        <VoudText style={{margin: 5}}>{desc}</VoudText>
      </View>
    );
  };

  _renderTransportLinesItem = () => {
    const {data, onPress, type} = this.props;
    const {isIntineraryVisible} = this.state;
    const description = data && data.description ? data.description : '';
    const lineNumber = data && data.lineNumber ? data.lineNumber : '';

    const onPressItermediary =
      type === TYPES.TRANSPORT && type !== TYPES.BUS_STOP
        ? this._onPressTransportLine
        : onPress;

    return (
      <View>
        <TouchableNative
          onPress={onPressItermediary}
          key={data.id}
          style={styles.resultSearchListItem}>
          <View style={styles.itemWrapper}>
            <View style={styles.transportLineContentLeft}>
              <VoudText style={styles.lineNumber}>{lineNumber}</VoudText>
              <VoudText style={styles.title}>
                {voudCapitalizeLetters(description)}
              </VoudText>
            </View>
            <View style={styles.contentRight}>
              {this._renderPointsImage(data)}
              {isIntineraryVisible ? (
                <Icon name="angle-up-solid" size={24} />
              ) : (
                <Icon name="angle-down-solid" size={24} />
              )}
            </View>
          </View>
        </TouchableNative>
        {isIntineraryVisible && (
          <ScrollView
            ref={ref => (this._scrollView = ref)}
            scrollEnabled={true}
            style={styles.padHorizontalDefault}>
            <View style={styles.flexOne}>
              <View style={styles.padHorizontalDefault}>
                <Icon Icon="bus" style={{color: colors.BRAND_PRIMARY_DARKER}} />
                <VoudText style={styles.fontBold}>
                  Sentido ida (TD - TS)
                </VoudText>
              </View>
              <FlatList
                listKey={'itineraryForward'}
                style={{}}
                keyExtractor={item => item.id.toString()}
                renderItem={this._renderItineraryLines}
                ListFooterComponent={this._renderFooterComponent.bind(
                  this,
                  data.itineraryForward.pop(),
                )}
                data={data.itineraryForward}
              />
            </View>
            <View style={styles.flexOne}>
              <View style={styles.padHorizontalDefault}>
                <Icon Icon="bus" style={{color: colors.BRAND_PRIMARY_DARKER}} />
                <VoudText style={styles.fontBold}>
                  Sentido volta (TD - TS)
                </VoudText>
              </View>
              <FlatList
                style={{}}
                listKey={'itineraryBackward'}
                keyExtractor={item => item.id.toString()}
                renderItem={this._renderItineraryLines}
                ListFooterComponent={this._renderFooterComponent.bind(
                  this,
                  data.itineraryBackward.pop(),
                )}
                data={data.itineraryBackward}

              />
            </View>
          </ScrollView>
        )}
      </View>
    );
  };

  _renderRechargePointsItem = () => {
    const {data, onPress, type} = this.props;
    const name = data && data.name ? data.name : '';
    const address =
      data && data.description
        ? data.description
        : data.shortAddress
        ? data.shortAddress
        : '';
    const distance = data && data.distance ? data.distance : '';

    return (
      <TouchableNative
        onPress={onPress}
        disabled={type === TYPES.BUS_STOP}
        key={data.id}
        style={styles.resultSearchListItem}>
        <View style={styles.itemWrapper}>
          <View style={styles.rechargePointContainerLeft}>
            <VoudText style={styles.rechargePointName}>
              {voudCapitalizeLetters(name)}
            </VoudText>
            <VoudText style={styles.rechargePointAddress}>
              {voudCapitalizeLetters(address)}
            </VoudText>
          </View>
          <View style={styles.contentRight}>
            <VoudText style={styles.rechargePointDistance}>{`${formatNumber(
              distance,
              1,
            )} km`}</VoudText>
            {this._renderPointsImage(data)}
          </View>
        </View>
      </TouchableNative>
    );
  };

  render() {
    const {type} = this.props;
    return type === TYPES.TRANSPORT
      ? this._renderTransportLinesItem()
      : this._renderRechargePointsItem();
  }
}

ResultSearchListItem.propTypes = propTypes;

// Styles
const styles = StyleSheet.create({
  resultSearchListItem: {
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 56,
    paddingHorizontal: 16,
    backgroundColor: 'white',
  },
  itemWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: colors.GRAY_LIGHTER,
    flexDirection: 'row',
    alignItems: 'center',
    minHeight: 56,
  },
  contentRight: {
    alignItems: 'center',
  },
  title: {
    flex: 1,
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.GRAY_DARK,
  },
  img: {
    width: 20,
    marginBottom: 8,
  },
  bigImg: {
    width: 24,
    height: 24,
  },
  bomImg: {
    height: 10,
  },
  buImg: {
    height: 19,
  },
  transportLineContentLeft: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 12,
    marginRight: 16,
  },
  lineNumber: {
    fontSize: GetFontSizeRatio().FONT_AVERAGE,
    fontWeight: 'bold',
    color: colors.GRAY_DARK,
    width: 60,
    marginRight: 8,
  },
  rechargePointContainerLeft: {
    flex: 1,
    paddingVertical: 12,
    marginRight: 16,
  },
  rechargePointName: {
    fontSize: 12,
    color: colors.GRAY_DARK,
    lineHeight: 16,
    fontWeight: '600',
  },
  rechargePointAddress: {
    fontSize: 10,
    color: colors.GRAY,
    lineHeight: 16,
  },
  rechargePointDistance: {
    fontSize: 12,
    color: colors.GRAY_DARK,
    lineHeight: 16,
    fontWeight: '600',
  },
  mb0: {
    marginBottom: 0,
  },
  additionalContent: {
    paddingHorizontal: 5,
    alignContent: 'flex-start',
    flexDirection: 'row',
  },
  containerCircle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  circleInner: {
    width: 5,
    height: 5,
    borderRadius: 3,
    alignSelf: 'center',
    backgroundColor: colors.BRAND_SUCCESS,
  },
  circle: {
    width: 10,
    height: 10,
    borderColor: '#000',
    borderWidth: 1,
    borderRadius: 6,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    flex: 1,
    height: 12,
    alignSelf: 'center',
    width: 1,
    backgroundColor: '#000',
  },
  flexOne: {
    flex: 1,
  },
  fontBold: {
    fontWeight: 'bold',
  },
  padHorizontalDefault: {
    paddingHorizontal: 16,
  },
});

const mapStateToProps = state => {
  return {
    isLoadingIninerary: state.mobilityServices.isLoadingItems,
  };
};

export default connect(mapStateToProps)(ResultSearchListItem);
