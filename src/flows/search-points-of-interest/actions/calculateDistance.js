import {angleToRad, hav} from './utils';
export function calculateDistance(lat, long, lat2, long2, precision = 1) {
  const earthRadius = 6371;
  const adjustment = 1.2;
  lat = angleToRad(lat);
  lat2 = angleToRad(lat2);
  const latTheta = lat - lat2;
  const longTheta = angleToRad(long - long2);
  const d =
    2 *
    earthRadius *
    Math.asin(
      Math.sqrt(
        hav(latTheta) + Math.cos(lat) * Math.cos(lat2) * hav(longTheta),
      ),
    );
  return (d * adjustment).toFixed(precision);
}
