// NPM
import {googleMapsRequest} from '../../../google-maps/services';

// Action types
import actionTypes from '../../mobility-services/actions/types';
import {uuidv4} from '../../../utils/uuid';

// consts
const BASIC_FIELDS = 'address_components,formatted_address,geometry,name';

// Synchronous actions

// Place search
function _fetchPlaceSearch() {
  return {type: actionTypes.FETCH_PLACE_SEARCH};
}

function _fetchPlaceSearchSuccess(response) {
  return {type: actionTypes.FETCH_PLACE_SEARCH_SUCCESS, response};
}

function _fetchPlaceSearchFailure(error) {
  return {type: actionTypes.FETCH_PLACE_SEARCH_FAILURE, error};
}

export function clearPlaceSearch() {
  return {type: actionTypes.PLACE_SEARCH_CLEAR};
}

// Predictions
function _fetchPredictions() {
  return {type: actionTypes.FETCH_PREDICTIONS};
}

function _fetchPredictionsSuccess(response) {
  return {type: actionTypes.FETCH_PREDICTIONS_SUCCESS, response};
}

function _fetchPredictionsFailure(error) {
  return {type: actionTypes.FETCH_PREDICTIONS_FAILURE, error};
}

export function clearPredictions() {
  return {type: actionTypes.PREDICTIONS_CLEAR};
}

export function setPredictionsSessionToken(sessionToken) {
  return {type: actionTypes.SET_PREDICTIONS_SESSION_TOKEN, sessionToken};
}

// Place details
function _fetchPlaceDetails() {
  return {type: actionTypes.FETCH_PLACE_DETAILS};
}

function _fetchPlaceDetailsSuccess(response, isOrigin, isDestination) {
  return {
    type: actionTypes.FETCH_PLACE_DETAILS_SUCCESS,
    response,
    isOrigin,
    isDestination,
  };
}

function _fetchPlaceDetailsFailure(error) {
  return {type: actionTypes.FETCH_PLACE_DETAILS_FAILURE, error};
}

export function clearPlaceDetails() {
  return {type: actionTypes.PLACE_DETAILS_CLEAR};
}

// Change directions
export function searchChangeDirections() {
  return {type: actionTypes.SEARCH_CHANGE_DIRECTIONS};
}

// Thunks

export const normalizeQuery = query => {
  const querySplited = query.split(',');
  return querySplited
    .map(item => item.slice(0, 9))
    .reduce((acc, cur) => acc.concat(`,${cur}`));
};

export function fetchPlaceTextSearch(query = '', radius = 1) {
  return async dispatch => {
    dispatch(_fetchPlaceSearch());

    try {
      const params = {
        query,
        radius,
      };

      const response = await googleMapsRequest(
        '/place/textsearch/json',
        params,
      );

      if (__DEV__) console.tron.log(response);

      dispatch(_fetchPlaceSearchSuccess(response));

      return response;
    } catch (error) {
      if (__DEV__) console.tron.log(error.message);
      dispatch(_fetchPlaceSearchFailure(error.message));
      throw error;
    }
  };
}

export function fetchPredictions(input) {
  return async (dispatch, getState) => {
    dispatch(_fetchPredictions());
    try {
      let sessiontoken = getState().mobilityServices.predictions.sessionToken;

      if (sessiontoken === '') {
        sessiontoken = uuidv4();
        dispatch(setPredictionsSessionToken(sessiontoken));
      }

      const params = {
        input: input || '',
        sessiontoken,
      };

      const {latitude, longitude} = getState().profile.position;

      if (latitude && longitude) {
        params.location = `${latitude},${longitude}`;
      }

      const response = await googleMapsRequest(
        '/place/autocomplete/json',
        params,
      );

      if (__DEV__) console.tron.log(response);
      dispatch(_fetchPredictionsSuccess(response));
      return response;
    } catch (error) {
      if (__DEV__) console.tron.log(error.message);
      dispatch(_fetchPredictionsFailure(error.message));
      throw error;
    }
  };
}

export function fetchPlaceDetails(
  placeid,
  isOrigin = false,
  isDestination = false,
) {
  return async (dispatch, getState) => {
    dispatch(_fetchPlaceDetails());

    try {
      const sessiontoken = getState().mobilityServices.predictions.sessionToken;
      const params = {
        placeid,
        fields: BASIC_FIELDS,
        ...(sessiontoken !== '' ? {sessiontoken} : {}),
      };

      const response = await googleMapsRequest('/place/details/json', params);

      if (__DEV__) console.tron.log(response);

      dispatch(_fetchPlaceDetailsSuccess(response, isOrigin, isDestination));
      return response;
    } catch (error) {
      if (__DEV__) console.tron.log(error.message);
      dispatch(_fetchPlaceDetailsFailure(error.message));
      throw error;
    }
  };
}

export const setMarkendTimeDistance = payload => {
  return {type: actionTypes.SET_MARKEND_TIME_DISTANCE, payload};
};

export const setMarkstartTimeDistance = payload => {
  return {type: actionTypes.SET_MARKSTART_TIME_DISTANCE, payload};
};
