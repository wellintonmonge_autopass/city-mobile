// NPM imports
import React, {Component} from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Linking,
  Platform,
  PixelRatio,
} from 'react-native';
import {NavigationActions} from 'react-navigation';
import {connect} from 'react-redux';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import PropTypes from 'prop-types';

// VouD imports
import Header, {headerActionTypes} from '../../../components/Header';
import Button from '../../../components/Button';
import Loader from '../../../components/Loader';
import BrandText from '../../../components/BrandText';
import {colors} from '../../../styles';
import {calculateDistance} from '../../../utils/haversine';
import {GAEventParams, GATrackEvent} from '../../../shared/analytics';
import {
  fetchBusLine,
  fetchNextPoint,
  fetchArrivalForecast,
  fetchNextPointClear,
  fetchArrivalForecastClear,
  fetchBusLineSearchClear,
} from '../actions';
import mapStyle from '../../../google-maps/map-style';
import UserMarker from '../../../google-maps/UserMarker';
import BusMarker from '../../../google-maps/BusMarker';
import StopMarker from '../../../google-maps/StopMarker';
import {NextPointPropTypes} from '../prop-types/next-point';
import {PointPropTypes} from '../prop-types/point';

export const routeTypes = {
  SUBWAY: '1',
  TRAIN: '2',
  BUS: '3',
};

export const searchTypes = {
  TRANSPORT_LINES: 'TRANSPORT_LINES',
  RECHARGE_POINTS: 'RECHARGE_POINTS',
};

export const pointsOfInterestTypes = {
  BUS: 'BUS',
  STOP_BUS: 'BUS_STOP',
  STOP_TRAIN: 'TRAIN_STATION',
  STOP_SUBWAY: 'SUBWAY_STATION',
  RECHARGE: 'RECHARGE_POINT',
};

export const DEFAULT_LAT_DELTA = 0.025;
export const DEFAULT_LNG_DELTA = 0.025;

// Props
const propTypes = {
  // From params
  navigation: PropTypes.shape({
    state: PropTypes.shape({
      params: PropTypes.shape({
        search: PropTypes.shape(PointPropTypes),
      }),
    }),
  }),

  // Redux
  fetchBusLine: PropTypes.func,
  fetchNextPoint: PropTypes.func,
  fetchArrivalForecast: PropTypes.func,
  fetchNextPointClear: PropTypes.func,
  fetchArrivalForecastClear: PropTypes.func,
  fetchBusLineSearchClear: PropTypes.func,
  goBack: PropTypes.func,
  arrivalForecast: PropTypes.shape({
    isFetching: PropTypes.bool,
    error: PropTypes.string,
    data: PropTypes.objectOf(PointPropTypes),
  }),
  busLineSearch: PropTypes.shape({
    isFetching: PropTypes.bool,
    error: PropTypes.string,
    data: PropTypes.arrayOf(PropTypes.shape(PointPropTypes)),
  }),
  nextPoint: PropTypes.shape({
    isFetching: PropTypes.bool,
    error: PropTypes.string,
    data: PropTypes.objectOf(NextPointPropTypes),
  }),
  userPosition: PropTypes.shape({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    latitudeDelta: PropTypes.number,
    longitudeDelta: PropTypes.number,
  }),
};

const defaultProps = {};

// Screen component
class BusLineDetailsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shouldRenderMap: false,
    };

    this._mapRef = null;
    this._mapHeight = 0;
    this._centerDelay = null;
    this._markerRefs = {};
  }

  componentDidMount() {
    this._fetchTransportDetails();
  }

  componentWillUnmount() {
    this.props.fetchNextPointClear();
    this.props.fetchArrivalForecastClear();
    this.props.fetchBusLineSearchClear();

    clearInterval(this._centerDelay);
  }

  // ---------------------------------------------------------------------------------------------------

  _onMapReady = () => {};

  _hasNextPoint = () => Object.keys(this.props.nextPoint.data).length > 0;

  _hasBusLine = () => Object.keys(this.props.busLineSearch.data).length > 0;

  _hasArrivalForecast = () =>
    Object.keys(this.props.arrivalForecast.data).length > 0;

  _fitToCoordinates = (pos, otherPos) => {
    let edgePadding = {
      top: 125,
      bottom: 20,
      left: 75,
      right: 75,
    };

    if (Platform.OS === 'android') {
      edgePadding = {
        top: PixelRatio.getPixelSizeForLayoutSize(edgePadding.top),
        right: PixelRatio.getPixelSizeForLayoutSize(edgePadding.right),
        left: PixelRatio.getPixelSizeForLayoutSize(edgePadding.left),
        bottom: PixelRatio.getPixelSizeForLayoutSize(edgePadding.bottom),
      };
    }

    if (this._mapRef) {
      this._mapRef.fitToCoordinates([pos, otherPos], {
        edgePadding,
        animated: true,
      });
    }
  };

  _getNearestPoint = nextPoint =>
    nextPoint && nextPoint.data.points
      ? nextPoint.data.points.find(el => el.isNearPoint)
      : null;

  // public method, to be used in a parent component
  _centerLocationByNearest = () => {
    const {
      userPosition,
      nextPoint: {data},
    } = this.props;

    if (this._hasNextPoint()) {
      const nearestStation = data.points.find(el => el.isNearPoint);

      if (nearestStation) {
        this._fitToCoordinates(userPosition, {
          latitude: nearestStation.positionLatitude,
          longitude: nearestStation.positionLongitude,
        });

        // Requested
        if (
          (this._markerRefs[nearestStation.id] &&
            data.pointType !== pointsOfInterestTypes.STOP_BUS) ||
          this._hasArrivalForecast()
        ) {
          this._markerRefs[nearestStation.id].showCallout();
        }
      } else {
        if (__DEV__) console.tron.log('nearestStation is null', true);
      }
    }
  };

  _renderPointsOfInterest = () => {
    const {
      userPosition,
      nextPoint,
      arrivalForecast,
      busLineSearch,
    } = this.props;

    if (this._hasNextPoint()) {
      let forecastData = null;

      if (
        this._hasArrivalForecast() &&
        nextPoint.data.routeType === routeTypes.BUS
      ) {
        forecastData = {
          nearestStation: nextPoint.data.points.find(el => el.isNearPoint),
          arrivalTime:
            arrivalForecast.data.busList &&
            arrivalForecast.data.busList.length > 0
              ? arrivalForecast.data.busList[0].estimatedArrivalTime
              : null,
        };
      }
      const hasForecastData =
        forecastData && forecastData.nearestStation && forecastData.arrivalTime;
      const pointsData = [...nextPoint.data.points, ...busLineSearch.data];

      return pointsData.map(item => {
        const isStopWithForecast =
          hasForecastData && item.id === forecastData.nearestStation.id;
        const line = item.lines[0] ? item.lines[0] : null;

        return item.pointType === pointsOfInterestTypes.BUS ? (
          <BusMarker
            key={item.externalId}
            coordinate={{
              latitude: item.positionLatitude,
              longitude: item.positionLongitude,
            }}
            lineColor={line ? line.lineColor : ''}
            acceptedCards={item.acceptedCards}
            lineNumber={line ? line.lineNumber : ''}
            lineOrigin={line ? line.origin : ''}
            lineDestiny={line ? line.destiny : ''}
          />
        ) : (
          <StopMarker
            key={item.id}
            userPosition={userPosition}
            coordinate={{
              latitude: item.positionLatitude,
              longitude: item.positionLongitude,
            }}
            type={item.pointType}
            stopName={item.name}
            lines={item.lines}
            timeToArrival={isStopWithForecast ? forecastData.arrivalTime : null}
            markerRef={el => {
              this._markerRefs[item.id] = el;
            }}
          />
        );
      });
    }
    return null;
  };

  _centerLocation = () => {
    if (this.mapRef && this.mapRef.centerLocation) {
      this.mapRef.centerLocation();
    }
  };

  _centerWithDelay = () => {
    if (this._centerDelay) clearInterval(this._centerDelay);
    this._centerDelay = setTimeout(() => {
      this._centerLocation();
    }, 750);
  };

  _fetchArrivalForecast = async externalId => {
    const {
      navigation: {
        state: {
          params: {search},
        },
      },
    } = this.props;

    try {
      await this.props.fetchArrivalForecast(search.lineNumber, externalId);
    } catch (error) {
      if (__DEV__) console.tron.log(error.message);
    }

    this._centerWithDelay();
  };

  _fetchTransportDetails = async () => {
    const {
      navigation: {
        state: {
          params: {search},
        },
      },
    } = this.props;

    // if (search.pointType !== searchTypes.TRANSPORT_LINES) return;

    const response = await this.props.fetchNextPoint(search.id);

    if (response && response.payload && search.routeType === routeTypes.BUS) {
      //const nearestStation = response.payload.points.find(el => el.isNearPoint);
      // this._fetchArrivalForecast(nearestStation.externalId);

      if (!this._hasBusLine()) {
        await this.props.fetchBusLine(search.lineNumber);
      }

      this.setState(
        {
          shouldRenderMap: true,
        },
        () => {
          this._centerLocationByNearest();
        },
      );
    } else {
      this._centerWithDelay();
    }
  };

  _back = () => {
    this.props.goBack();
  };

  _openRouteOnMaps = () => {
    const {nextPoint} = this.props;

    const nearestPoint = this._getNearestPoint(nextPoint);

    const queryUrl = `https://www.google.com/maps/dir/?api=1&destination=${encodeURIComponent(
      nearestPoint.latitude,
    )},${encodeURIComponent(nearestPoint.longitude)}`;

    Linking.canOpenURL(queryUrl)
      .then(supported => {
        if (!supported) {
          if (__DEV__) console.tron.log("Can't handle url: " + queryUrl, true);
        } else {
          const {
            categories: {BUTTON},
            actions: {CLICK},
            labels: {SERVICE_POINT_ROUTE},
          } = GAEventParams;
          GATrackEvent(
            BUTTON,
            CLICK,
            `${SERVICE_POINT_ROUTE} ${nearestPoint.name}`,
          );
          return Linking.openURL(queryUrl);
        }
      })
      .catch(err => {
        if (__DEV__)
          console.tron.log('An error occurred: ' + err.message, true);
      });
  };

  _renderUserMarker = () => {
    return <UserMarker coordinate={this.props.userPosition} />;
  };

  _calculateDistance = nearestPoint => {
    const {userPosition} = this.props;

    const distance = calculateDistance(
      nearestPoint.positionLatitude,
      nearestPoint.positionLongitude,
      userPosition.latitude,
      userPosition.longitude,
    );

    if (distance > 1) {
      return `${distance}km`;
    }

    // if under 1km, convert to meters
    return `${distance * 1000}m`;
  };

  render() {
    const {
      navigation: {
        state: {
          params: {search},
        },
      },
      userPosition,
      nextPoint,
    } = this.props;

    const nearestPoint = this._getNearestPoint(nextPoint);

    return (
      <View style={styles.mainContainer}>
        <Header
          title={search.name}
          left={{
            type: headerActionTypes.CLOSE,
            onPress: this._back,
          }}
        />
        <ScrollView>
          <View>
            <MapView
              ref={ref => {
                this._mapRef = ref;
              }}
              style={styles.mapView}
              provider={PROVIDER_GOOGLE}
              minZoomLevel={9}
              maxZoomLevel={18}
              customMapStyle={mapStyle}
              moveOnMarkerPress={false}
              onMapReady={this._onMapReady}
              onMarkerPress={this._animateToMarker}
              onLayout={event => {
                this._mapHeight = event.nativeEvent.layout.height;
              }}
              initialRegion={userPosition}>
              {this._renderPointsOfInterest()}
              {this._renderUserMarker()}
            </MapView>
            {!this.state.shouldRenderMap ? (
              <View style={styles.mapLoading}>
                <Loader text="Carregando mapa..." />
              </View>
            ) : null}
          </View>
          <View style={styles.main}>
            <BrandText style={styles.addressNumber}>
              {search.name} -
              <BrandText style={styles.addressDescription}>
                {' '}
                {search.description}
              </BrandText>
            </BrandText>
          </View>
          {nearestPoint !== null && (
            <React.Fragment>
              <View style={styles.containerNextStop}>
                <BrandText style={styles.nextStop}>
                  Parada mais próxima
                </BrandText>
              </View>
              <View style={styles.main}>
                <BrandText style={styles.location}>
                  {nearestPoint.name}
                  <BrandText style={styles.locationDistance}>
                    ({this._calculateDistance(nearestPoint)})
                  </BrandText>
                </BrandText>
              </View>
            </React.Fragment>
          )}
        </ScrollView>
        <View style={styles.traceRouteContainer}>
          <Button
            onPress={this._openRouteOnMaps}
            icon="md-map"
            outline
            outlineText="white"
            style={styles.traceRouteBtn}>
            Traçar rota
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  mapView: {
    justifyContent: 'center',
    height: 200,
    // backgroundColor: colors.GRAY_LIGHTER
  },
  mapLoading: {
    position: 'absolute',
    justifyContent: 'center',
    height: 200,
    width: '100%',
    backgroundColor: colors.GRAY_LIGHTER,
  },
  main: {
    padding: 16,
    borderColor: colors.GRAY_LIGHTER,
  },
  addressNumber: {
    color: colors.BRAND_PRIMARY,
    fontSize: 14,
    fontWeight: 'bold',
    lineHeight: 20,
  },
  addressDescription: {
    color: colors.GRAY,
    fontSize: 14,
    lineHeight: 20,
  },

  containerNextStop: {
    backgroundColor: colors.GRAY_LIGHTER,
    padding: 16,
  },
  nextStop: {
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.BRAND_PRIMARY,
  },
  location: {
    fontSize: 13,
    fontWeight: 'bold',
    color: colors.BRAND_PRIMARY,
    marginBottom: 8,
  },
  locationDistance: {
    fontSize: 13,
    color: colors.GRAY,
  },
  prediction: {
    fontSize: 13,
    color: colors.GRAY,
  },
  predictionTime: {
    fontWeight: 'bold',
  },

  traceRouteContainer: {
    backgroundColor: colors.BRAND_PRIMARY,
  },
  traceRouteBtn: {
    margin: 16,
  },
});

// Redux
const mapStateToProps = state => {
  return {
    arrivalForecast: state.busLines.arrivalForecast,
    busLineSearch: state.busLines.busLineSearch,
    nextPoint: state.busLines.nextPoint,
    userPosition: {
      latitude:
        state.profile.position.latitude || state.profile.position.defaultLat,
      longitude:
        state.profile.position.longitude || state.profile.position.defaultLng,
      latitudeDelta: DEFAULT_LAT_DELTA,
      longitudeDelta: DEFAULT_LNG_DELTA,
    },
  };
};

const mapDispatchToProps = {
  fetchBusLine,
  fetchNextPoint,
  fetchArrivalForecast,
  fetchNextPointClear,
  fetchArrivalForecastClear,
  fetchBusLineSearchClear,
  goBack: NavigationActions.back,
};

BusLineDetailsView.propTypes = propTypes;
BusLineDetailsView.defaultProps = defaultProps;

export const BusLineDetails = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BusLineDetailsView);
