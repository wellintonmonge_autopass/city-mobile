// NPM liraries
import React, {Component} from 'react';
import {View, StyleSheet, ScrollView, FlatList} from 'react-native';
import PropTypes from 'prop-types';
import {reduxForm, Field, formValueSelector} from 'redux-form';
import {connect} from 'react-redux';

// VouD imports
import {openMenu} from '../../../redux/menu';
import {colors} from '../../../styles/constants';
import Header, {headerActionTypes} from '../../../components/Header';
import TextField from '../../../components/TextField';
import BusLineItem from '../components/busLineItem';
import {fetchTransportLineSearch} from '../actions';
import {
  getDefaultRequestPositionConfig,
  getCurrentPosition,
} from '../../../utils/geolocation';
import {setPosition} from '../../../redux/profile';
import FadeInView from '../../../components/FadeInView';
import Loader from '../../../components/Loader';
import RequestFeedback from '../../../components/RequestFeedback';
import {routeNames} from '../../../shared/route-names';
import {navigateToRoute} from '../../../redux/nav';
import {PointPropTypes} from '../prop-types/point';

const reduxFormName = 'busLinesSearch';

// Props
const propTypes = {
  // Redux
  dispatch: PropTypes.func,
  searchQuery: PropTypes.string,
  transportLinesSearch: PropTypes.shape({
    isFetching: PropTypes.bool,
    error: PropTypes.string,
    reachedEnd: PropTypes.bool,
    page: PropTypes.number,
    data: PropTypes.arrayOf(PropTypes.shape(PointPropTypes)),
    lastRequestUid: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    currentSearchTerm: PropTypes.string,
  }).isRequired,
};

const defaultProps = {};

// create a component
class BusLineView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasSearched: false,
    };
  }

  componentDidMount() {
    this._getUserLocation(
      getDefaultRequestPositionConfig(true),
      true,
      getDefaultRequestPositionConfig(false),
    );
  }

  _getUserLocation = (config, shouldRetry = false, retryConfig) => {
    const {dispatch} = this.props;

    getCurrentPosition(config, shouldRetry, retryConfig)
      .then(({latitude, longitude}) => {
        dispatch(setPosition(latitude, longitude));
      })
      .catch(() => {});
  };

  _openMenu = () => {
    const {dispatch} = this.props;

    dispatch(openMenu());
  };

  _search = query => {
    const {dispatch, searchQuery} = this.props;

    this.setState({
      hasSearched: true,
    });

    dispatch(fetchTransportLineSearch(query || searchQuery));
  };

  _renderItems = ({item}) => (
    <BusLineItem
      onPress={() => {
        this.props.dispatch(
          navigateToRoute(routeNames.BUS_LINE_DETAILS, {
            search: item,
          }),
        );
      }}
      number={item.name}
      description={item.description}
    />
  );

  render() {
    const {transportLinesSearch} = this.props;
    const {hasSearched} = this.state;

    return (
      <View style={styles.container}>
        <Header
          title="Linhas de ônibus"
          left={{
            type: headerActionTypes.MENU,
            onPress: this._openMenu,
          }}
        />
        <View style={styles.scrollContainer}>
          <View style={styles.serachFieldContainer}>
            <Field
              name="searchQuery"
              props={{
                label: 'Pesquise por nome ou nº da linha',
                returnKeyType: 'search',
                maxLength: 100,
                onSubmitEditing: () => this._search(),
                autoCorrect: false,
                editable: !transportLinesSearch.isFetching,
              }}
              component={TextField}
            />
          </View>
          <View style={styles.container}>
            {transportLinesSearch.error.length > 0 && hasSearched && (
              <FadeInView style={styles.main}>
                <RequestFeedback
                  style={styles.requestFeedbackContainer}
                  errorMessage={transportLinesSearch.error}
                  onRetry={this._search}
                  retryMessage="Tentar novamente"
                />
              </FadeInView>
            )}
            {!transportLinesSearch.isFetching &&
              transportLinesSearch.error.length === 0 &&
              hasSearched &&
              transportLinesSearch.data.length === 0 && (
                <FadeInView style={styles.main}>
                  <RequestFeedback
                    style={styles.requestFeedbackContainer}
                    emptyMessage={`Nenhuma linha encontrada por "${
                      transportLinesSearch.currentSearchTerm
                    }"`}
                  />
                </FadeInView>
              )}
            {transportLinesSearch.isFetching && (
              <FadeInView style={styles.main}>
                <Loader text="Carregando..." style={styles.loader} />
              </FadeInView>
            )}
            {!transportLinesSearch.isFetching &&
              transportLinesSearch.error.length === 0 &&
              hasSearched &&
              transportLinesSearch.data.length > 0 && (
                <FlatList
                  contentContainerStyle={styles.mainContainer}
                  data={transportLinesSearch.data}
                  renderItem={this._renderItems}
                  keyExtractor={item => item.id}
                />
              )}
          </View>
        </View>
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  main: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollContainer: {
    flex: 1,
  },
  serachFieldContainer: {
    paddingHorizontal: 16,
    paddingBottom: 16,
    borderBottomWidth: 1,
    borderColor: colors.GRAY_LIGHTER,
  },
  loader: {},
  requestFeedbackContainer: {
    alignItems: 'center',
  },
});

BusLineView.propTypes = propTypes;
BusLineView.defaultProps = defaultProps;

// Redux
const mapStateToProps = state => ({
  initialValues: {
    searchQuery: '',
  },
  searchQuery: formValueSelector(reduxFormName)(state, 'searchQuery'),
  transportLinesSearch: state.busLines.transportLinesSearch,
});

export const BusLines = connect(mapStateToProps)(
  reduxForm({
    form: reduxFormName,
    destroyOnUnmount: true,
  })(BusLineView),
);
