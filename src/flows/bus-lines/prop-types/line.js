import PropTypes from 'prop-types';

export const LinePropTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  lineNumber: PropTypes.string,
  lineColor: PropTypes.string,
  textColor: PropTypes.string,
  externalId: PropTypes.string,
  routeType: PropTypes.string,
  description: PropTypes.string,
  company: PropTypes.string,
  acceptedCards: PropTypes.arrayOf(
    PropTypes.string,
  ),
  pathId: PropTypes.number,
  directionId: PropTypes.number,
  tripHeadsign: PropTypes.string,
};
