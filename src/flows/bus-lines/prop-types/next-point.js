import PropTypes from 'prop-types';

import { PointPropTypes } from './point'

export const NextPointPropTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  lineNumber: PropTypes.string,
  lineColor: PropTypes.string,
  textColor: PropTypes.string,
  externalId: PropTypes.string,
  routeType: PropTypes.number,
  description: PropTypes.string,
  points: PropTypes.arrayOf(
    PropTypes.shape(PointPropTypes),
  ),
  company: PropTypes.string,
};