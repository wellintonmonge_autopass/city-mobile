import PropTypes from 'prop-types';

import { LinePropTypes } from './line';

export const PointPropTypes = {
  id: PropTypes.number,
  name: PropTypes.string,
  positionLatitude: PropTypes.number,
  positionLongitude: PropTypes.number,
  company: PropTypes.string,
  acceptedCards: PropTypes.arrayOf(
    PropTypes.string,
  ),
  fare: PropTypes.number,
  lines: PropTypes.arrayOf(
    PropTypes.shape(LinePropTypes),
  ),
  pointType: PropTypes.string,
  externalId: PropTypes.string,
  description: PropTypes.string,
};