import actionTypes from './types';
import {voudRequest} from '../../../shared/services';
import {requestErrorHandler} from '../../../shared/request-error-handler';
import {uuidv4} from '../../../utils/uuid';

// Transport Line Search
function _fetchTransportLineSearch(searchTerm, uid) {
  return {type: actionTypes.FETCH_TRANSPORT_LINE_SEARCH, searchTerm, uid};
}

function _fetchTransportLineSearchSuccess(response, searchTerm) {
  return {
    type: actionTypes.FETCH_TRANSPORT_LINE_SEARCH_SUCCESS,
    response,
    searchTerm,
  };
}

function _fetchTransportLineSearchFailure(error) {
  return {type: actionTypes.FETCH_TRANSPORT_LINE_SEARCH_FAILURE, error};
}

export function fetchTransportLineSearchClear() {
  return {type: actionTypes.FETCH_TRANSPORT_LINE_SEARCH_CLEAR};
}

// Bus Line Search
function _fetchBusLineSearch() {
  return {type: actionTypes.FETCH_BUS_LINE_SEARCH};
}

function _fetchBusLineSearchSuccess(response) {
  return {type: actionTypes.FETCH_BUS_LINE_SEARCH_SUCCESS, response};
}

function _fetchBusLineSearchFailure(error) {
  return {type: actionTypes.FETCH_BUS_LINE_SEARCH_FAILURE, error};
}

export function fetchBusLineSearchClear() {
  return {type: actionTypes.FETCH_BUS_LINE_SEARCH_CLEAR};
}

// Next point
function _fetchNextPoint() {
  return {type: actionTypes.FETCH_NEXT_POINT};
}

function _fetchNextPointSuccess(response) {
  return {type: actionTypes.FETCH_NEXT_POINT_SUCCESS, response};
}

function _fetchNextPointFailure(error) {
  return {type: actionTypes.FETCH_NEXT_POINT_FAILURE, error};
}

export function fetchNextPointClear() {
  return {type: actionTypes.FETCH_NEXT_POINT_CLEAR};
}

// Arrival Forecast
function _fetchArrivalForecast() {
  return {type: actionTypes.FETCH_ARRIVAL_FORECAST};
}

function _fetchArrivalForecastSuccess(response) {
  return {type: actionTypes.FETCH_ARRIVAL_FORECAST_SUCCESS, response};
}

function _fetchArrivalForecastFailure(error) {
  return {type: actionTypes.FETCH_ARRIVAL_FORECAST_FAILURE, error};
}

export function fetchArrivalForecastClear() {
  return {type: actionTypes.FETCH_ARRIVAL_FORECAST_CLEAR};
}

// SEARCH BUS LINES
const ITEMS_PER_PAGE = 15;

export function fetchTransportLineSearch(searchTerm) {
  return async (dispatch, getState) => {
    // Generate and dispatch request unique id
    // this is used later in thunk to ignore
    // old requests that take longer to respond
    const requestUid = uuidv4();

    try {
      const {
        latitude,
        longitude,
        defaultLat,
        defaultLng,
      } = getState().profile.position;
      const {
        page,
        currentSearchTerm,
      } = getState().busLines.transportLinesSearch;
      const hasChangedSearch = searchTerm !== currentSearchTerm;
      const searchPage = hasChangedSearch ? 0 : page;

      if (hasChangedSearch) {
        dispatch(fetchTransportLineSearchClear());
      }

      const requestBody = {
        userLocation: `${latitude || defaultLat},${longitude || defaultLng}`,
        searchTerm,
        page: searchPage,
        size: ITEMS_PER_PAGE,
      };

      dispatch(_fetchTransportLineSearch(searchTerm, requestUid));

      const response = await voudRequest(
        '/mobility/list-lines',
        'POST',
        requestBody,
        true,
      );

      // compare this request UID to lastRequestUid on store,
      // if is different, a newer request has been made,
      // and the results of this one should be ignored
      if (
        getState().busLines.transportLinesSearch.lastRequestUid === requestUid
      ) {
        dispatch(_fetchTransportLineSearchSuccess(response, searchTerm));
      }
      return response;
    } catch (error) {
      if (__DEV__) console.tron.log(error.message);

      if (
        getState().busLines.transportLinesSearch.lastRequestUid !== requestUid
      )
        return;

      if (
        !requestErrorHandler(
          dispatch,
          error,
          _fetchTransportLineSearchFailure(error.message),
        )
      ) {
        throw error;
      }
    }
  };
}

// BUS LINE DETAILS

export function fetchBusLine(lineNumber) {
  return async dispatch => {
    try {
      const requestBody = {
        lineNumber,
      };

      dispatch(_fetchBusLineSearch());

      const response = await voudRequest(
        '/mobility/list-bus-line',
        'POST',
        requestBody,
        true,
      );
      dispatch(_fetchBusLineSearchSuccess(response));

      if (__DEV__) console.tron.log(response);

      return response;
    } catch (error) {
      if (__DEV__) console.tron.log(error.message);
      if (
        !requestErrorHandler(
          dispatch,
          error,
          _fetchBusLineSearchFailure(error.message),
        )
      ) {
        throw error;
      }
    }
  };
}

// Next point
export function fetchNextPoint(lineId) {
  return async (dispatch, getState) => {
    try {
      const {
        latitude,
        longitude,
        defaultLat,
        defaultLng,
      } = getState().profile.position;
      const requestBody = {
        userLocation: `${latitude || defaultLat},${longitude || defaultLng}`,
        lineId,
      };

      dispatch(_fetchNextPoint());

      const response = await voudRequest(
        '/mobility/next-point',
        'POST',
        requestBody,
        true,
      );
      dispatch(_fetchNextPointSuccess(response));
      return response;
    } catch (error) {
      if (__DEV__) console.tron.log(error.message);
      if (
        !requestErrorHandler(
          dispatch,
          error,
          _fetchNextPointFailure(error.message),
        )
      ) {
        throw error;
      }
    }
  };
}

export function fetchArrivalForecast(lineNumber, stopExternalId) {
  return async dispatch => {
    try {
      const requestBody = {
        lineNumber,
        stopExternalId,
      };

      dispatch(_fetchArrivalForecast());

      const response = await voudRequest(
        '/mobility/arrival-forecast',
        'POST',
        requestBody,
        true,
      );
      dispatch(_fetchArrivalForecastSuccess(response));
      return response;
    } catch (error) {
      if (__DEV__) console.tron.log(error.message);
      if (
        !requestErrorHandler(
          dispatch,
          error,
          _fetchArrivalForecastFailure(error.message),
        )
      ) {
        throw error;
      }
    }
  };
}
