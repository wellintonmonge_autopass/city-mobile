// VouD imports
import actionTypes from '../actions/types';

// Reducer
const initialState = {
  transportLinesSearch: {
    isFetching: false,
    error: '',
    reachedEnd: false,
    page: 0,
    data: [],
    currentSearchTerm: '',
    lastRequestUid: null,
  },
  busLineSearch: {
    isFetching: false,
    error: '',
    data: [],
  },
  nextPoint: {
    isFetching: false,
    error: '',
    data: {},
  },
  arrivalForecast: {
    isFetching: false,
    error: '',
    data: {},
  },
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    // transportLinesSearch
    case actionTypes.FETCH_TRANSPORT_LINE_SEARCH:
      return {
        ...state,
        transportLinesSearch: {
          ...state.transportLinesSearch,
          isFetching: true,
          error: '',
          currentSearchTerm: action.searchTerm,
          lastRequestUid: action.uid,
        },
      };

    case actionTypes.FETCH_TRANSPORT_LINE_SEARCH_SUCCESS:
      return {
        ...state,
        transportLinesSearch: {
          ...state.transportLinesSearch,
          isFetching: false,
          page: ++state.transportLinesSearch.page,
          data:
            action.response.payload.aaData &&
            state.transportLinesSearch.currentSearchTerm === action.searchTerm
              ? state.transportLinesSearch.data
                  .filter(
                    el =>
                      !action.response.payload.aaData.some(
                        curPurchase => el.id === curPurchase.id,
                      ),
                  )
                  .concat(action.response.payload.aaData)
              : state.transportLinesSearch.data,
          reachedEnd:
            ++state.transportLinesSearch.page >=
            action.response.payload.iTotalPages,
        },
      };

    case actionTypes.FETCH_TRANSPORT_LINE_SEARCH_FAILURE:
      return {
        ...state,
        transportLinesSearch: {
          ...state.transportLinesSearch,
          isFetching: false,
          error: action.error,
        },
      };

    case actionTypes.FETCH_TRANSPORT_LINE_SEARCH_CLEAR:
      return {
        ...state,
        transportLinesSearch: {
          ...initialState.transportLinesSearch,
        },
      };

    // busLineSearch
    case actionTypes.FETCH_BUS_LINE_SEARCH:
      return {
        ...state,
        busLineSearch: {
          ...state.busLineSearch,
          isFetching: true,
          error: '',
        },
      };

    case actionTypes.FETCH_BUS_LINE_SEARCH_SUCCESS:
      return {
        ...state,
        busLineSearch: {
          ...state.busLineSearch,
          isFetching: false,
          data: action.response.payload
            ? action.response.payload
            : state.busLineSearch.data,
        },
      };

    case actionTypes.FETCH_BUS_LINE_SEARCH_FAILURE:
      return {
        ...state,
        busLineSearch: {
          ...state.busLineSearch,
          isFetching: false,
          error: action.error,
        },
      };

    case actionTypes.FETCH_BUS_LINE_SEARCH_CLEAR:
      return {
        ...state,
        busLineSearch: {
          ...initialState.busLineSearch,
        },
      };

    // nextPoint
    case actionTypes.FETCH_NEXT_POINT:
      return {
        ...state,
        nextPoint: {
          ...state.nextPoint,
          isFetching: true,
          error: '',
        },
      };

    case actionTypes.FETCH_NEXT_POINT_SUCCESS:
      return {
        ...state,
        nextPoint: {
          ...state.nextPoint,
          isFetching: false,
          data: action.response.payload
            ? action.response.payload
            : state.nextPoint.data,
        },
      };

    case actionTypes.FETCH_NEXT_POINT_FAILURE:
      return {
        ...state,
        nextPoint: {
          ...state.nextPoint,
          isFetching: false,
          error: action.error,
        },
      };

    case actionTypes.FETCH_NEXT_POINT_CLEAR:
      return {
        ...state,
        nextPoint: {
          ...initialState.nextPoint,
        },
      };

    // arrivalForecast
    case actionTypes.FETCH_ARRIVAL_FORECAST:
      return {
        ...state,
        arrivalForecast: {
          ...state.arrivalForecast,
          isFetching: true,
          error: '',
        },
      };

    case actionTypes.FETCH_ARRIVAL_FORECAST_SUCCESS:
      return {
        ...state,
        arrivalForecast: {
          ...state.arrivalForecast,
          isFetching: false,
          data: action.response.payload
            ? action.response.payload
            : state.arrivalForecast.data,
        },
      };

    case actionTypes.FETCH_ARRIVAL_FORECAST_FAILURE:
      return {
        ...state,
        arrivalForecast: {
          ...state.arrivalForecast,
          isFetching: false,
          error: action.error,
        },
      };

    case actionTypes.FETCH_ARRIVAL_FORECAST_CLEAR:
      return {
        ...state,
        arrivalForecast: {
          ...initialState.arrivalForecast,
        },
      };

    // Default
    default:
      return state;
  }
}
