// NPM liraries
import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

// VouD imports
import TouchableNative from '../../../components/TouchableNative';
import { colors } from '../../../styles';
import BrandText from '../../../components/BrandText';
import Icon from '../../../components/Icon';

// Props
const propTypes = {
  number: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
  description: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

const defaultProps = {};

// Component
class BusLineItem extends Component {
  render() {
    const { onPress, number, description } = this.props;

    return (
      <TouchableNative
        style={styles.mainContainer}
        onPress={() => onPress(number)}
      >
        <View style={styles.container}>
          <View style={styles.iconContainer}>
            <Icon name="bus" style={styles.icon} />
          </View>
          <BrandText style={styles.busLabel} numberOfLines={2}>
            <BrandText style={styles.busNumber}>{number} - </BrandText>
            {description}
          </BrandText>
        </View>
      </TouchableNative>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconContainer: {
    width: 32,
    height: 32,
    backgroundColor: colors.BRAND_SECONDARY,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 32,
  },
  icon: {
    color: '#FFFFFF',
    fontSize: 24,
  },
  busLabel: {
    fontSize: 16,
    color: colors.GRAY,
    marginLeft: 16,
    flex: 1,
  },
  busNumber: {
    color: colors.BRAND_PRIMARY,
    fontWeight: 'bold',
  },
});

BusLineItem.propTypes = propTypes;
BusLineItem.defaultProps = defaultProps;

export default BusLineItem;