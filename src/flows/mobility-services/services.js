// Base URLs
// const VAH_BASE_URL = "http://test-api.voud.com.br:8090/v1"; // Autopass DEV
const VAH_BASE_URL = 'https://homolog-api.voud.com.br:8090/v1'; // Autopass HML
// const VAH_BASE_URL = "https://api.voud.com.br:8090/v1"; // Autopass PROD

const bearerToken =
  'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6ImRzYTg3ZHNhOTgmRCUmXiZEUyVeJkFEJUFTJiVqaEdnamhhc2Rnamhkc2FqaGcifQ.eyJpc3MiOiJodHRwOlwvXC9hcHB2YWguY29tIiwiYXVkIjoiaHR0cDpcL1wvYXBwdmFoLmNvbSIsImp0aSI6ImRzYTg3ZHNhOTgmRCUmXiZEUyVeJkFEJUFTJiVqaEdnamhhc2Rnamhkc2FqaGciLCJpYXQiOjE1NTQzODc2MjYsInVpZCI6NDA4NTc0LCJwaWQiOjExfQ.n5wuFiBOF2uruHvEi_LRjcNbRdVDQ88RCl5s8N2et0E';

/**
 * fetch wrapper, including headers configurations
 * @param {string} endpoint - service endopoint
 * @param {Object} requestBody
 */
const vahRequest = async (endpoint, requestBody) => {
  const config = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${bearerToken}`,
    },
  };

  if (requestBody) config.body = JSON.stringify(requestBody);

  const response = await fetch(`${VAH_BASE_URL}${endpoint}`, config);
  const responseJson = await response.json();

  if (!response.ok) {
    const errorMessage =
      responseJson && responseJson.length > 0 ? responseJson[0].Message : null;
    const error = new Error(errorMessage || response.status);
    error.response = response;
    throw error;
  }

  return responseJson;
};

export default vahRequest;
