import {
  FETCH_SCHOLAR_TICKETS,
  FETCH_SCHOLAR_TICKETS_SUCCESS,
  FETCH_SCHOLAR_TICKETS_FAILURE,
  SET_FORM_PICTURE,
  SET_DOC_PICTURE,
  SEND_REVALIDATION_FORM,
  SEND_REVALIDATION_FORM_SUCCESS,
  SEND_REVALIDATION_FORM_FAILURE,
} from './types';
import {voudRequest} from '../../../shared/services';
import {requestErrorHandler} from '../../../shared/request-error-handler';

const _fetchScholarTickets = () => {
  return {type: FETCH_SCHOLAR_TICKETS};
};
const _fetchScholarTicketsSuccess = payload => {
  return {type: FETCH_SCHOLAR_TICKETS_SUCCESS, payload};
};
const _fetchScholarTicketsFailure = error => {
  return {type: FETCH_SCHOLAR_TICKETS_FAILURE, error};
};

export const fetchScholarTickets = customerId => {
  return async dispatch => {
    try {
      dispatch(_fetchScholarTickets());
      const response = await voudRequest(
        '/student-travel-pass/revalidation/',
        'GET',
        null,
        true,
        {
          customer: customerId,
        },
      );
      dispatch(_fetchScholarTicketsSuccess(response));
      return response;
    } catch (error) {
      if (__DEV__) {
        console.tron.log(error.message);
      }
      if (
        !requestErrorHandler(
          dispatch,
          error,
          _fetchScholarTicketsFailure(error.message),
        )
      ) {
        throw error;
      }
    }
  };
};

export const setFormPicture = payload => {
  return {type: SET_FORM_PICTURE, payload};
};
export const setDocPicture = payload => {
  return {type: SET_DOC_PICTURE, payload};
};

const _sendFormRevalidation = () => {
  return {type: SEND_REVALIDATION_FORM};
};
const _sendFormRevalidationSuccess = payload => {
  return {type: SEND_REVALIDATION_FORM_SUCCESS, payload};
};
const _sendFormRevalidationFailure = error => {
  return {type: SEND_REVALIDATION_FORM_FAILURE, error};
};

export const sendFormRevalidation = dataForm => {
  const docExtension = `image/${dataForm.documentPicture.uri.split('.').pop()}`;
  const formExtension = `image/${dataForm.formPicture.uri.split('.').pop()}`;

  const requestBody = {
    studentName: dataForm.studentName,
    cpf: dataForm.cpf,
    travelPassNumber: dataForm.travelPassNumber,
    photoIdentification: {
      data: `data:${docExtension};base64,${dataForm.documentPicture.base64}`,
      mediaType: docExtension,
    },
    revalidationForm: {
      data: `data:${formExtension};base64,${dataForm.formPicture.base64}`,
      mediaType: formExtension,
    },
  };
  return async dispatch => {
    try {
      dispatch(_sendFormRevalidation());
      const response = await voudRequest(
        '/student-travel-pass/revalidation/',
        'POST',
        requestBody,
        true,
      );
      dispatch(_sendFormRevalidationSuccess(response));
      return response;
    } catch (error) {
      if (__DEV__) {
        console.tron.log(error.message);
      }
      if (
        !requestErrorHandler(
          dispatch,
          error,
          _sendFormRevalidationFailure(error.message),
        )
      ) {
        throw error;
      }
    }
  };
};
