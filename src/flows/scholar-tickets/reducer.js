import {
  FETCH_SCHOLAR_TICKETS,
  FETCH_SCHOLAR_TICKETS_FAILURE,
  FETCH_SCHOLAR_TICKETS_SUCCESS,
  SET_DOC_PICTURE,
  SET_FORM_PICTURE,
  SEND_REVALIDATION_FORM,
  SEND_REVALIDATION_FORM_SUCCESS,
  SEND_REVALIDATION_FORM_FAILURE,
} from './actions/types';

// Reducer
export const initialState = {
  revalidationForm: {
    isFetching: false,
    error: '',
    success: false,
    data: null,
  },
  formPicture: null,
  documentPicture: null,
  scholarTickets: {
    isFetching: false,
    error: '',
    reachedEnd: false,
    page: 0,
    data: [],
  },
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_SCHOLAR_TICKETS:
      return {
        ...state,
        scholarTickets: {
          ...state.scholarTickets,
          isFetching: true,
          error: '',
          lastRequestUid: action.uid,
        },
      };
    case FETCH_SCHOLAR_TICKETS_SUCCESS:
      return {
        ...state,
        scholarTickets: {
          ...state.scholarTickets,
          isFetching: false,
          data: action.payload.reverse(),
        },
      };
    case FETCH_SCHOLAR_TICKETS_FAILURE:
      return {
        ...state,
        scholarTickets: {
          ...state.scholarTickets,
          isFetching: false,
          error: action.error,
        },
      };

    case SET_DOC_PICTURE:
      return {
        ...state,
        documentPicture: action.payload,
      };

    case SET_FORM_PICTURE:
      return {
        ...state,
        formPicture: action.payload,
      };

    case SEND_REVALIDATION_FORM:
      return {
        ...state,
        formPicture: null,
        documentPicture: null,
        revalidationForm: {
          ...state.revalidationForm,
          isFetching: true,
          success: false,
          error: '',
        },
      };
    case SEND_REVALIDATION_FORM_SUCCESS:
      return {
        ...state,
        revalidationForm: {
          ...state.revalidationForm,
          isFetching: false,
          success: true,
          data: action.payload,
        },
      };
    case SEND_REVALIDATION_FORM_FAILURE:
      return {
        ...state,
        revalidationForm: {
          ...state.revalidationForm,
          isFetching: false,
          success: false,
          error: action.error,
        },
      };
    default:
      return state;
  }
}
