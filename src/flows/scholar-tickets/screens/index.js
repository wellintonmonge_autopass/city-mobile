/* eslint-disable prettier/prettier */
import React, { Component } from 'react';

import { View, FlatList, Platform } from 'react-native';
import { connect } from 'react-redux';

import Header, { headerActionTypes } from '../../../components/Header';
import { NavigationActions } from 'react-navigation';
import SystemText from '../../../components/SystemText';
import VoudTouchableOpacity from '../../../components/TouchableOpacity';
import moment from 'moment';
import Icon from '../../../components/Icon';
import { fetchScholarTickets } from '../actions';
import { navigateToRoute } from '../../../redux/nav';
import { routeNames } from '../../../shared/route-names';
import Button from '../../../components/Button';
import { colors } from '../../../styles';
import LoadMask from '../../../components/LoadMask';
import { GetFontSizeRatio } from '../../../utils/font-size';

const fontSizeRatio = GetFontSizeRatio();

const styles = {
  comment: {
    marginLeft: 16,
    marginTop: 16,
  },
  container: {
    flex: 1,
    backgroundColor: colors.WHITE,
  },
  line: {
    height: 1,
    flex: 0,
    backgroundColor: colors.GRAY_LIGHT3,
    marginLeft: 16,
    marginRight: 16,
  },
  icon: {
    color: colors.BRAND_PRIMARY,
  },
  flexRow: {
    flexDirection: 'row',
  },
  containerRow: {
    paddingHorizontal: 8,
    paddingVertical: 8,
    flexDirection: 'row',
    borderColor: colors.GRAY_LIGHT,
    borderWidth: 0.4,
    borderRadius: 5,
    justifyContent: 'space-between',
  },
  date: {
    fontSize: fontSizeRatio.FONT_MIN,
    color: colors.GRAY_DARKER,
    marginTop: 9,
    fontWeight: 'bold',
  },
  description: {
    fontSize: fontSizeRatio.FONT_MIN,
    marginTop: 9,
    color: colors.BRAND_SECONDARY,
  },
  ticket: {
    marginTop: 9.75,
    fontSize: fontSizeRatio.FONT_MIN,
    color: colors.GRAY_LIGHT3,
    fontWeight: 'bold',
  },
  containerIcon: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerButton: {
    marginTop: 16,
    paddingHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  containerButtonNewCalled: {
    justifyContent: 'flex-end',
    padding: 10,
  },
  buttonClose: {
    marginLeft: 8,
  },
  containerList: {
    flex: 1,
    marginTop: 16,
  },
  buttonNewCalledStyle: {
    marginBottom: 8,
  },
  buttonStyle: {
    height: 48,
  },
  textStyle: {
    fontSize: fontSizeRatio.FONT_AVERAGE,
    fontWeight: 'bold',
  },
  messageText: {
    fontSize: fontSizeRatio.FONT_MIN,
    textAlign: 'center',
    color: colors.GRAY,
  },
};

const APPROVED = 'APPROVED';
const INSERTED = 'INSERTED';
const APPROVED_TEXT_MSG =
  'A validação será automática ao utilizar o cartão nos validadores dos ônibus.';

export const getTextSituation = item => {
  switch (item) {
    case APPROVED:
      return 'Aprovado';
    case INSERTED:
      return 'Pendente de Aprovação';

    default:
      return 'Não aprovado';
  }
};

export const getCommentSituation = (item, onlyText) => {
  const isOutOfLength =
    item.unapprovalComments && item.unapprovalComments.length >= 60;
  const comment = `${
    isOutOfLength && isOutOfLength
      ? item.unapprovalComments.substr(0, 60)
      : item.unapprovalComments
    }...`;
  let result = <SystemText style={styles.comment}>{comment}</SystemText>;

  if (onlyText) {
    return item.status === APPROVED
      ? APPROVED_TEXT_MSG
      : item.status === INSERTED
        ? null
        : item.unapprovalComments;
  }
  if (item.status === APPROVED) {
    result = (
      <SystemText style={styles.comment}>{APPROVED_TEXT_MSG}</SystemText>
    );
  } else if (item.status === INSERTED) {
    return null;
  }
  return (
    <View style={{ paddingVertical: 16, width: '80%' }}>
      <View style={{ borderTopWidth: 0.5, borderTopColor: colors.GRAY_LIGHT }}>
        {result}
      </View>
    </View>
  );
};

export const renderIcon = item => {
  let icon;
  let color;
  if (item === APPROVED) {
    icon =
      Platform.OS === 'ios'
        ? 'ios-checkmark-outline'
        : 'checkmark-circle-outline';
    color = colors.BRAND_SUCCESS;
  } else if (item === INSERTED) {
    icon = 'waiting';
    color = colors.BRAND_SECONDARY;
  } else {
    icon = 'feedback';
    color = colors.BRAND_ERROR;
  }

  return (
    <Icon
      name={icon}
      style={{ marginTop: 9, marginRight: 9 }}
      color={color}
      size={17}
    />);
};

const renderSituation = item => {
  let icon;
  let color = colors.BRAND_ERROR;
  if (item === APPROVED) {
    icon =
      Platform.OS === 'ios'
        ? 'ios-checkmark-outline'
        : 'checkmark-circle-outline';
    
    color = colors.BRAND_SUCCESS;
  } else if (item === INSERTED) {
    icon = 'waiting';
    color = colors.BRAND_SECONDARY;
  } else {
    icon = 'feedback';
    color = colors.BRAND_ERROR;
  }

  return (<View style={styles.flexRow}>
      <Icon
        name={icon}
        style={{ marginTop: 9, marginRight: 9 }}
        color={color}
        size={24}
      />
      <SystemText
        style={{
          fontSize: fontSizeRatio.FONT_MIN,
          marginTop: 9,
          color: color,
        }}>
        {getTextSituation(item)}
      </SystemText>
    </View>
  );
};

const Item = props => (
  <VoudTouchableOpacity
    style={{ paddingHorizontal: 8, paddingVertical: 8 }}
    onPress={props.onPress}>
    <View style={styles.containerRow}>
      <View>
        {renderSituation(props.data.status)}
        <SystemText style={styles.date}>
          Data de envio:{' '}
          {moment(props.data.registryDate).format('DD/MM/YYYY HH:mm')}
        </SystemText>
        <SystemText style={styles.ticket}>
          Nome do aluno: {props.data.studentName}
        </SystemText>

        <SystemText style={styles.ticket}>
          Nº do chamado: {props.data.id}
        </SystemText>
        {getCommentSituation(props.data)}
      </View>
      <View style={styles.containerIcon}>
        <Icon name="arrow-forward" style={styles.icon} size={17} />
      </View>
    </View>
  </VoudTouchableOpacity>
);

class ScholarTicketsView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const {
      navigation: { dispatch },
      idCustomer,
    } = this.props;
    dispatch(fetchScholarTickets(idCustomer));
  }

  close = () => {
    const {
      navigation: { dispatch },
    } = this.props;
    dispatch(NavigationActions.back());
  };

  handlerDetails = item => {
    const {
      navigation: { dispatch },
    } = this.props;
    dispatch(navigateToRoute(routeNames.SCHOLAR_TICKET_DETAILS, { data: item }));
  };

  renderItem = data => {
    const { item } = data;
    return <Item data={item} onPress={this.handlerDetails.bind(this, item)} />;
  };

  render() {
    const { isFetching, scholarTickets } = this.props;
    return (
      <View style={styles.container}>
        <Header
          title="Solicitações"
          left={{
            type: headerActionTypes.CLOSE,
            onPress: this.close,
          }}
        />
        {isFetching && <LoadMask message="Carregando solicitações..." />}
        <View style={styles.containerList}>
          {isFetching ? null : scholarTickets && scholarTickets.length >= 1 ? (
            <FlatList
              data={scholarTickets}
              keyExtractor={item => item.id.toString()}
              renderItem={this.renderItem}
            />
          ) : (
              <SystemText style={styles.messageText}>
                {'Nenhum dado encontrado'}
              </SystemText>
            )}
        </View>
        <View style={styles.containerButtonNewCalled}>
          <Button
            style={styles.buttonNewCalledStyle}
            buttonStyle={styles.buttonStyle}
            sm
            onPress={() => {
              const { dispatch } = this.props;
              dispatch(navigateToRoute(routeNames.SCHOLAR_TICKET_REVALIDATE));
            }}>
            REVALIDAR CARTÃO
          </Button>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    scholarTickets: state.scholarTickets.scholarTickets.data,
    isFetching: state.scholarTickets.scholarTickets.isFetching,
    idCustomer: state.profile.data.id,
  };
};

export const ScholarTickets = connect(mapStateToProps)(ScholarTicketsView);

export * from './detail';
export * from './revalidation';
