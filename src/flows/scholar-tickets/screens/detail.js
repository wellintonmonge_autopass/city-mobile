import {View, Text, ScrollView, Keyboard} from 'react-native';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import Header, {headerActionTypes} from '../../../components/Header';
import SystemText from '../../../components/SystemText';
import {colors} from '../../../styles';
import NavigationActions from 'react-navigation/src/NavigationActions';
import BrandText from '../../../components/BrandText';
import {
  getTextSituation,
  renderIcon,
  getCommentSituation,
} from '../screens/index';
import {GetFontSizeRatio} from '../../../utils/font-size';
const fontSizeRatio = GetFontSizeRatio();

const styles = {
  container: {flex: 1},
  title: {
    fontWeight: 'bold',
    alignSelf: 'center',
    fontSize: fontSizeRatio.FONT_TITLE,
    color: colors.BRAND_PRIMARY,
    paddingVertical: 32,
    paddingHorizontal: 32,
  },
  textBold: {
    fontWeight: 'bold',
    fontSize: fontSizeRatio.FONT_MIN,
  },
  text: {
    fontWeight: 'normal',
    fontSize: fontSizeRatio.FONT_MIN,
  },
  comment: {
    fontWeight: 'bold',
    alignSelf: 'flex-start',
    fontSize: fontSizeRatio.FONT_AVERAGE,
    margin: 16,
    padding: 16,
    color: colors.GRAY,
  },
  line: {
    height: 1,
    backgroundColor: colors.GRAY_LIGHT3,
    marginLeft: 16,
    marginRight: 16,
  },
};

class ScholarTicketDetailView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  close = () => {
    const {
      navigation: {dispatch},
    } = this.props;
    dispatch(NavigationActions.back());
  };

  render() {
    const {dataDetail} = this.props;
    if (!dataDetail) {
      return null;
    }
    const {
      id,
      studentName,
      cpf,
      status,
      travelPassNumber,
      travelPassExpiration,
      unapprovalComments,
      registryDate,
      evaluatedByCustomer,
    } = dataDetail;
    const commentSituation = getCommentSituation(dataDetail, true);
    return (
      <View style={styles.container}>
        <Header
          borderless
          barStyle="dark-content"
          left={{
            type: headerActionTypes.CLOSE,
            onPress: this.close,
          }}
        />
        <ScrollView style={styles.container}>
          <BrandText style={styles.title}>
            Revalidação de Cartão Escolar
          </BrandText>
          <View style={{alignContent: 'flex-start', paddingHorizontal: 32}}>
            <View style={{flexDirection: 'row', padding: 5}}>
              <SystemText style={styles.textBold}>Nome do aluno: </SystemText>
              <SystemText style={styles.text}>{studentName}</SystemText>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <SystemText style={styles.textBold}>CPF: </SystemText>
              <SystemText style={styles.text}>{cpf}</SystemText>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <SystemText style={styles.textBold}>
                Número do Cartão:{' '}
              </SystemText>
              <SystemText style={styles.text}>{travelPassNumber}</SystemText>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <SystemText style={styles.textBold}>
                Data de expiração:
              </SystemText>
              <SystemText style={styles.text}>
                {moment(travelPassExpiration).format('DD/MM/YYYY HH:mm')}
              </SystemText>
            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
              <SystemText style={styles.textBold}>Data de envio: </SystemText>
              <SystemText style={styles.text}>
                {moment(registryDate).format('DD/MM/YYYY HH:mm')}
              </SystemText>
            </View>
            <View style={{flexDirection: 'row', padding: 5, paddingBottom: 10}}>
              <SystemText style={styles.textBold}>Situação: </SystemText>
              <SystemText style={styles.text}>
                {getTextSituation(status)}
              </SystemText>
            </View>
            {commentSituation && (
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 5,
                  borderTopWidth: 0.5,
                  borderTopColor: colors.GRAY_LIGHT,
                }}>
                <View
                  style={{
                    alignSelf: 'flex-start',
                    fontSize: fontSizeRatio.FONT_MIN,
                    flexDirection: 'row',
                    margin: 5,
                    height: 100,
                    borderRightWidth: 0.5,
                    borderRightColor: colors.GRAY_LIGHT,
                  }}>
                  {renderIcon(status)}
                </View>
                <SystemText style={styles.comment}>
                  {commentSituation}
                </SystemText>
              </View>
            )}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const dataDetail =
    ownProps.navigation &&
    ownProps.navigation.state &&
    ownProps.navigation.state.params.data;
  return {
    dataDetail: dataDetail,
    scholarTickets: state.scholarTickets.data,
    isFetching: state.scholarTickets.isFetching,
  };
};

export const ScholarTicketDetail = connect(mapStateToProps)(
  ScholarTicketDetailView,
);
