import {
  View,
  ScrollView,
  Keyboard,
  PermissionsAndroid,
  Platform,
} from 'react-native';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Field, reduxForm, formValueSelector,} from 'redux-form';
import Header, {headerActionTypes} from '../../../components/Header';
import SystemText from '../../../components/SystemText';
import {colors} from '../../../styles';
import VoudTextField from '../../../components/TextField';
import {
  required,
  cpfValidator,
  scholarCardNumberValidator,
} from '../../../utils/validators';
import {
  formatCpf,
  parseCpf,
  formatBomCardNumber,
  parseBomCardNumber,
} from '../../../utils/parsers-formaters';
import CPFField from '../../../components/CPFField';
import Button from '../../../components/Button';
import IconButton from '../../../components/IconButton';
import BrandText from '../../../components/BrandText';
import {navigateToRoute} from '../../../redux/nav';
import {routeNames} from '../../../shared/route-names';
import NavigationActions from 'react-navigation/src/NavigationActions';
import {pipe} from 'ramda';
import {appendIf} from '../../../utils/fp-util';
import {setFormPicture, sendFormRevalidation, setDocPicture} from '../actions';
import {showToast, toastStyles} from '../../../redux/toast';
import LoadMask from '../../../components/LoadMask';
import {GetFontSizeRatio} from '../../../utils/font-size';
const fontSizeRatio = GetFontSizeRatio();

const PRIMARY = 'primary';
const SECONDARY = 'secondary';
const DOCUMENTO_FOTO = 'DOCUMENTO COM FOTO';
const ANEXAR_DOCUMENTO_FOTO = 'ANEXAR DOCUMENTO (RG OU CNH)';
const FORMULARIO = 'FORMULÁRIO';
const ANEXAR_FORMULARIO = 'ANEXAR FORMULÁRIO';
const TOAST_MSG = 'Todos os campos são obrigatórios.';
const reduxFormName = 'ScholarTicketsRevalidateForm';

const styles = {
  flexOne: {
    flex: 1,
  },
  flexTwo: {
    flex: 2,
  },
  padding16: {
    padding: 16,
  },
  alignContentCenter: {
    alignContent: 'center',
  },
  title: {
    fontWeight: 'bold',
    alignSelf: 'center',
    fontSize: fontSizeRatio.FONT_TITLE,
    color: colors.BRAND_PRIMARY,
    paddingVertical: 16,
    paddingHorizontal: 32,
  },
  containerButtons: {
    alignContent: 'center',
    justifyContent: 'space-evenly',
    padding: 8,
    flexDirection: 'row',
  },
  iconStyle: {
    color: colors.BRAND_SECONDARY,
    fontSize: fontSizeRatio.FONT_TITLE,
  },
  iconButton: {
    marginRight: 5,
    width: 50,
    borderWidth: 1,
    borderColor: colors.BRAND_SECONDARY,
  },
};

class ScholarTicketsRevalidateView extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps(nextProps) {
    const {
      navigation: {dispatch},
    } = this.props;
    if (
      !this.props.revalidationForm.success &&
      nextProps.revalidationForm.success
    ) {
      this.handleFeedBack(nextProps.revalidationForm.success);
    }
  }

  _checkAndroidStoragePermission = async () => {
    if (Platform.Version >= 23) {
      const grantedLocation = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      return grantedLocation === PermissionsAndroid.RESULTS.GRANTED;
    }
    return true;
  };

  _openCamera = isDocument => {
    const {
      navigation: {dispatch},
    } = this.props;
    Keyboard.dismiss();
    dispatch(navigateToRoute(routeNames.CAMERA_VIEW, {isDocument: isDocument}));
  };

  _submit = () => {
    const {
      valid,
      dispatch,
      travelPassNumber,
      cpf,
      studentName,
      documentPicture,
      formPicture,
    } = this.props;

    if (valid && documentPicture && formPicture) {
      dispatch(
        sendFormRevalidation({
          travelPassNumber,
          cpf,
          studentName,
          documentPicture,
          formPicture,
        }),
      );
    } else {
      dispatch(showToast(TOAST_MSG, toastStyles.DEFAULT));
    }
    Keyboard.dismiss();
  };

  close = () => {
    const {
      navigation: {dispatch},
    } = this.props;
    dispatch(setFormPicture(null));
    dispatch(setDocPicture(null));

    dispatch(NavigationActions.back());
  };

  handleFeedBack = success => {
    const {dispatch, idCustomer} = this.props;
    dispatch(
      navigateToRoute(routeNames.HANDLE_FEEDBACK, {
        success,
        idCustomer,
      }),
    );
  };

  render() {
    const cardNumberValidators = pipe(
      baseValidator => [baseValidator],
      appendIf(scholarCardNumberValidator, true),
    )(required);
    const {formPicture, documentPicture, isFetching} = this.props;
    const styleForm = formPicture ? styles.flexTwo : styles.flexOne;
    const styleDocs = documentPicture ? styles.flexTwo : styles.flexOne;
    return (
      <View style={styles.flexOne}>
        {isFetching && <LoadMask message="Enviando solicitação..." />}
        <Header
          borderless
          barStyle="dark-content"
          left={{
            type: headerActionTypes.CLOSE,
            onPress: this.close,
          }}
        />
        <View style={styles.alignContentCenter}>
          <BrandText style={styles.title}>
            Solicitar revalidação de Cartão Escolar
          </BrandText>
          <ScrollView>
            <View style={styles.padding16}>
              <Field
                name="studentName"
                props={{
                  label: 'Nome do aluno (*)',
                  textFieldRef: el => (this.StudentName = el),
                  returnKeyType: 'next',
                  maxLength: 100,
                  onSubmitEditing: () => {
                    this.CPFField.focus();
                  },
                }}
                component={VoudTextField}
                validate={required}
              />
              <Field
                name="cpf"
                props={{
                  label: 'CPF (*)',
                  textFieldRef: el => (this.CPFField = el),
                  keyboardType: 'numeric',
                  onSubmitEditing: () => {
                    this.TravelPassNumber.focus();
                  },
                }}
                format={formatCpf}
                parse={parseCpf}
                component={VoudTextField}
                validate={[required, cpfValidator]}
              />
              <Field
                name="travelPassNumber"
                props={{
                  label: 'Número do Cartão (*)',
                  textFieldRef: el => (this.TravelPassNumber = el),
                  keyboardType: 'numeric',
                  returnKeyType: 'done',
                  maxLength: 16,
                  onSubmitEditing: () => {
                    Keyboard.dismiss();
                  },
                }}
                format={formatBomCardNumber}
                parse={parseBomCardNumber}
                component={VoudTextField}
                validate={cardNumberValidators}
              />
            </View>
          </ScrollView>
          <View style={styles.containerButtons}>
            <Button
              style={styleForm}
              outline={formPicture ? PRIMARY : SECONDARY}
              outlineText={documentPicture ? PRIMARY : SECONDARY}
              align={formPicture ? 'left' : 'center'}
              icon={formPicture ? 'done' : null}
              onPress={() => {
                this._openCamera(false);
              }}>
              {formPicture ? FORMULARIO : ANEXAR_FORMULARIO}
            </Button>
            {formPicture && (
              <IconButton
                iconName={'delete'}
                style={styles.iconButton}
                iconStyle={styles.iconStyle}
                onPress={() => {
                  const {
                    navigation: {dispatch},
                  } = this.props;
                  dispatch(setFormPicture(null));
                }}
              />
            )}
          </View>
          <View style={styles.containerButtons}>
            <Button
              style={styleDocs}
              outline={documentPicture ? PRIMARY : SECONDARY}
              outlineText={documentPicture ? PRIMARY : SECONDARY}
              align={documentPicture ? 'left' : 'center'}
              icon={documentPicture ? 'done' : ''}
              onPress={() => {
                this._openCamera(true);
              }}>
              {documentPicture ? DOCUMENTO_FOTO : ANEXAR_DOCUMENTO_FOTO}
            </Button>
            {documentPicture && (
              <IconButton
                iconName={'delete'}
                style={styles.iconButton}
                iconStyle={styles.iconStyle}
                onPress={() => {
                  const {
                    navigation: {dispatch},
                  } = this.props;
                  dispatch(setDocPicture(null));
                }}
              />
            )}
          </View>
          <View style={styles.containerButtons}>
            <Button style={styles.flexOne} onPress={this._submit}>
              ENVIAR
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    studentName: formValueSelector(reduxFormName)(state, 'studentName'),
    cpf: formValueSelector(reduxFormName)(state, 'cpf'),
    travelPassNumber: formValueSelector(reduxFormName)(
      state,
      'travelPassNumber',
    ),
    formPicture: state.scholarTickets.formPicture,
    documentPicture: state.scholarTickets.documentPicture,
    isFetching: state.scholarTickets.revalidationForm.isFetching,
    revalidationForm: state.scholarTickets.revalidationForm,
    idCustomer: state.profile.data.id,
  };
};

export const ScholarTicketsRevalidate = connect(mapStateToProps)(
  reduxForm({form: reduxFormName, destroyOnUnmount: true})(
    ScholarTicketsRevalidateView,
  ),
);
