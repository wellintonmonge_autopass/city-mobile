import React from 'react';
import {View, Easing} from 'react-native';
import {connect} from 'react-redux';

import {colors} from '../../../styles';
import BrandText from '../../../components/BrandText';
import CircleTransition from '../../../components/CircleTransition';
import {routeNames} from '../../../shared/route-names';
import {backToRoute} from '../../../redux/nav';
import Icon from '../../../components/Icon';

import {GetFontSizeRatio} from '../../../utils/font-size';
import {fetchScholarTickets} from '../actions';
const fontSizeRatio = GetFontSizeRatio();

const styles = {
  container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
  text: {
    color: colors.WHITE,
    fontSize: fontSizeRatio.FONT_MAX,
    fontWight: 'bold',
  },
};

const SUCCESS_MSG = 'Solicitação enviado com successo!';
const FAILURE_MSG = 'Ocorreu um erro no envio da solicitação!';

const TRANSITION_BUFFER = 3000;
const ANIMATION_DURATION = 2000;

const setTimeoutTransition = (dispatch, callback, nameRoute, idCustomer) => {
  dispatch(fetchScholarTickets(idCustomer));
  setTimeout(() => {
    dispatch(callback(nameRoute));
  }, TRANSITION_BUFFER);
};

const HandleFeedBackView = ({navigation, dispatch}) => {
  const success = navigation.getParam('success');
  const idCustomer = navigation.getParam('idCustomer');
  setTimeoutTransition(
    dispatch,
    backToRoute,
    routeNames.SCHOLAR_TICKETS,
    idCustomer,
  );

  const backgroundColor = success ? colors.BRAND_SUCCESS : colors.BRAND_ERROR;
  const iconResult = success ? (
    <Icon name="checkmark-circle-outline" size={40} color={colors.WHITE} />
  ) : (
    <Icon name="error-outline" size={40} color={colors.WHITE} />
  );
  const message = success ? SUCCESS_MSG : FAILURE_MSG;

  return (
    <CircleTransition
      color={backgroundColor}
      expand
      transitionBuffer={TRANSITION_BUFFER}
      duration={ANIMATION_DURATION}
      easing={Easing.linear}
      position={'center'}>
      <View style={styles.container}>
        {iconResult}
        <BrandText style={styles.text}>{message}</BrandText>
      </View>
    </CircleTransition>
  );
};

export default connect()(HandleFeedBackView);
