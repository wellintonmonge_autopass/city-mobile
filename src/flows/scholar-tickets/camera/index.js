import React, {Component} from 'react';
import {View, ImageBackground, Alert, Platform} from 'react-native';
import {connect} from 'react-redux';
import {RNCamera} from 'react-native-camera';
import {request, check, PERMISSIONS, RESULTS} from 'react-native-permissions';

import {colors} from '../../../styles';
import Header, {headerActionTypes} from '../../../components/Header';
import NavigationActions from 'react-navigation/src/NavigationActions';
import {setDocPicture, setFormPicture} from '../actions';
import IconButton from '../../../components/IconButton';

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  iconButton: {
    flex: 0,
    borderRadius: 100,
    borderWidth: 4,
    borderColor: colors.WHITE,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  iconStyle: {
    color: colors.WHITE,
    fontSize: 24,
  },
  centerButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
};

class CameraView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imagePreview: null,
    };
    this.camera = null;
  }

  componentDidMount = () => {
    if (Platform.OS === 'ios') {
      check(PERMISSIONS.IOS.CAMERA).then(result => {
        if (result !== RESULTS.GRANTED) {
          request(PERMISSIONS.IOS.CAMERA).then(resultCamera => {
            this.statusMessagePermissions(resultCamera, 'Camera');
          });
        }
      });
    }
  };

  alertStatus = (title, msg) => {
    Alert.alert(
      title,
      msg,
      [
        {
          text: 'OK',
          onPress: () => {
            const {dispatch} = this.props;
            dispatch(NavigationActions.back());
          },
        },
      ],
      {
        onDismiss: () => {},
      },
    );
  };

  statusMessagePermissions = (status, PERMISSION) => {
    switch (status) {
      case RESULTS.UNAVAILABLE:
        this.alertStatus(
          PERMISSION,
          'Esta funcionalidade não é suportada por este aparelho',
        );
        break;
      case RESULTS.DENIED:
        this.alertStatus(
          PERMISSION,
          'Esta funcionalidade não foi requisitada ao usuário',
        );
        break;
      case RESULTS.GRANTED:
        console.tron.log('PASSOU AQUI');

        break;
      case RESULTS.BLOCKED:
        this.alertStatus(
          PERMISSION,
          `Para usar a ${PERMISSION} verifique se o City + tem permissão para acessá-lo`,
        );
        break;
    }
  };

  close = () => {
    const {dispatch} = this.props;
    dispatch(NavigationActions.back());
  };

  takePicture = async () => {
    const {dispatch, isDocument} = this.props;
    if (this.camera) {
      const options = {quality: 0.1, base64: true, pauseAfterCapture: true};
      const data = await this.camera.takePictureAsync(options);

      isDocument
        ? dispatch(setDocPicture(data))
        : dispatch(setFormPicture(data));

      this.camera.resumePreview();
    }
  };

  render() {
    const {documentPicture, formPicture, isDocument} = this.props;
    const imagePreview = isDocument ? documentPicture : formPicture;
    if (imagePreview) {
      return (
        <ImageBackground
          style={styles.container}
          source={{uri: imagePreview.uri}}>
          <Header
            borderless
            backTextWhite
            barStyle="light-content"
            left={{
              type: headerActionTypes.CLOSE,
              onPress: this.close,
              iconSize: 36,
              iconColor: colors.WHITE,
            }}
          />
          <View style={styles.centerButton}>
            <IconButton
              iconName={'done'}
              size={24}
              style={styles.iconButton}
              iconStyle={styles.iconStyle}
              onPress={this.close}
            />
          </View>
        </ImageBackground>
      );
    } else {
      return (
        <View style={styles.container}>
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style={styles.preview}
            captureAudio={false}
            type={RNCamera.Constants.Type.back}
            flashMode={RNCamera.Constants.FlashMode.off}
          />
          <View
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              bottom: 0,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <IconButton
              iconName={'camera'}
              style={styles.iconButton}
              iconStyle={styles.iconStyle}
              onPress={this.takePicture.bind(this)}
            />
          </View>
        </View>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    isDocument: ownProps.navigation.state.params.isDocument,
    formPicture: state.scholarTickets.formPicture,
    documentPicture: state.scholarTickets.documentPicture,
    isFetching: state.scholarTickets.isFetching,
  };
};

export const Camera = connect(mapStateToProps)(CameraView);
