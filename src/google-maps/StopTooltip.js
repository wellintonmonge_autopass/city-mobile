import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import Moment from 'moment';

import BrandText from '../components/BrandText';
import Icon from '../components/Icon';
import {colors} from '../styles';
import {pointsOfInterestTypes} from '../flows/bus-lines/screens/details';
import {calculateDistance} from '../utils/haversine';
import {voudCapitalizeLetters} from '../utils/string-util';

class StopTooltip extends Component {
  _getStopIconName = stopType => {
    if (stopType === pointsOfInterestTypes.STOP_BUS) return 'bus';
    if (stopType === pointsOfInterestTypes.STOP_SUBWAY) return 'subway';
    if (stopType === pointsOfInterestTypes.STOP_TRAIN) return 'train';
    if (stopType === pointsOfInterestTypes.RECHARGE) return 'validator';

    return 'bus';
  };

  _calculateDistance = () => {
    const {stopPosition, userPosition} = this.props;

    const distance = calculateDistance(
      stopPosition.latitude,
      stopPosition.longitude,
      userPosition.latitude,
      userPosition.longitude,
    );

    if (distance > 1) {
      return `${distance}km`;
    }

    // if under 1km, convert to meters
    return `${distance * 1000}m`;
  };

  _formatRemainingTime = timeToArrival => {
    const remainingTime = Moment(timeToArrival, 'HH:mm').diff(
      Moment(),
      'minute',
    );
    return `${remainingTime} min`;
  };

  _renderForecastToolTip = () => (
    <View style={styles.forecastTooltipBody}>
      <Icon name="bus" style={styles.forecastIcon} />
      <BrandText style={styles.forecastText}>
        <BrandText>{'Chegando em '}</BrandText>
        <BrandText style={styles.timeToArrivalText}>
          {this._formatRemainingTime(this.props.timeToArrival)}
        </BrandText>
      </BrandText>
    </View>
  );

  _renderDefaultTooltip = () => {
    const {type, stopName, lines, pointAddress, pointName} = this.props;

    return (
      <View style={styles.tooltipBody}>
        <View style={styles.busLineInfo}>
          <Icon
            size={16}
            name={this._getStopIconName(type)}
            style={styles.icon}
          />
          <BrandText style={styles.busLineName}>
            {voudCapitalizeLetters(stopName || pointAddress)}
            <BrandText style={{fontWeight: 'normal'}}>
              {' '}
              ({this._calculateDistance()})
            </BrandText>
          </BrandText>
        </View>

        <View style={styles.busLinesList}>
          {lines &&
            lines.map(line => (
              <View
                key={line.id}
                style={StyleSheet.flatten([
                  styles.busLineLabel,
                  {backgroundColor: `#${line.lineColor || '000000'}`},
                ])}>
                <BrandText
                  style={StyleSheet.flatten([
                    styles.busLineLabelText,
                    {color: `#${line.textColor || '000000'}`},
                  ])}>
                  {line.lineNumber}
                </BrandText>
              </View>
            ))}
          {type === pointsOfInterestTypes.RECHARGE && (
            <View
              key={pointName}
              style={StyleSheet.flatten([
                styles.busLineLabel,
                {backgroundColor: colors.BRAND_PRIMARY_DARKER},
              ])}>
              <BrandText
                style={StyleSheet.flatten([
                  styles.busLineLabelText,
                  {color: 'white'},
                ])}>
                {voudCapitalizeLetters(pointName)}
              </BrandText>
            </View>
          )}
        </View>
      </View>
    );
  };

  render() {
    const {timeToArrival} = this.props;
    return (
      <LinearGradient
        start={{x: 0.5, y: 0}}
        end={{x: 0.5, y: 1}}
        locations={[0, 0.92, 1]}
        colors={['transparent', 'rgba(0,0,0,0.15)', 'transparent']}
        style={styles.tooltip}>
        {timeToArrival
          ? this._renderForecastToolTip()
          : this._renderDefaultTooltip()}
        <View style={styles.tooltipArrow} />
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  tooltip: {
    paddingBottom: 9,
    paddingHorizontal: 1,
  },
  tooltipBody: {
    backgroundColor: 'white',
    borderRadius: 4,
    padding: 8,
    width: 190,
  },

  tooltipArrow: {
    backgroundColor: 'white',
    width: 9,
    height: 9,
    marginLeft: -4,
    position: 'absolute',
    left: '50%',
    bottom: 4,
    transform: [{rotate: '45deg'}],
  },

  busLineInfo: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  busLineName: {
    flex: 1,
    flexDirection: 'row',
    fontSize: 10,
    fontWeight: 'bold',
    color: '#4b4b4b',
    marginLeft: 4,
  },

  icon: {
    fontSize: 16,
    color: colors.BRAND_PRIMARY,
  },

  busLinesList: {
    flexDirection: 'row',
    marginTop: 8,
    flexWrap: 'wrap',
  },

  busLineLabel: {
    borderRadius: 2,
    padding: 4,
    backgroundColor: '#234A76',
    marginLeft: 4,
    marginBottom: 4,
  },
  busLineLabelText: {
    lineHeight: 10,
    fontSize: 8,
    color: 'white',
    fontWeight: 'bold',
  },

  forecastTooltipBody: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 4,
    padding: 8,
  },
  forecastIcon: {
    fontSize: 24,
    color: colors.BRAND_PRIMARY,
  },
  forecastText: {
    marginLeft: 4,
    fontSize: 12,
    color: colors.GRAY,
  },
  timeToArrivalText: {
    fontWeight: 'bold',
    color: colors.BRAND_SUCCESS,
  },
});

StopTooltip.propTypes = {
  stopName: PropTypes.string,
  lines: PropTypes.array,
  pointAddress: PropTypes.string,
  pointName: PropTypes.string,
  type: PropTypes.string,
};
StopTooltip.defaultProps = {
  stopName: '',
  lines: [],
  pointAddress: '',
  pointName: '',
  type: 'BUS_STOP',
};

export default StopTooltip;
