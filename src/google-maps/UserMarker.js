import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {Marker} from 'react-native-maps';
import PropTypes from 'prop-types';

import Icon from '../components/Icon';
import LinearGradient from 'react-native-linear-gradient';

class UserMarker extends Component {
  constructor(props) {
    super(props);
    this._tracksViewChanges = true;
  }

  componentDidUpdate() {
    this._tracksViewChanges = false;
  }

  render() {
    const {coordinate} = this.props;
    return (
      <Marker
        coordinate={coordinate}
        tracksViewChanges={this._tracksViewChanges}>
        <View style={styles.userMarkerContainer}>
          <View style={styles.userMarkerGradientContainer}>
            <LinearGradient
              colors={['#F39200', '#FDC300']}
              style={styles.userMarker}
              start={{x: 0, y: 1}}
              end={{x: 1.2, y: 0}}>
              <Icon style={styles.userMarkerIcon} name="user" size={16} />
            </LinearGradient>
          </View>
        </View>
      </Marker>
    );
  }
}

const styles = StyleSheet.create({
  userMarkerContainer: {
    borderWidth: 5,
    borderColor: '#F8E71C',
    backgroundColor: '#F8E71C',
    borderRadius: 50,
  },
  userMarkerGradientContainer: {
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 15,
    overflow: 'hidden',
  },
  userMarker: {
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    // padding: 2,
  },
  userMarkerIcon: {
    color: 'white',
  },
});

UserMarker.propTypes = {
  coordinate: PropTypes.shape({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    latitudeDelta: PropTypes.number,
    longitudeDelta: PropTypes.number,
  }).isRequired,
};

UserMarker.defaultProps = {};

export default UserMarker;
