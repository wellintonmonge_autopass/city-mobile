// Reactotron config
import './ReactotronConfig';

// NPM imports
import React, {Component} from 'react';
import {createStore, applyMiddleware, compose} from 'redux';

import {Provider} from 'react-redux';
import firebase from 'react-native-firebase';
import thunkMiddleware from 'redux-thunk';
import Reactotron from 'reactotron-react-native';
import Moment from 'moment';
import SplashScreen from 'react-native-splash-screen';
import {StatusBar} from 'react-native';
import {useScreens} from 'react-native-screens';

import 'moment/locale/pt-br';
import 'intl';
import 'intl/locale-data/jsonp/pt-BR';

// VouD imports
import AppReducer from './redux';
import {hydrateFromAsyncStorage} from './redux/init';
import AppWithNavigationState, {
  reactNavigationMiddleware,
} from './navigators/AppNavigator';
import KeyboardAvoidingView from './components/KeyboardAvoidingView';
import Toast from './components/Toast';
import ConfigError from './components/ConfigError';
import {makePushController} from './utils/push-controller';
import {googleAnalyticsScreenTracker} from './redux/middleware';
import {fetchContent, handleDiscountCode, setDeviceId} from './redux/config';
import {emitInvalidDiscountToast} from './utils/member-get-member';
import {navigateToRoute} from './redux/nav';
import {routeNames} from './shared/route-names';
import {getDeviceId} from './utils/device-info';

useScreens();

const middleware = applyMiddleware(
  reactNavigationMiddleware,
  googleAnalyticsScreenTracker,
  thunkMiddleware,
);
const store = createStore(
  AppReducer,
  compose(
    middleware,
    Reactotron.createEnhancer(),
  ),
);

export default class CityMaisApp extends Component {
  // Check if opened deep link has a valid discountCode and
  // check if user is already logged in. If both are true
  // show a toast stating that the discount code is valid only for
  // new users only.
  _handleFirebaseDeepLink = async openedLink => {
    store
      .dispatch(handleDiscountCode(openedLink))
      .then(({userIsLoggedIn, discountCode, onboardingViewed}) => {
        if (userIsLoggedIn && discountCode) {
          emitInvalidDiscountToast(store.dispatch);
        }
        if (!userIsLoggedIn && discountCode && onboardingViewed) {
          store.dispatch(navigateToRoute(routeNames.AUTH));
        }
      });
  };

  componentDidMount() {
    Moment.locale('pt-br');

    getDeviceId().then(deviceId => {
      store.dispatch(setDeviceId(deviceId));
    });

    store.dispatch(hydrateFromAsyncStorage()).then(() => {
      this._handleFirebaseDeepLink();

      // Subscribe to URL open events even if the app is already running
      firebase.links().onLink(openedLink => {
        this._handleFirebaseDeepLink(openedLink);
      });
    });

    SplashScreen.hide();

    this.pushController = makePushController(store.dispatch);
    this.pushController.init();

    store.dispatch(fetchContent());
  }

  componentWillUnmount() {
    this.pushController.removeListeners();
  }

  render() {
    return (
      <Provider store={store}>
        <KeyboardAvoidingView>
          <StatusBar
            barStyle="light-content"
            translucent
            backgroundColor="transparent"
          />
          <AppWithNavigationState />
          <Toast />
          <ConfigError />
        </KeyboardAvoidingView>
      </Provider>
    );
  }
}
