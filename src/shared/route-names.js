// VouD imports
import mobilityServiceRouteNames from '../flows/mobility-services/route-names';

export const routeNames = {
  // Home
  ONBOARDING: 'Onboarding',
  HOME: 'Home',
  HOME_NOT_LOGGED_IN: 'HomeNotLoggedIn',
  SERVICE_POINTS: 'ServicePoints',
  HELP: 'Help',
  HELP_DETAILS: 'HelpDetails',
  // Navigator
  MODAL_NAVIGATOR: 'ModalNavigator',
  MAIN_NAVIGATOR: 'MainNavigator',
  MENU_NAVIGATOR: 'MenuNavigator',
  // Auth
  AUTH: 'Auth',
  PRE_AUTH: 'PreAuth',
  LOGIN: 'Login',
  REGISTER: 'Register',
  REGISTER_FACEBOOK: 'RegisterFacebook',
  CONFIRM_MOBILE: 'ConfirmMobile',
  REGISTER_EDIT_MOBILE: 'RegisterEditMobile',
  CONFIRM_EMAIL: 'ConfirmEmail',
  REGISTER_EDIT_EMAIL: 'RegisterEditEmail',
  REGISTER_SUCCESS: 'RegisterSuccess',
  RECOVER_PASSWORD: 'RecoverPassword',
  CHANGE_PASSWORD: 'ChangePassword',
  // Card
  CARD_DETAILS: 'CardDetails',
  ADD_CARD: 'AddCard',
  ADD_CARD_HELPER_DIALOG: 'AddCardHelperDialog',
  EDIT_CARD: 'EditCard',
  NEXT_RECHARGES: 'NextRecharges',
  SCHOOL_CARD_DETAILS: 'SchoolCardDetails',
  SERVICE_POINTS_DETAILS: 'ServicePointDetails',
  // Profile
  MY_PROFILE: 'MyProfile',
  EDIT_PERSONAL_DATA: 'EditPersonalData',
  EDIT_PASSWORD: 'EditPassword',
  EDIT_EMAIL: 'EditEmail',
  EDIT_EMAIL_CHECKED: 'EditEmailChecked',
  EDIT_EMAIL_CONFIRMATION: 'EditEmailConfirmation',
  EDIT_MOBILE: 'EditMobile',
  EDIT_MOBILE_CHECKED: 'EditMobileChecked',
  EDIT_MOBILE_CONFIRMATION: 'EditMobileConfirmation',
  EDIT_ADDRESS: 'EditAddress',
  SELECT_STATE_DIALOG: 'SelectStateDialog',
  // Purchase
  BUY_CREDIT: 'BuyCredit',
  SELECT_PAYMENT_METHOD: 'SelectPaymentMethod',
  PURCHASE_HISTORY: 'PurchaseHistory',
  PURCHASE_HISTORY_DETAIL: 'PurchaseHistoryDetail',
  PAYMENT_SUCCESSFUL: 'PaymentSuccessful',
  PAYMENT_ERROR: 'PaymentError',
  PAYMENT_SMART_PURCHASE_DIALOG: 'PaymentSmartPurchaseDialog',
  DEBIT_CARD_REDIRECT_ALERT: 'DebitCardRedirectAlert',
  DEBIT_CARD_SUPPORTED_BANKS_HELPER_DIALOG:
    'DebitCardSupportedBanksHelperDialog',
  PURCHASE_CONFIRMATION_DIALOG: 'PurchaseConfirmationDialog',
  // Payment Methods
  PAYMENT_METHODS: 'PaymentMethods',
  ADD_PAYMENT_METHOD: 'AddPaymentMethod',
  // Report
  REPORTS: 'Reports',
  REPORT_IS_HAPPENING_QUESTION: 'ReportIsHappeningQuestion',
  FINISH_REPORT_REQUEST: 'FinishReportRequest',
  ADD_REPORT: 'AddReport',
  SELECT_REPORT_TYPE_DIALOG: 'SelectReportTypeDialog',
  SELECT_REPORT_MODAL_TYPE_DIALOG: 'SelectReportModalTypeDialog',
  SELECT_REPORT_DETAILS_DIALOG: 'SelectReportDetailsDialog',
  SEND_REPORT_SUCCESSFUL: 'SendReportSuccessful',
  // Smart Purchase
  SMART_PURCHASE: 'SmartPurchase',
  EDIT_SMART_PURCHASE: 'EditSmartPurchase',
  // Facebook
  FB_PRE_AUTH: 'FacebookPreAuth',
  FB_REGISTER: 'FacebookRegister',
  FB_CONNECT: 'FacebookConnect',
  // Browser (WebView)
  BROWSER: 'Browser',
  // UnsupportedVersionDialog
  UNSUPPORTED_VERSION_DIALOG: 'UnsupportedVersionDialog',
  // UnsupportedDeviceDialog
  UNSUPPORTED_DEVICE_DIALOG: 'UnsupportedDeviceDialog',
  // Service Unavailable
  SERVICE_UNAVAILABLE: 'ServiceUnavailable',
  // Notification Center
  NOTIFICATION_CENTER: 'NotificationCenter',
  NOTIFICATION_DETAIL: 'NotificationDetail',
  // Cellphone Recharge
  PHONE_RECHARGE: 'PhoneRecharge',
  CONFIRM_PHONE_DIALOG: 'ConfirmPhoneDialog',
  PHONE_CARRIERS_LIST_DIALOG: 'PhoneCarriersDialog',
  PHONE_RECHARGE_SUCCESSFUL: 'PhoneRechargeSuccessful',
  // GPS
  GPS_LOW_ACCURACY_DIALOG: 'GPSLowAccuracyDialog',
  // Usage Terms
  ACCEPT_USAGE_TERMS: 'AcceptUsageTerms',
  // Bus lines
  BUS_LINES: 'BusLines',
  BUS_LINE_DETAILS: 'BusLineDetails',
  // Mobility Services
  ...mobilityServiceRouteNames,
  // Search Points of Interest
  SEARCH_POINTS_OF_INTEREST: 'SearchPointsOfInterest',
  // Search Routes
  SEARCH_ROUTES: 'SearchRoutes',
  // Trace Routes
  TRACE_ROUTES: 'TraceRoutes',
  // Transport Search Result
  TRANSPORT_SEARCH_RESULT: 'TransportSearchResult',
  //SCHOLAR_TICKETS
  SCHOLAR_TICKETS: 'ScholarTicketRevalidation',
  SCHOLAR_TICKET_DETAILS: 'ScholarTicketDetailRevalidation',
  SCHOLAR_TICKET_REVALIDATE: 'ScholarTicketRevalidate',
  CAMERA_VIEW: 'CAMERA_VIEW',
  HANDLE_FEEDBACK: 'HANDLE_FEEDBACK',
};
