// NPM imports
import axios from 'axios';
import NetInfo from '@react-native-community/netinfo';

import {
  SessionExpiredError,
  VoudError,
  UnsupportedVersionError,
  ServiceUnavailableError,
  FetchError,
} from './custom-errors';
import { saveItem, getItem, removeItem } from '../utils/async-storage';
import { getBuildNumber } from '../utils/device-info';
import CONFIG from '../../config/config.js';

// Private variables

// Base URLs
const VOUD_BASE_URL = CONFIG.api[CONFIG.environment].baseUrl;

const VOUD_USER_TOKEN_KEY = 'x-voud-user-token';
const SESSION_TOKEN_ASYNC_KEY = 'sessionToken';

export const voudErrorCodes = {
  VT_CARD_DOES_NOT_BELONG_TO_USER: '412007',
  EXCEEDED_DAILY_PURCHASE_LIMIT: '999002',
  ACQUIRER_INTEGRATION_ERROR: '999901',
};

export const httpStatusCodes = {
  SESSION_EXPIRED: 403,
  UNSUPPORTED_VERSION: 409,
  SERVICE_UNAVAILABLE: 503,
};

// Session Token
let sessionToken;

export const clearSessionToken = () => {
  sessionToken = undefined;
  removeItem(SESSION_TOKEN_ASYNC_KEY);
};

// App Build Number
let buildNumber = getBuildNumber();

/**
 * fetch wrapper, including headers configurations
 * @param {string} endpoint - service endopoint
 * @param {string} method - request method ('GET', 'POST', 'PUT', 'DELETE')
 * @param {Object} requestBody
 */
export const voudRequest = async (
  endpoint,
  method,
  requestBody,
  isAuth = false,
  params,
) => {
  if (isAuth && !sessionToken) {
    sessionToken = await getItem(SESSION_TOKEN_ASYNC_KEY);
  }

  const netState = await NetInfo.fetch();

  if (netState.isInternetReachable === false) {
    throw new VoudError('Verifique sua conexão com a internet.', 12, {});
  }

  const config = {
    method,
    url: endpoint,
    baseURL: VOUD_BASE_URL,
    params: params ? params : {},
    headers: {
      ...(isAuth ? { [VOUD_USER_TOKEN_KEY]: sessionToken } : {}),
      'x-voud-build-number': buildNumber,
      'Content-Type': 'application/json',
      'x-voud-channel': CONFIG.api[CONFIG.environment].channel,
      'x-voud-api-version': 2,
    },
  };

  if (requestBody) config.data = requestBody;

  let response;
  try {
    response = await axios(config);
  } catch (error) {
    response = error.response;
    const responseJson = response.data;
    if (response.status !== 200) {
      const errorMessage = responseJson.returnMessage || response.status;

      if (response.status === httpStatusCodes.SESSION_EXPIRED) {
        throw new SessionExpiredError(errorMessage, response.status);
      }

      if (response.status === httpStatusCodes.UNSUPPORTED_VERSION) {
        throw new UnsupportedVersionError(errorMessage, response.status);
      }

      if (response.status === httpStatusCodes.SERVICE_UNAVAILABLE) {
        const messageTitle = responseJson.title ? responseJson.title : '';
        const messageBody = responseJson.message ? responseJson.message : '';
        throw new ServiceUnavailableError(
          messageTitle,
          messageBody,
          response.status,
        );
      }

      const error = new VoudError(errorMessage, responseJson.returnCode, {
        ...responseJson,
      });
      error.response = response;
      throw error;
    }
  }

  const responseJson = response.data;

  const voudSessionToken = response.headers[VOUD_USER_TOKEN_KEY];
  if (voudSessionToken) {
    sessionToken = voudSessionToken;
    saveItem(SESSION_TOKEN_ASYNC_KEY, voudSessionToken);
  }

  if (responseJson.returnCode && responseJson.returnCode !== '0') {
    throw new VoudError(responseJson.returnMessage, responseJson.returnCode, {
      ...responseJson,
    });
  }

  return responseJson;
};
