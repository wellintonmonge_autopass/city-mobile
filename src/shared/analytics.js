// NPM imports
import firebase from "react-native-firebase";

// export tracker
const voudTracker = firebase.analytics();

// dry run config
// GoogleAnalyticsSettings.setDryRun(false);

// Set user id
export const GASetUser = id => {
  voudTracker.setUserId(id);
};

// Track screen

export const GATrackScreen = nextScreen => {
  voudTracker.setCurrentScreen(nextScreen);
};

// Track event
export const GATrackEvent = (category = '', action = '', label = '', value = null) => {
  const params = {};
  if (label) params.label = label;
  if (value) params.value = value;

  voudTracker.logEvent(category, {
    action,
    ...params
  });
};

// Track purchase event
export const GATrackPurchaseEvent = (product = {}, transaction = {}, eventCategory = '', eventAction = '') => {
  voudTracker.logEvent(GAEventParams.labels.PURCHASE, {
    product,
    transaction,
    category: eventCategory,
    action: eventAction,
  });
};

// GA event params
export const GAEventParams = {
  categories: {
    BUTTON: 'button',
    FORM: 'form',
    ERROR: 'appError',
  },
  actions: {
    CLICK: 'click',
    SUBMIT: 'submit',
  },
  labels: {
    LOGIN: 'Cadastrar',
    REPORT: 'Denunciar',
    SUBMIT_PRE_AUTH: 'CPF',
    SUBMIT_REGISTER: 'Cadastro',
    SUBMIT_LOGIN: 'Login',
    FORGET_PASSWORD: 'EsqueciSenha',
    SUBMIT_RECOVER_PW: 'Redefinir',
    SUBMIT_MOBILE_VALIDATION_CODE: 'CodValidacao',
    REGISTER_EDIT_MOBILE: 'CadastroEditarCelular',
    REGISTER_EDIT_EMAIL: 'CadastroEditarEmail',
    FINISH_REGISTER: 'Validacao',
    ADD_CARD: 'AddCard',
    MENU_TRANSPORT_CARDS: 'CartoesMenu',
    MENU_TRANSPORT_CARD_DETAILS: 'DetalheCartaoMenu',
    MENU_ADD_CARD: 'AddCardMenu',
    MENU_SMART_PURCHASE: 'CompraProgramadaMenu',
    MENU_PURCHASE_HISTORY: 'HistoricoMenu',
    MENU_SERVICE_POINTS: 'PontosRecargaMenu',
    MENU_MOBILITY_SERVICES: 'TaxiMobilidadeMenu',
    MENU_REPORT: 'DenunciaMenu',
    MENU_HELP: 'AjudaMenu',
    MENU_MY_PROFILE: 'PerfilMenu',
    MENU_PAYMENT_METHODS: 'FormasPagamentoMenu',
    MENU_LOGOUT: 'SairMenu',
    MENU_HOME: 'InicioMenu',
    MENU_LOGIN: 'CadastrarMenu',
    MENU_PHONE_RECHARGE: 'RecargaCelularMenu',
    MENU_SUPPORT: 'Suporte',
    MENU_TRANSPORT_CARDS_COVERAGE: 'CoberturaCartoesMenu',
    TRANSPORT_CARD_DETAILS: 'Cartao',
    BUY: 'ComprarCreditos',
    SUBMIT_DEBIT_CARD_PAYMENT: 'EnvioCompraDebito',
    SUBMIT_DEBIT_CARD_PAYMENT_ERROR: 'ErroEnvioCompraDebito',
    SUBMIT_CREDIT_CARD_PAYMENT: 'EnvioCompraCredito',
    SUBMIT_CREDIT_CARD_PAYMENT_ERROR: 'ErroEnvioCompraCredito',
    SERVICE_POINT: 'Ponto', // + service point name
    SERVICE_POINT_ROUTE: 'Rota', // + service point name
    // Reports
    REPORT_START: 'Denuncia',
    CHANGE_REPORT_CATEGORY: 'AlterouDenuncia', // + report category
    REPORT_LOCATION_TYPE: 'LocalAssedio', // + report location type
    SUBMIT_REPORT: 'DenunciaFeita',
    SUBMIT_REPORT_FINISH: 'DenunciaFinalizada',
    CALL_EMERGENCY: 'ChamadaEmergencia',
    REPORT_IS_HAPPENING: 'DenunciaOcorrendoAgora',
    // Payment Method
    SUBMIT_SAVE_PAYMENT_METHOD: 'SaveCard',
    // MGM
    SHARE_MGM_DEEP_LINK: 'CompartilhouDeepLinkMGM',
    // VAH
    VAH_OPEN_PLAYER_DEEP_LINK: 'AbriuPlayerDeepLinkVAH',
    // Phone Recharge
    SELECT_PHONE_CARRIER: 'SelecionouOperadoraCelular',
    // Purchase
    PURCHASE: 'Purchase',
    //Bus Lines
    BUS_LINES: 'BusLines',
    //SEARCH_POINTS_OF_INTEREST
    MENU_SEARCH_POINTS_OF_INTEREST: 'Rotas',
    //SCHOLAR_TICKETS
    SCHOLAR_TICKETS: 'Revalidação de bilhete escolar',
    SCHOLAR_TICKETS_REVALIDATE: 'Revalidação de bilhete escolar Envio',
  },
};
