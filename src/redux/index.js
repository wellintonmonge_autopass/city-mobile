// NPM imports
import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';

// import reducers
import auth from './auth';
import config from './config';
import financial from './financial';
import help from './help';
import init from './init';
import login from './login';
import menu from './menu';
import nav from './nav';
import onboarding from './onboarding';
import paymentMethod from './payment-method';
import profile from './profile';
import profileEdit from './profile-edit';
import register from './register';
import report from './report';
import servicePoint from './service-point';
import smartPurchase from './smart-purchase';
import toast from './toast';
import transportCard from './transport-card';
import facebook from './facebook';
import apiStatus from './api-status';
import notifications from './notifications';
import phoneRecharge from './phone-recharge';
import usageTerms from '../flows/usage-terms/reducer';
import busLines from '../flows/bus-lines/reducers';
import searchPointsOfInterest from '../flows/search-points-of-interest/reducer';
import mobilityServices from '../flows/mobility-services/reducer';
import scholarTickets from '../flows/scholar-tickets/reducer';

const appReducer = combineReducers({
  auth,
  config,
  financial,
  form,
  help,
  init,
  login,
  menu,
  nav,
  onboarding,
  paymentMethod,
  profile,
  profileEdit,
  register,
  report,
  servicePoint,
  smartPurchase,
  toast,
  transportCard,
  facebook,
  apiStatus,
  notifications,
  phoneRecharge,
  usageTerms,
  busLines,
  searchPointsOfInterest,
  mobilityServices,
  scholarTickets,
});

export default appReducer;
