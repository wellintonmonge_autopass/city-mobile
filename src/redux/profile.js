// VouD imports
import Moment from 'moment';
import {loginActions} from './login';
import {registerActions} from './register';
import {profileEditActions} from './profile-edit';
import {initActions, asyncStorageKeys, persistData, clearData} from './init';
import {getCurrentPosition} from '../utils/geolocation';
import {getStateCurrentRouteName} from '../utils/nav-util';

// Actions
const SET_POSITION = 'voud/profile/SET_POSITION';
const SET_POSITION_FAILURE = 'voud/profile/SET_POSITION_FAILURE';
const SET_FCM_TOKEN = 'voud/profile/SET_FCM_TOKEN';
const GETTING_POSITION = 'voud/profile/GETTING_POSITION';
const SET_WATCH_POSITION_ID = 'voud/profile/SET_WATCH_POSITION_ID';

// Reducer
export const initialState = {
  data: {},
  position: {
    latitude: null,
    longitude: null,
    // default: Marco Zero de São Paulo
    defaultLat: -23.550148,
    defaultLng: -46.633879,
    geolocPermGranted: false,
    error: null,
    watchPositionId: null,
    isGettingPosition: false,
    positionReadingDate: null,
  },
  fcmToken: '',
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case initActions.HYDRATE:
      return {
        ...state,
        data: action.data.profileData || state.data,
        position: {
          ...state.position,
          ...(action.data.currentPosition && action.data.currentPosition.coords
            ? action.data.currentPosition.coords
            : {}),
          positionReadingDate: action.data.currentPosition
            ? action.data.currentPosition.positionReadingDate
            : null,
        },
      };
    case loginActions.PRE_AUTH:
      return {
        ...state,
        data: {
          ...state.data,
          cpf: action.cpf,
        },
      };
    case loginActions.PRE_AUTH_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          mobile: action.response.payload.mobile || '',
          name: action.response.payload.name
            ? action.response.payload.name.trim()
            : '',
        },
      };
    case loginActions.LOGIN_SUCCESS:
    case registerActions.REGISTER_SUCCESS:
      return {
        ...state,
        data: {
          ...action.response.payload,
          name: action.response.payload.name
            ? action.response.payload.name.trim()
            : '',
        },
      };
    case registerActions.CONFIRM_MOBILE_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          isValidMobile: true,
        },
      };
    case registerActions.CONFIRM_EMAIL_SUCCESS:
      return {
        ...state,
        data: {
          ...state.data,
          isValidEmail: true,
        },
      };
    case loginActions.LOGOUT:
      return {
        ...state,
        data: {
          cpf: state.data.cpf,
          name: state.data.name,
          mobile: state.data.mobile,
        },
      };
    case GETTING_POSITION:
      return {
        ...state,
        position: {
          ...state.position,
          isGettingPosition: true,
          error: null,
        },
      };
    case SET_POSITION:
      return {
        ...state,
        position: {
          ...state.position,
          latitude: action.latitude,
          longitude: action.longitude,
          latitudeDelta: action.latitudeDelta,
          longitudeDelta: action.longitudeDelta,
          positionReadingDate: action.positionReadingDate,
          geolocPermGranted: true,
          error: null,
          isGettingPosition: false,
        },
      };
    case SET_POSITION_FAILURE:
      return {
        ...state,
        position: {
          ...state.position,
          latitude: state.position.defaultLat,
          longitude: state.position.defaultLng,
          geolocPermGranted: false,
          error:
            'Não localizamos você. Por favor, verifique se a permissão de geolocalização está habilitada.',
          isGettingPosition: false,
        },
      };
    case SET_WATCH_POSITION_ID:
      return {
        ...state,
        position: {
          ...state.position,
          watchPositionId: action.watchId,
        },
      };

    case SET_FCM_TOKEN:
      return {
        ...state,
        fcmToken: action.token,
      };

    // Edit actions
    case profileEditActions.EDIT_PERSONAL_DATA_SUCCESS:
    case profileEditActions.EDIT_EMAIL_SUCCESS:
    case profileEditActions.EDIT_MOBILE_SUCCESS:
    case profileEditActions.EDIT_EMAIL_PREFERENCES_SUCCESS:
    case profileEditActions.EDIT_ADDRESS_SUCCESS:
      return {
        ...state,
        data: {
          authenticationToken: state.data.authenticationToken,
          ...action.response.payload,
        },
      };

    default:
      return state;
  }
}

export default reducer;

export function setPosition(latitude, longitude, latitudeDelta = null, longitudeDelta = null) {
  const positionReadingDate = Moment();
  return {
    type: SET_POSITION,
    latitude,
    longitude,
    latitudeDelta,
    longitudeDelta,
    positionReadingDate,
  };
}

export function setPositionFailed() {
  return {type: SET_POSITION_FAILURE};
}

export function gettingPosition() {
  return {type: GETTING_POSITION};
}

function _setWatchPositionId(watchId) {
  return {type: SET_WATCH_POSITION_ID, watchId};
}

export function setFCMToken(token) {
  return {
    type: SET_FCM_TOKEN,
    token,
  };
}

// Utility functions

export function persistProfile(state) {
  persistData(asyncStorageKeys.profileData, state.profile.data);
}

export function clearProfileStorage() {
  clearData(asyncStorageKeys.profileData);
}

export function getCurrentPositionWithLowAccuracyHandler(
  config,
  shouldRetry,
  retryConfig,
  disableLowAccuracyHandler = false,
) {
  return async function(dispatch, getState) {
    try {
      const lowAccuracyConfig = disableLowAccuracyHandler
        ? null
        : {
            dispatch,
            getState,
            routeName: getStateCurrentRouteName(getState().nav),
          };

      const position = await getCurrentPosition(
        config,
        shouldRetry,
        retryConfig,
        lowAccuracyConfig,
      );
      dispatch(setPosition(position.latitude, position.longitude));

      return position;
    } catch (error) {
      dispatch(setPositionFailed());
      throw error;
    }
  };
}
