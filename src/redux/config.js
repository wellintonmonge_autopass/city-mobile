// VouD imports
import {voudRequest, httpStatusCodes} from '../shared/services';
import {requestErrorHandler} from '../shared/request-error-handler';
import {initActions} from './init';
import {savePreviousPaymentCardTypeUsed} from '../utils/payment-card';
import {getDiscountCode} from '../utils/member-get-member';
import {isLoggedIn} from '../utils/auth';
import CONFIG from '../../config/config';

// Actions

const REQUEST_CONTENT = 'voud/config/REQUEST_CONTENT';
const REQUEST_CONTENT_SUCCESS = 'voud/config/REQUEST_CONTENT_SUCCESS';
const REQUEST_CONTENT_FAILURE = 'voud/config/REQUEST_CONTENT_FAILURE';

const ADD_DISCOUNT_CODE = 'voud/config/ADD_DISCOUNT_CODE';
const CLEAR_DISCOUNT_CODE = 'voud/config/CLEAR_DISCOUNT_CODE';
const SET_PREVIOUS_PAYMENT_CARD_TYPE_USED =
  'voud/config/SET_PREVIOUS_PAYMENT_CARD_TYPE_USED';

const SET_DEVICE_ID = 'voud/config/SET_DEVICE_ID';

export const configActions = {
  REQUEST_CONTENT_SUCCESS,
};

const hasConfigUiError = configUi =>
  configUi.error !== '' &&
  configUi.errorStatusCode !== httpStatusCodes.UNSUPPORTED_VERSION &&
  configUi.errorStatusCode !== httpStatusCodes.SERVICE_UNAVAILABLE;

const hasCheckLastTermAcceptedUiError = termsAcceptedUi =>
  termsAcceptedUi.error !== '' &&
  termsAcceptedUi.errorStatusCode !== httpStatusCodes.UNSUPPORTED_VERSION &&
  termsAcceptedUi.errorStatusCode !== httpStatusCodes.SERVICE_UNAVAILABLE &&
  termsAcceptedUi.errorStatusCode !== httpStatusCodes.SESSION_EXPIRED;

export const hasConfigError = (configUi, termsAcceptedUi) =>
  hasConfigUiError(configUi) ||
  hasCheckLastTermAcceptedUiError(termsAcceptedUi);

// Reducer
const initialState = {
  isFetching: false,
  requested: null,
  error: '',
  errorStatusCode: null,
  content: null,
  activeDiscountCode: null,
  previousPaymentCardTypeUsed: null,
  deviceId: null,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case initActions.HYDRATE:
      return {
        ...state,
        previousPaymentCardTypeUsed: action.data.previousPaymentCardTypeUsed,
      };
    case REQUEST_CONTENT:
      return {
        ...state,
        requested: new Date(),
        isFetching: true,
      };
    case REQUEST_CONTENT_SUCCESS:
      return {
        ...state,
        isFetching: false,
        error: '',
        errorStatusCode: null,
        content: action.response.payload.sort((a, b) => a.id - b.id),
      };
    case REQUEST_CONTENT_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: action.error,
        errorStatusCode: action.errorStatusCode,
      };
    case ADD_DISCOUNT_CODE:
      return {
        ...state,
        activeDiscountCode: action.discountCode,
      };
    case CLEAR_DISCOUNT_CODE: {
      return {
        ...state,
        activeDiscountCode: null,
      };
    }
    case SET_PREVIOUS_PAYMENT_CARD_TYPE_USED:
      return {
        ...state,
        previousPaymentCardTypeUsed: action.paymentCardType,
      };

    case SET_DEVICE_ID:
      return {
        ...state,
        deviceId: action.deviceId,
      };

    default:
      return state;
  }
}

export default reducer;

// Actions creators

// Request service points
function requestContent() {
  return {type: REQUEST_CONTENT};
}
function requestContentSuccess(response) {
  return {
    type: REQUEST_CONTENT_SUCCESS,
    response,
  };
}
function requestContentFailure(error, errorStatusCode) {
  return {
    type: REQUEST_CONTENT_FAILURE,
    error,
    errorStatusCode,
  };
}

// Discount code
export function addDiscountCode(discountCode) {
  return {
    type: ADD_DISCOUNT_CODE,
    discountCode,
  };
}

export function clearDiscountCode() {
  return {
    type: CLEAR_DISCOUNT_CODE,
  };
}

// Payment Card
export function setPreviousPaymentCardTypeUsed(paymentCardType) {
  savePreviousPaymentCardTypeUsed(paymentCardType);
  return {type: SET_PREVIOUS_PAYMENT_CARD_TYPE_USED, paymentCardType};
}

export function setDeviceId(deviceId) {
  return {
    type: SET_DEVICE_ID,
    deviceId,
  };
}

// Thunk action creators
export function fetchContent() {
  return function(dispatch) {
    // dispatch request action
    dispatch(requestContent());

    (async () => {
      try {
        const response = await voudRequest('/content/list', 'GET');

        if (!response.payload) response.payload = [];

        dispatch(requestContentSuccess(response));
      } catch (error) {
        if (__DEV__) console.tron.log(error.message, true);
        requestErrorHandler(
          dispatch,
          error,
          requestContentFailure(error.message, error.statusCode),
        );
      }
    })();
  };
}

export function handleDiscountCode(openedLink = null) {
  return async function(dispatch, getState) {
    const {data} = getState().profile;
    const {viewed} = getState().onboarding;

    const userIsLoggedIn = isLoggedIn(data);
    const discountCode = await getDiscountCode(openedLink);

    const isDiscountUseInvalid = userIsLoggedIn && discountCode;

    if (isDiscountUseInvalid) {
      dispatch(clearDiscountCode());
    } else {
      dispatch(addDiscountCode(discountCode));
    }

    return {
      userIsLoggedIn,
      discountCode,
      onboardingViewed: viewed,
    };
  };
}
