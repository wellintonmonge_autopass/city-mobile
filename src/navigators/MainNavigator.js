// NPM imports
import {StackNavigator} from 'react-navigation';

// VouD imports
import MenuNavigator from './MenuNavigator';
import {routeNames} from '../shared/route-names';
import {
  BuyCredit,
  CardDetails,
  SelectPaymentMethod,
  EditSmartPurchase,
  ScholarTicketDetail,
  ScholarTicketsRevalidate,
  Camera,
} from '../screens';
import handleFeedBack from '../flows/scholar-tickets/screens/handleFeedBack';

// Navigator component
let MainNavigator = StackNavigator(
  {
    [routeNames.MENU_NAVIGATOR]: {screen: MenuNavigator},
    [routeNames.CARD_DETAILS]: {screen: CardDetails},
    [routeNames.BUY_CREDIT]: {screen: BuyCredit},
    [routeNames.SELECT_PAYMENT_METHOD]: {screen: SelectPaymentMethod},
    [routeNames.EDIT_SMART_PURCHASE]: {screen: EditSmartPurchase},
    [routeNames.SCHOLAR_TICKET_DETAILS]: {screen: ScholarTicketDetail},
    [routeNames.SCHOLAR_TICKET_REVALIDATE]: {screen: ScholarTicketsRevalidate},
    [routeNames.CAMERA_VIEW]: {
      screen: Camera,
    },
    [routeNames.HANDLE_FEEDBACK]: {
      screen: handleFeedBack,
    },
  },
  {
    cardStyle: {backgroundColor: 'white'},
    initialRouteName: routeNames.MENU,
    navigationOptions: {
      gesturesEnabled: false,
      header: null,
    },
  },
);

MainNavigator.navigationOptions = {
  header: null,
};

export default MainNavigator;
