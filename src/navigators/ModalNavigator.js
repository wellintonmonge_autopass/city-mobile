// NPM imports
import {StackNavigator} from 'react-navigation';

// VouD imports
import MainNavigator from '../navigators/MainNavigator';
import {routeNames} from '../shared/route-names';

import {
  Onboarding,
  ServicePointDetails,
  HelpDetails,
  AddCard,
  EditCard,
  NextRecharges,
  SchoolCardDetails,
  EditPassword,
  EditEmail,
  EditMobile,
  EditAddress,
  PurchaseHistoryDetail,
  PaymentError,
  AddPaymentMethod,
  EditPersonalData,
  ServiceUnavailable,
  AcceptUsageTerms,
  SearchRoutes,
  TransportSearchResult,
  SearchPointsOfInterest,
  TraceRoutes,
  Camera,
} from '../screens';

import {BusLineDetails} from '../flows/bus-lines/screens/details';
import {
  MobilityServicesSearchAddress,
  MobilityServices,
  MobilityServicesEstimates,
  MobilityServicesConfirmation,
} from '../flows/mobility-services/screens';
import handleFeedBack from '../flows/scholar-tickets/screens/handleFeedBack';

// create custom transitioner without the opacity animation, ie. for iOS
function forVertical(props) {
  const {layout, position, scene} = props;

  const index = scene.index;
  const height = layout.initHeight;

  const translateX = 0;
  const translateY = position.interpolate({
    inputRange: [index - 1, index, index + 1],
    outputRange: [height, 0, 0],
  });

  return {
    transform: [{translateX}, {translateY}],
  };
}

const ModalNavigator = StackNavigator(
  {
    [routeNames.MAIN_NAVIGATOR]: {screen: MainNavigator},
    [routeNames.ONBOARDING]: {screen: Onboarding},
    [routeNames.ADD_CARD]: {screen: AddCard},
    [routeNames.EDIT_CARD]: {screen: EditCard},
    [routeNames.NEXT_RECHARGES]: {screen: NextRecharges},
    [routeNames.SCHOOL_CARD_DETAILS]: {screen: SchoolCardDetails},
    [routeNames.SERVICE_POINTS_DETAILS]: {screen: ServicePointDetails},
    [routeNames.HELP_DETAILS]: {screen: HelpDetails},
    [routeNames.EDIT_PERSONAL_DATA]: {screen: EditPersonalData},
    [routeNames.EDIT_PASSWORD]: {screen: EditPassword},
    [routeNames.EDIT_EMAIL]: {screen: EditEmail},
    [routeNames.EDIT_MOBILE]: {screen: EditMobile},
    [routeNames.EDIT_ADDRESS]: {screen: EditAddress},
    [routeNames.PURCHASE_HISTORY_DETAIL]: {screen: PurchaseHistoryDetail},
    [routeNames.PAYMENT_ERROR]: {screen: PaymentError},
    [routeNames.ADD_PAYMENT_METHOD]: {screen: AddPaymentMethod},
    [routeNames.SERVICE_UNAVAILABLE]: {screen: ServiceUnavailable},
    [routeNames.ACCEPT_USAGE_TERMS]: AcceptUsageTerms,
    [routeNames.BUS_LINE_DETAILS]: BusLineDetails,
    [routeNames.SEARCH_ROUTES]: {screen: SearchRoutes},
    [routeNames.SEARCH_POINTS_OF_INTEREST]: {screen: SearchPointsOfInterest},
    [routeNames.TRANSPORT_SEARCH_RESULT]: {screen: TransportSearchResult},
    [routeNames.MOBILITY_SERVICES]: {screen: MobilityServices},
    [routeNames.MOBILITY_SERVICES_SEARCH_ADDRESS]: {
      screen: MobilityServicesSearchAddress,
    },
    [routeNames.MOBILITY_SERVICES_ESTIMATES]: {
      screen: MobilityServicesEstimates,
    },
    [routeNames.MOBILITY_SERVICES_CONFIRMATION]: {
      screen: MobilityServicesConfirmation,
    },
    [routeNames.TRACE_ROUTES]: {
      screen: TraceRoutes,
    },
  },
  {
    mode: 'modal',
    navigationOptions: {
      gesturesEnabled: false,
      header: null,
    },
    cardStyle: {backgroundColor: 'transparent'},
    transitionConfig: () => ({screenInterpolator: forVertical}),
  },
);

export default ModalNavigator;
