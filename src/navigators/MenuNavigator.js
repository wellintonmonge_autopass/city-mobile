// NPM imports
import React, {Component} from 'react';
import SideMenu from 'react-native-side-menu';
import {connect} from 'react-redux';

import {
  Dimensions,
  StyleSheet,
  View,
  Animated,
  Easing,
  BackHandler,
} from 'react-native';

import {
  addNavigationHelpers,
  createNavigator,
  TabRouter,
  Transitioner,
} from 'react-navigation';

// VouD imports
import {openMenu, closeMenu} from '../redux/menu';
import {routeNames} from '../shared/route-names';
import {BusLines} from '../flows/bus-lines/screens';

import {
  Menu,
  Home,
  PurchaseHistory,
  ServicePoints,
  Help,
  MyProfile,
  PaymentMethods,
  SmartPurchase,
  SearchPointsOfInterest,
  ScholarTickets,
  ScholarTicketsRevalidate,
  ScholarTicketDetail,
} from '../screens';

// component
class MenuView extends Component {
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler);
  }

  _backHandler = () => {
    const {isOpen, dispatch} = this.props;
    if (isOpen) {
      dispatch(closeMenu());
      return true;
    }

    return false;
  };

  _configureTransition = (transitionProps, prevTransitionProps) => {
    return {
      duration: 1000,
      easing: Easing.bezier(0.2833, 0.99, 0.31833, 0.99),
      timing: Animated.timing,
    };
  };

  _renderScene = (transitionProps, onEnter) => {
    const {progress, scene} = transitionProps;
    const opacity = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });
    const Scene = this.props.router.getComponentForRouteName(
      scene.route.routeName,
    );

    if (onEnter)
      return (
        <Animated.View
          style={StyleSheet.flatten([styles.animatedViewContainer, {opacity}])}
          key={scene.key}>
          <Scene
            navigation={addNavigationHelpers({
              ...this.props.navigation,
              state: this.props.navigation.state.routes[scene.index],
            })}
          />
        </Animated.View>
      );

    return (
      <Animated.View key={scene.key} style={styles.previousViewContainer}>
        <Scene />
      </Animated.View>
    );
  };

  _render = (transitionProps, prevTransitionProps) => {
    const previousScene = prevTransitionProps
      ? this._renderScene(prevTransitionProps, false)
      : null;
    const actualScene = this._renderScene(transitionProps, true);
    const menu = <Menu navigation={this.props.navigation} />;

    return (
      <SideMenu
        menu={menu}
        isOpen={this.props.isOpen}
        onChange={this.updateMenuState}
        openMenuOffset={Dimensions.get('window').width - 56}>
        <View style={styles.mainContainer}>
          {previousScene}
          {actualScene}
        </View>
      </SideMenu>
    );
  };

  updateMenuState = isOpen => {
    if (isOpen) this.props.dispatch(openMenu());
    else this.props.dispatch(closeMenu());
  };

  render() {
    return (
      <Transitioner
        configureTransition={this._configureTransition}
        navigation={this.props.navigation}
        render={this._render}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    isOpen: state.menu.isOpen,
  };
};
const MenuViewContainer = connect(mapStateToProps)(MenuView);

const MenuRouter = TabRouter(
  {
    [routeNames.HOME]: {screen: Home},
    [routeNames.SMART_PURCHASE]: {screen: SmartPurchase},
    [routeNames.PURCHASE_HISTORY]: {screen: PurchaseHistory},
    [routeNames.SERVICE_POINTS]: {screen: ServicePoints},
    [routeNames.BUS_LINES]: {screen: BusLines},
    [routeNames.HELP]: {screen: Help},
    [routeNames.MY_PROFILE]: {screen: MyProfile},
    [routeNames.PAYMENT_METHODS]: {screen: PaymentMethods},
    [routeNames.SEARCH_POINTS_OF_INTEREST]: {screen: SearchPointsOfInterest},
    [routeNames.SCHOLAR_TICKETS]: {screen: ScholarTickets},
  },
  {
    initialRouteName: routeNames.HOME,
  },
);

const MenuNavigator = createNavigator(MenuRouter)(MenuViewContainer);

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    shadowColor: 'black',
    shadowOffset: {
      width: -4,
      height: 0,
    },
    shadowOpacity: 0.25,
    elevation: 8,
  },
  previousViewContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  animatedViewContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'white',
    opacity: 0,
  },
});

export default MenuNavigator;
