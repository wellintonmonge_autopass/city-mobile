// NPM imports
import React, {Component} from 'react';
import {BackHandler} from 'react-native';
import {NavigationActions} from 'react-navigation';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {addNavigationHelpers, StackNavigator} from 'react-navigation';
import {
  createReduxBoundAddListener,
  createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';

// VouD imports
import ModalNavigator from './ModalNavigator';
import {waitAndNavigateToRoute} from '../redux/nav';
import {routeNames} from '../shared/route-names';
import {colors} from '../styles/constants';

import {
  Auth,
  AddCardHelperDialog,
  TransportCardRechargeSuccessful,
  Browser,
  UnsupportedVersionDialog,
  SelectStateDialog,
  UnsupportedDeviceDialog,
  PurchaseConfirmationDialog,
  Camera,
  ScholarTicketDetail,
  ScholarTicketsRevalidate,
} from '../screens';
import {isEmulator} from '../utils/device-info';
import {getStateCurrentRouteName} from '../utils/nav-util';
import handleFeedBack from '../flows/scholar-tickets/screens/handleFeedBack';

// create custom transitioner for dialogs
function getDialogInterpolator(props) {
  const {position, scene} = props;

  const index = scene.index;

  const translateX = 0;
  const translateY = 0;

  const opacity = position.interpolate({
    inputRange: [index - 1, index, index + 1],
    outputRange: [0, 1, 1],
  });

  return {
    transform: [{translateX}, {translateY}],
    opacity,
  };
}

// export app navigator to control routes
export const AppNavigator = StackNavigator(
  {
    [routeNames.MODAL_NAVIGATOR]: {screen: ModalNavigator},
    [routeNames.AUTH]: {screen: Auth},
    [routeNames.ADD_CARD_HELPER_DIALOG]: {screen: AddCardHelperDialog},
    [routeNames.PAYMENT_SUCCESSFUL]: {screen: TransportCardRechargeSuccessful},
    [routeNames.BROWSER]: {screen: Browser},
    [routeNames.UNSUPPORTED_VERSION_DIALOG]: {screen: UnsupportedVersionDialog},
    [routeNames.UNSUPPORTED_DEVICE_DIALOG]: {screen: UnsupportedDeviceDialog},
    [routeNames.SELECT_STATE_DIALOG]: {screen: SelectStateDialog},
    [routeNames.PURCHASE_CONFIRMATION_DIALOG]: {
      screen: PurchaseConfirmationDialog,
    },
  },
  {
    mode: 'modal',
    navigationOptions: {
      gesturesEnabled: false,
      header: null,
    },
    cardStyle: {
      backgroundColor: colors.OVERLAY,
      shadowOpacity: 0,
    },
    transitionConfig: () => ({screenInterpolator: getDialogInterpolator}),
  },
);

// Note: createReactNavigationReduxMiddleware must be run before createReduxBoundAddListener
export const reactNavigationMiddleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav,
);
const addListener = createReduxBoundAddListener('root');

export class AppWithNavigationState extends Component {
  componentWillMount() {
    const {dispatch} = this.props;

    BackHandler.addEventListener('hardwareBackPress', this._backHandler);

    if (isEmulator()) {
      dispatch(waitAndNavigateToRoute(routeNames.UNSUPPORTED_DEVICE_DIALOG));
      return;
    }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler);
  }

  _backHandler = () => {
    const {dispatch, nav} = this.props;
    const currentRouteName = getStateCurrentRouteName(nav);

    if (currentRouteName !== routeNames.HOME) {
      dispatch(NavigationActions.back());
      return true;
    }

    return false;
  };

  render() {
    const {dispatch, nav} = this.props;
    return (
      <AppNavigator
        navigation={addNavigationHelpers({
          dispatch,
          state: nav,
          addListener,
        })}
      />
    );
  }
}

AppWithNavigationState.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);
