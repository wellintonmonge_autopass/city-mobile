// NPM imports
import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

// VouD imports
import Header, { headerActionTypes } from '../../components/Header';
import RequestFeedback from '../../components/RequestFeedback';
import { openMenu } from '../../redux/menu';
import { fetchHelpTopics, viewHelpDetails } from '../../redux/help';
import { getFaqList, getFaqListUI } from '../../redux/selectors';
import { routeNames } from '../../shared/route-names';
import { hasNotch } from '../../utils/hasNotch';
import { dimensions } from '../../styles/constants';

// Group imports
import QuestionsList from './QuestionsList';
import { navigateToRoute } from '../../redux/nav';

// Screen component
class HelpView extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        // fetch help topics if needed
        if (!this.props.requestUi.requested)
            this._fetchHelpTopics();
    }

    openMenu = () => {
        const { dispatch } = this.props;
        dispatch(openMenu());
    };

    _fetchHelpTopics = () => {
        this.props.dispatch(fetchHelpTopics());
    };

    _viewDetails = (id) => {
        const { dispatch } = this.props;
        dispatch(viewHelpDetails(id));
        dispatch(navigateToRoute(routeNames.HELP_DETAILS));
    };

    _renderContent = () => {
        const { helpTopics } = this.props;
        const { error, isFetching } = this.props.requestUi;

        return helpTopics[0] ?
        (
            <QuestionsList 
                itemList={this.props.helpTopics}
                onPress={this._viewDetails}
            />
        ) :
        (
            <View style={styles.requestFeedbackContainer}>
                <RequestFeedback
                    loadingMessage="Carregando dúvidas e respostas..."
                    errorMessage={error}
                    emptyMessage="Não foram encontrados tópicos de ajuda"
                    retryMessage="Tentar novamente"
                    isFetching={isFetching}
                    onRetry={this._fetchHelpTopics}
                />
            </View>
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Header
                    title="Dúvidas Frequentes"
                    left={{
                        type: headerActionTypes.MENU,
                        onPress: this.openMenu
                    }}
                />
                {this._renderContent()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: hasNotch() ? dimensions.notchSpace.top : 0,
    },
    requestFeedbackContainer: {
        flex: 1,
        justifyContent: 'center'
    }
});

// Redux
const mapStateToProps = state => {
    return {
        helpTopics: getFaqList(state),
        requestUi: getFaqListUI(state),
        isMenuOpen: state.menu.isOpen
    };
};

export const Help = connect(mapStateToProps)(HelpView);

// Export other screens from group
export * from './HelpDetails';