// NPM imports
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {
  Alert,
  Dimensions,
  Image,
  StyleSheet,
  ScrollView,
  View,
} from 'react-native';

// VouD imports
import Button from '../../components/Button';
import {logout} from '../../redux/login';
import {closeMenu} from '../../redux/menu';
import {colors, getStatusBarHeight, dimensions} from '../../styles';
import {viewDetails} from '../../redux/transport-card';
import {navigateToRoute, navigateFromHome} from '../../redux/nav';
import {GAEventParams, GATrackEvent} from '../../shared/analytics';
import {routeNames} from '../../shared/route-names';
import {getIsLoggedIn, getProfileAlertCount} from '../../redux/selectors';
import {hasNotch} from '../../utils/hasNotch';

// Group imports
import MenuItem from './MenuItem';
import MenuSubItem from './MenuSubItem';
import {openSupportEmail} from '../../utils/mailto-util';

// images
const voudLogo = require('../../images/logo.png');

// aux consts
const window = Dimensions.get('window');

// component
class MenuView extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  _renderMenuItems = (items, isSecondary = false) => {
    return items.map(item => (
      <MenuItem
        onPress={() =>
          this._navigate(item.routeName, item.eventLabel, item.routeParams)
        }
        text={item.text}
        icon={item.icon}
        isSecondary={isSecondary}
        key={item.routeName}
      />
    ));
  };

  renderCards = () => {
    const {cards, dispatch} = this.props;
    const {MENU_TRANSPORT_CARD_DETAILS} = GAEventParams.labels;
    return cards.map(card => (
      <MenuSubItem
        onPress={() => {
          dispatch(viewDetails(card.uuid));
          this._navigate(routeNames.CARD_DETAILS, MENU_TRANSPORT_CARD_DETAILS);
        }}
        text={card.nick}
        key={card.uuid}
        cardData={card}
      />
    ));
  };

  renderContent = () => {
    const {
      MENU_HOME,
      MENU_SERVICE_POINTS,
      SCHOLAR_TICKETS,
      MENU_HELP,
      MENU_LOGIN,
      // MENU_TRANSPORT_CARDS_COVERAGE
    } = GAEventParams.labels;

    const items = [
      {
        text: 'Início',
        icon: 'home',
        routeName: routeNames.HOME,
        eventLabel: MENU_HOME,
      },
      // {
      //   text: 'Cobertura BOM e BU',
      //   icon: 'md-map',
      //   routeName: routeNames.TRANSPORT_CARDS_COVERAGE,
      //   eventLabel: MENU_TRANSPORT_CARDS_COVERAGE,
      // },
      {
        text: 'Pontos de recarga',
        icon: 'pin',
        routeName: routeNames.SERVICE_POINTS,
        eventLabel: MENU_SERVICE_POINTS,
      },
      // {
      //   text: 'Denúncias',
      //   icon: 'feedback',
      //   routeName: routeNames.REPORTS,
      //   eventLabel: MENU_REPORT,
      // },
      {
        text: 'Dúvidas Frequentes',
        icon: 'help-outline',
        routeName: routeNames.HELP,
        eventLabel: MENU_HELP,
      },
    ];

    return (
      <ScrollView scrollsToTop={false} contentContainerStyle={styles.content}>
        <Image source={voudLogo} style={styles.logo} />
        {this._renderMenuItems(items)}
        <View style={styles.hr} />
        <View style={styles.loginInfo}>
          <Button
            outline
            onPress={() => this._navigate(routeNames.AUTH, MENU_LOGIN)}>
            Cadastre-se ou{'\n'}faça seu login
          </Button>
        </View>
      </ScrollView>
    );
  };

  renderLoggedContent = () => {
    const {hasNoDataScholarTickets} = this.props;
    const {
      MENU_PURCHASE_HISTORY,
      MENU_SERVICE_POINTS,
      MENU_HELP,
      MENU_TRANSPORT_CARDS,
      MENU_ADD_CARD,
      MENU_MY_PROFILE,
      MENU_PAYMENT_METHODS,
      SCHOLAR_TICKETS,
      SCHOLAR_TICKETS_REVALIDATE,
      MENU_SEARCH_POINTS_OF_INTEREST,
    } = GAEventParams.labels;

    const items = [
      {
        text: 'Histórico de compras',
        icon: 'history',
        routeName: routeNames.PURCHASE_HISTORY,
        eventLabel: MENU_PURCHASE_HISTORY,
      },
      {
        text: 'Pontos de recarga',
        icon: 'pin',
        routeName: routeNames.SERVICE_POINTS,
        eventLabel: MENU_SERVICE_POINTS,
      },
      {
        text: 'Rotas e trajetos',
        icon: 'bus',
        routeName: routeNames.SEARCH_POINTS_OF_INTEREST,
        eventLabel: MENU_SEARCH_POINTS_OF_INTEREST,
      },
      {
        text: 'Revalidar Cartão Escolar',
        icon: 'revalidar',
        routeName: !hasNoDataScholarTickets
          ? routeNames.SCHOLAR_TICKETS
          : routeNames.SCHOLAR_TICKET_REVALIDATE,
        eventLabel: !hasNoDataScholarTickets
          ? SCHOLAR_TICKETS
          : SCHOLAR_TICKETS_REVALIDATE,
      },
      {
        text: 'Ajuda',
        icon: 'help-outline',
        routeName: routeNames.HELP,
        eventLabel: MENU_HELP,
      },
    ];

    return (
      <ScrollView scrollsToTop={false} contentContainerStyle={styles.content}>
        <Image source={voudLogo} style={styles.logo} />
        <MenuItem
          onPress={() => this._navigate(routeNames.HOME, MENU_TRANSPORT_CARDS)}
          text="Meus Cartões"
          icon="bom-card"
        />
        {this.renderCards()}
        <MenuSubItem
          onPress={() => this._navigate(routeNames.ADD_CARD, MENU_ADD_CARD)}
          text="Adicionar cartão"
        />
        <View style={styles.hr} />
        {this._renderMenuItems(items)}
        {/* <View style={styles.hr} /> */}
        {/* {this._renderMenuItems(secondaryItems, true)} */}
        <MenuItem
          onPress={this._openSupportEmailUrl}
          text="Suporte"
          icon="mail"
        />
        <View style={styles.hr} />
        <MenuItem text={this.props.userData.name} icon="people" />
        <MenuSubItem
          onPress={() => this._navigate(routeNames.MY_PROFILE, MENU_MY_PROFILE)}
          text="Meu perfil"
          badge={this.props.profileAlertCount}
        />
        <MenuSubItem
          onPress={() =>
            this._navigate(routeNames.PAYMENT_METHODS, MENU_PAYMENT_METHODS)
          }
          text="Minhas formas de pagamento"
        />
        <MenuSubItem onPress={this._logout} text="Sair" />
      </ScrollView>
    );
  };

  _openSupportEmailUrl = () => {
    const {userData, dispatch} = this.props;

    // Open mailto
    openSupportEmail(userData);

    // Dispatch GATrackEvent for support
    const {
      categories: {BUTTON},
      actions: {CLICK},
      labels: {MENU_SUPPORT},
    } = GAEventParams;
    GATrackEvent(BUTTON, CLICK, MENU_SUPPORT);

    // Close Menu
    dispatch(closeMenu());
  };

  _navigate = (routeName, eventLabel = '') => {
    const {dispatch} = this.props;
    dispatch(closeMenu());

    const {
      categories: {BUTTON},
      actions: {CLICK},
      labels: {MENU_TRANSPORT_CARD_DETAILS},
    } = GAEventParams;
    if (routeName === routeNames.CARD_DETAILS) {
      GATrackEvent(BUTTON, CLICK, MENU_TRANSPORT_CARD_DETAILS);
      dispatch(navigateFromHome(routeName));
    } else {
      GATrackEvent(BUTTON, CLICK, eventLabel);
      dispatch(navigateToRoute(routeName));
    }
  };

  _logout = () => {
    const {dispatch} = this.props;

    Alert.alert('Sair', 'Você tem certeza que deseja sair do City +?', [
      {
        text: 'Cancelar',
        onPress: () => {},
      },
      {
        text: 'Sim',
        onPress: () => {
          dispatch(closeMenu());

          const {
            categories: {BUTTON},
            actions: {CLICK},
            labels: {MENU_LOGOUT},
          } = GAEventParams;
          GATrackEvent(BUTTON, CLICK, MENU_LOGOUT);
          dispatch(logout());
        },
      },
    ]);
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.statusBarSpaceHolder} />
        {this.props.isLoggedIn
          ? this.renderLoggedContent()
          : this.renderContent()}
        {__DEV__ && (
          <Button
            onPress={() => {
              throw new Error();
            }}>
            Reload
          </Button>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: hasNotch() ? dimensions.notchSpace.top : 0,
    flex: 1,
    width: window.width,
    backgroundColor: colors.BRAND_PRIMARY_LIGHTER,
    paddingRight: 56, // menu offset
  },
  statusBarSpaceHolder: {
    height: getStatusBarHeight(),
  },
  content: {
    paddingTop: 16,
    paddingBottom: 16,
    paddingHorizontal: 8,
  },
  logo: {
    alignSelf: 'center',
    marginBottom: 16,
    resizeMode: 'stretch',
  },
  hr: {
    alignSelf: 'stretch',
    height: 1,
    marginVertical: 16,
    backgroundColor: colors.BLACK_ALPHA1,
  },
  loginInfo: {
    paddingHorizontal: 16,
  },
});

// redux connect and export
const mapStateToProps = state => {
  return {
    cards: state.transportCard.list,
    userData: state.profile.data,
    profileAlertCount: getProfileAlertCount(state),
    isLoggedIn: getIsLoggedIn(state),
    hasNoDataScholarTickets: state.scholarTickets.scholarTickets.data === 0,
  };
};

export const Menu = connect(mapStateToProps)(MenuView);
