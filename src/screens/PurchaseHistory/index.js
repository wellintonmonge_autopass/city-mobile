// NPM imports
import React, { Component } from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import Moment from 'moment';

// VouD imports
import Header, { headerActionTypes } from '../../components/Header';
import Icon from '../../components/Icon';
import { openMenu } from '../../redux/menu';
import { fetchPurchaseList, viewPurchaseDetails, requestPurchaseListClear, paymentCardTypes } from '../../redux/financial';
import TouchableNative from '../../components/TouchableNative';
import BrandText from '../../components/BrandText';
import SystemText from '../../components/SystemText';
import TouchableText from '../../components/TouchableText';
import RequestFeedback from '../../components/RequestFeedback';
import { formatCurrency } from '../../utils/parsers-formaters';
import PurchaseStatus from './PurchaseStatus';
import { colors, dimensions } from '../../styles';
import { routeNames } from '../../shared/route-names';
import { navigateToRoute } from '../../redux/nav';
import { isMobileProduct, isTransportCardProduct, getPurchaseStatusText } from '../../utils/purchase-history';
import { getPurchaseListUI } from '../../redux/selectors';
import { hasNotch } from '../../utils/hasNotch';

// Screen component
class PurchaseHistoryView extends Component {
  componentDidMount() {
    this._fetchPurchaseHistory();
  }

  componentWillUnmount() {
    this.props.dispatch(requestPurchaseListClear());
  }

  openMenu = () => {
    const { dispatch } = this.props;
    dispatch(openMenu());
  };

  _goToDetail = (purchaseId) => {
    const { dispatch } = this.props;
    dispatch(viewPurchaseDetails(purchaseId));
    dispatch(navigateToRoute(routeNames.PURCHASE_HISTORY_DETAIL));
  };

  _getStatusCode = (status) => {
    switch (status) {
      case 'ERROR':
      case 'CANCELED':
        return 0;
      case 'PROCESSING':
        return 1;
      case 'PROCCESSED':
        return 3;
    }
  }

  _fetchPurchaseHistory = () => {
    const { dispatch, customerId, page } = this.props;
    dispatch(fetchPurchaseList(customerId, page));
  }

  _loadMore = () => {
    const { dispatch, customerId, page } = this.props;
    dispatch(fetchPurchaseList(customerId, page));
  }

  render() {
    const {
      historyList,
      purchaseListUi,
      reachedEnd,
    } = this.props;

    return (
      <View style={styles.container}>
        <Header
          title="Histórico de compras"
          left={{
            type: headerActionTypes.MENU,
            onPress: this.openMenu
          }}
        />

        {/* ================ */}
        {/* REQUEST FEEDBACK */}
        {/* ================ */}
        {
          ((purchaseListUi.isFetching && historyList.length === 0) || historyList.length === 0 || purchaseListUi.error !== '') &&
          <View style={styles.requestFeedbackContainer}>
            <RequestFeedback
              loadingMessage="Carregando histórico de compras..."
              errorMessage={purchaseListUi.error}
              emptyMessage="Não foi encontrado histórico de compras"
              retryMessage="Tentar novamente"
              isFetching={purchaseListUi.isFetching}
              onRetry={this._fetchPurchaseHistory}
            />
          </View>
        }

        {/* =============== */}
        {/* === CONTENT === */}
        {/* =============== */}
        {
          (purchaseListUi.error === '' && historyList && historyList.length > 0) &&
          <ScrollView>
            {
              historyList.map(history => {
                const statusCode = this._getStatusCode(history.status);
                const isDebit = history.type === paymentCardTypes.DEBIT;
                const productType = history.productType ? history.productType : '';

                return (
                  <TouchableNative key={history.id} style={styles.historyListContainer} onPress={() => this._goToDetail(history.id)}>
                    <View style={styles.historyListStatus}>
                      {
                        statusCode > 0 && isTransportCardProduct(productType) &&
                        <PurchaseStatus maxStatus={3} currentStatus={statusCode} />
                      }
                      {
                        statusCode > 0 &&  isMobileProduct(productType) &&
                        <PurchaseStatus maxStatus={1} currentStatus={statusCode} />
                      }
                      <BrandText style={statusCode > 0 ? styles.historyListStatusText : styles.historyListStatusTextCanceled}>
                        {getPurchaseStatusText(history.status, isDebit, isMobileProduct(productType))}
                      </BrandText>
                    </View>
                    <View style={styles.historyListData}>
                      <View style={styles.historyListValueContainer}>
                        {
                          history.recurrent &&
                          <Icon
                            name="automatic-purchase"
                            style={styles.purchaseTypeIcon}
                          />
                        }
                        {
                          isMobileProduct(productType) &&
                          <Icon
                            name="phone"
                            style={styles.purchaseTypeIcon}
                          />
                        }
                        <SystemText style={statusCode > 0 ? styles.historyListValueText : styles.historyListValueTextCanceled}>
                          R$ {formatCurrency(history.transactionValue)}
                        </SystemText>
                      </View>
                      <SystemText style={styles.historyListDateText}>
                        {Moment(history.createdDate).format("DD/MM/YYYY")}
                      </SystemText>
                    </View>
                  </TouchableNative>
                );
              })
            }

            {/* =============== */}
            {/* == LOAD MORE == */}
            {/* =============== */}
            {
              !reachedEnd && !purchaseListUi.isFetching &&
              <TouchableText
                onPress={this._loadMore}
                style={styles.nextPageButton}
              >
                Carregar mais
              </TouchableText>
            }

            {
              purchaseListUi.isFetching &&
              <BrandText style={styles.loadingText}>
                Carregando...
              </BrandText>
            }
          </ScrollView>
        }
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: hasNotch() ? dimensions.notchSpace.top : 0,
  },
  historyListContainer: {
    flexDirection: 'row',
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: colors.GRAY_LIGHTER,
  },
  historyListStatus: {
    flex: 1,
    padding: 16,
    paddingLeft: 0,
    borderRadius: 2
  },
  historyListStatusText: {
    marginTop: 8,
    fontSize: 12,
    lineHeight: 16,
    color: colors.GRAY_DARKER,
    textAlign: 'center'
  },
  historyListStatusTextCanceled: {
    fontSize: 12,
    lineHeight: 16,
    color: colors.GRAY,
    textAlign: 'center'
  },
  historyListData: {
    flex: 1,
    justifyContent: 'center',
    marginLeft: 16,
  },
  historyListValueContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  purchaseTypeIcon: {
    marginRight: 8,
    fontSize: 24,
    color: colors.BRAND_SECONDARY,
  },
  historyListValueText: {
    fontSize: 20,
    lineHeight: 24,
    color: colors.BRAND_PRIMARY
  },
  historyListValueTextCanceled: {
    fontSize: 20,
    lineHeight: 24,
    textDecorationLine: 'line-through',
    color: colors.GRAY_LIGHT
  },
  historyListDateText: {
    marginTop: 8,
    fontSize: 14,
    lineHeight: 20,
    color: colors.GRAY,
  },
  historyListCard: {
    justifyContent: 'center'
  },
  nextPageButton: {
    marginVertical: 24,
    marginHorizontal: 16
  },
  requestFeedbackContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  loadingText: {
    height: 32,
    marginVertical: 24,
    fontSize: 14,
    textAlign: 'center',
    color: colors.BRAND_PRIMARY
  }
});

function mapStateToProps(state) {
  return {
    page: state.financial.purchaseList.page,
    purchaseListUi: getPurchaseListUI(state),
    reachedEnd: state.financial.purchaseList.reachedEnd,
    historyList: state.financial.purchaseList.data,
    customerId: state.profile.data.id
  }
}

export const PurchaseHistory = connect(mapStateToProps)(PurchaseHistoryView);

export * from './PurchaseHistoryDetail';
