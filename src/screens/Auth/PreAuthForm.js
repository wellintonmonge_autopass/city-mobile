// NPM imports
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field} from 'redux-form';
import {Image, Keyboard, StyleSheet, View} from 'react-native';

// VouD imports
import Button from '../../components/Button';
import BrandText from '../../components/BrandText';
import FacebookButton from '../../components/FacebookButton';
import MessageBox from '../../components/MessageBox';
import CPFField from '../../components/CPFField';
import {formatCpf, parseCpf} from '../../utils/parsers-formaters';
import FadeInView from '../../components/FadeInView';
import {preAuthClear} from '../../redux/login';
import {loginFBClear} from '../../redux/facebook';
import {cpfValidator, required} from '../../utils/validators';
import {clearReduxForm} from '../../utils/redux-form-util';
import {colors} from '../../styles';

const reduxFormName = 'preAuth';

// component
const voudLogoImg = require('../../images/logo.png');

class PreAuthForm extends Component {
  componentWillUnmount() {
    const {dispatch} = this.props;
    dispatch(preAuthClear());
    dispatch(loginFBClear());
    clearReduxForm(dispatch, reduxFormName);
  }

  _submit = () => {
    const {handleSubmit, onSubmit, ui, valid} = this.props;

    Keyboard.dismiss();

    if (valid && !ui.isFetching) handleSubmit(onSubmit)();
  };

  render() {
    const {style, ui, fbLoginUi, loginUi, onFBLogin} = this.props;

    return (
      <View style={style}>
        <Image source={voudLogoImg} style={styles.logo} />
        <FadeInView>
          <BrandText style={styles.text}>Informe seu CPF:</BrandText>
          <Field
            name="cpf"
            props={{
              textFieldRef: el => (this.CPFField = el),
              onSubmitEditing: () => {
                Keyboard.dismiss();
              },
            }}
            format={formatCpf}
            parse={parseCpf}
            component={CPFField}
            validate={[required, cpfValidator]}
          />
          {ui.error ? (
            <MessageBox message={ui.error} style={styles.errorMessage} />
          ) : null}
          <Button
            onPress={this._submit}
            style={styles.button}
            disabled={ui.isFetching}>
            Prosseguir
          </Button>
          <BrandText style={styles.orText}>ou</BrandText>
          <FacebookButton
            buttonText="Conectar com Facebook"
            onPress={onFBLogin}
          />
          {fbLoginUi.error || loginUi.error ? (
            <MessageBox
              message={fbLoginUi.error || loginUi.error}
              style={styles.errorMessage}
            />
          ) : null}
        </FadeInView>
      </View>
    );
  }
}

// styles
const styles = StyleSheet.create({
  logo: {
    alignSelf: 'center',
    marginTop: -8,
    marginBottom: 40,
    resizeMode: 'stretch',
  },
  text: {
    marginBottom: 8,
    fontSize: 14,
    lineHeight: 20,
    color: colors.GRAY_DARKER,
    textAlign: 'center',
  },
  errorMessage: {
    marginTop: 8,
  },
  errorMessageText: {
    color: colors.BRAND_ERROR,
  },
  button: {
    marginTop: 8,
  },
  orText: {
    marginVertical: 24,
    fontSize: 14,
    lineHeight: 20,
    color: colors.GRAY_DARKER,
    textAlign: 'center',
  },
});

// Redux
const mapStateToProps = state => {
  return {
    initialValues: {
      cpf: state.profile.data.cpf,
    },
  };
};

export default connect(mapStateToProps)(
  reduxForm({form: reduxFormName, destroyOnUnmount: false})(PreAuthForm),
);
