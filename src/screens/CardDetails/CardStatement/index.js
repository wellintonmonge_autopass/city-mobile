// NPM imports
import React     from 'react';
import PropTypes from 'prop-types';
import Moment    from 'moment';

import {
    Image,
    SectionList,
    View
} from 'react-native';

// VouD imports
import { colors } from '../../../styles';
import BrandText  from '../../../components/BrandText';
import TouchableText  from '../../../components/TouchableText';
import Loader  from '../../../components/Loader';
import { walletApplicationId } from '../../../redux/transport-card';

const errorImage = require('../../../images/error.png');
const emptyImage = require('../../../images/empty.png');

// Group imports
import CardStatementItem          from './Item';
import CardStatementSectionHeader from './SectionHeader';

const CardStatement = ({ cardData, itemList, onPress, style, statementUI, onReload, showEntryType }) => {
    
    const _itemListToSections = () => itemList.reduce((sections, item, i) => {
        const monthYearGroup = Moment(item.transactionDate).format('MMMM YYYY');
        const itemSection = sections.find(section => section.key === monthYearGroup);
        const itemObj = {
            ...item,
            key: i,
            isFirst: i === 0,
            isLast: i + 1 === itemList.length
        };

        if (itemSection) {
            return sections.map(section => {
                if (section.key === itemSection.key) {
                    return {
                        ...section,
                        data: [
                            ...section.data,
                            itemObj
                        ]
                    }
                }

                return section;
            })
        }

        return [
            ...sections,
            {
                key: monthYearGroup,
                title: monthYearGroup,
                date: item.transactionDate,
                isFirst: sections.length === 0,
                data: [itemObj]
            }
        ];
    }, []);

    const _getGratuityLimit = () => {
        const wallets = cardData && cardData.wallets ? cardData.wallets : null;
        const application = wallets ? wallets.find((wallet) => wallet.applicationId === walletApplicationId.CITY_PLUS_ESCOLAR_GRATUIDADE) : null;

        if (!application) return null;

        return application.gratuityLimit || null;
    };

    const _renderCardStatementRow = (gratuityLimit) => ({ item }) =>  {
        return (
            <CardStatementItem
                itemData={item}
                onPress={onPress}
                showEntryType={showEntryType}
                gratuityLimit={gratuityLimit}
            />
        );
    }

    const _renderHeader = () => {
        return (
            <View style={styles.titleContainer}>
                <BrandText style={styles.title}>Movimentações dos últimos 30 dias</BrandText>
                {
                    statementUI.isFetching &&
                    <View style={styles.loaderContainer}>
                        <Loader
                            iconSize={60}
                            text="Carregando..."
                            style={styles.loader}
                        />
                    </View>
                    || statementUI.error && statementUI.error !== '' &&
                    <View style={styles.stateMessage}>
                        <Image source={errorImage} style={styles.stateIcon} />
                        <View style={styles.errorStateTextContainer}>
                            <BrandText style={styles.errorStateText}>Ocorreu um erro na sua requisição</BrandText>
                            <TouchableText onPress={onReload} style={styles.errorStateButton} color={colors.BRAND_PRIMARY}>Tentar novamente</TouchableText>
                        </View>
                    </View>
                    || itemList.length === 0 &&
                    <View style={styles.stateMessage}>
                        <Image source={emptyImage} style={styles.stateIcon} />
                        <BrandText style={styles.emptyStateText}>Nenhum registro encontrado</BrandText>
                    </View>
                }                
            </View>
        );
    }

    const _renderSectionHeader = ({ section }) => {
        const sectionDate = Moment(section.date);
        const today = Moment();
        const isCurrentMonth = sectionDate.isSame(today, 'month') && sectionDate.isSame(today, 'year');

        if (section.isFirst && isCurrentMonth) return;

        return (
            <CardStatementSectionHeader
                isFirst={section.isFirst}
                title={section.title}
            />
        );
    };

    return (
        <View style={styles.mainContainer}>
            {_renderHeader()}
            <SectionList
                renderSectionHeader={_renderSectionHeader}
                renderItem={_renderCardStatementRow(_getGratuityLimit())}
                sections={_itemListToSections()}
                stickySectionHeadersEnabled={false}
            />
        </View>
    );
}

const styles = {
    mainContainer: {
        flex: 1,
    },
    titleContainer: {
        paddingVertical: 8,
        paddingHorizontal: 16,
    },
    title: {
        color: colors.BRAND_PRIMARY,
        fontSize: 14,
        fontWeight: 'bold',
        lineHeight: 16
    },
    stateMessage: {
        flexDirection: 'row',
        paddingVertical: 24
    },
    stateIcon: {
        height: 40,
        width: 40
    },
    errorStateTextContainer: {
        marginLeft: 16
    },
    errorStateText: {
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 24,
        color: colors.GRAY
    },
    errorStateButton: {
        minHeight: 24,
        alignItems: 'flex-start',
        paddingHorizontal: 0,
        paddingVertical: 0
    },
    emptyStateText: {
        margin: 8,
        marginHorizontal: 16,
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 24,
        color: colors.GRAY
    },
    loaderContainer: {
        marginTop: 24,
        alignSelf: 'stretch'
    },
    loader: {
        alignSelf: 'center',
        justifyContent: 'center'
    }
}

// prop types
CardStatement.propTypes = {
    itemList: PropTypes.array.isRequired,
    onPress: PropTypes.func.isRequired,
};

export default CardStatement;
