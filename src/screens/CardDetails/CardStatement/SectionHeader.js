import React          from 'react';
import { View, Text } from 'react-native';
import PropTypes      from 'prop-types';

// VouD imports
import { colors } from '../../../styles';

// Group imports
import CardStatementTimeline from './Timeline';

const CardStatementSectionHeader = ({ title, isFirst }) => {

    return (
        <View style={styles.mainContainer}>
            <CardStatementTimeline isTransparent={isFirst}/>
            <View style={styles.sectionHeaderContainer}>
                <Text style={styles.sectionHeader}>{title}</Text>
            </View>
        </View>
    );
}

const styles = {
    mainContainer: {
        flexDirection: 'row'
    },
    sectionHeaderContainer: {
        marginTop: 24,
        marginBottom: 8
    },
    sectionHeader: {
        color: colors.BRAND_PRIMARY,
        fontWeight: 'bold',
        fontSize: 14
    }
}

// prop types
CardStatementSectionHeader.propTypes = {
    title: PropTypes.string.isRequired,
};

export default CardStatementSectionHeader;