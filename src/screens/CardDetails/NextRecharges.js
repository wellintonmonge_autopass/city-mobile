// NPM imports
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import {
    View,
    ScrollView,
    StyleSheet
} from 'react-native';
import Moment from 'moment';

// VouD imports
import Header, { headerActionTypes } from '../../components/Header';
import BrandText from '../../components/BrandText';
import SystemText from '../../components/SystemText';
import InfoListItem from '../../components/InfoList/InfoListItem';
import KeyValueItem from '../../components/KeyValueItem';
import RequestFeedback from '../../components/RequestFeedback';
import { colors } from '../../styles';
import { formatCurrency, formatBomCardNumber } from '../../utils/parsers-formaters';

import { fetchNextRecharges, transportCardTypes } from '../../redux/transport-card';
import { getCurrentTransportCard, getNextRechargesHelpId } from '../../redux/selectors';
import { viewHelpDetails } from '../../redux/help';
import { routeNames } from '../../shared/route-names';
import { navigateToRoute } from '../../redux/nav';

// Screen component
class NextRechargesView extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this._fetchNextRecharges();
    }

    _fetchNextRecharges = () => {
        const { dispatch, cardData } = this.props;
        dispatch(fetchNextRecharges(cardData.uuid));
    }

    _getCardColor = () => {
        const { cardData } = this.props;

        if (cardData.layoutType && (cardData.layoutType === transportCardTypes.CITY_PLUS_VT)) {
            return colors.CARD_VT;
        }

        if (cardData.layoutType && (cardData.layoutType === transportCardTypes.CITY_PLUS_ESCOLAR || cardData.layoutType === transportCardTypes.CITY_PLUS_ESCOLAR_GRATUIDADE)) {
            return colors.BRAND_SECONDARY;
        }

        return colors.CARD_C;
    };

    _renderMainContent = () => {
        const { cardData } = this.props;
        const { nextRecharges } = cardData;
        
        return (
            <ScrollView style={styles.scrollView}>
                <View style={styles.cardInfoContainer}>
                    <BrandText style={styles.cardInfoLabel}>{cardData.nick}</BrandText>
                    <SystemText style={StyleSheet.flatten([styles.cardInfoValue, {borderColor: this._getCardColor()}])}>
                        {formatBomCardNumber(cardData.cardNumber)}
                    </SystemText>
                </View>
                <InfoListItem 
                    itemContent={'Próxima recarga'}
                    isHeader
                />
                <View style={styles.groupInfoContainer}>
                    <KeyValueItem
                        keyContent={'Valor'}
                        valueContent={'R$ ' + formatCurrency(cardData.balanceNextRecharge)}
                    />                  
                </View>
                <InfoListItem 
                    itemContent={'Detalhado'}
                    isHeader
                />
                <View style={styles.detailSubheader}>
                    <View style={styles.detailSubheaderLabels}>
                        <BrandText style={styles.subheaderInfoLabelL}>Data cadastro</BrandText>
                        <BrandText style={styles.subheaderInfoLabelR}>Valor</BrandText>
                    </View>
                </View>
                <View style={styles.groupInfoContainer}>
                    {
                        nextRecharges && nextRecharges.list && nextRecharges.list.map((item, i) => {
                            return (
                                <KeyValueItem
                                    key={`${i}`}
                                    keyContent={Moment(item.availableDate).format('DD/MM/YYYY')}
                                    valueContent={'R$ ' + formatCurrency(item.recharge)}
                                    useSysFontOnKey
                                />
                            );
                        })
                    }
                </View>
            </ScrollView>
        );
    }

    _viewHelp = () => {
        const { dispatch, nextRechargesHelpId } = this.props;
        dispatch(viewHelpDetails(nextRechargesHelpId));
        dispatch(navigateToRoute(routeNames.HELP_DETAILS));
    }

    render() {
        const { dispatch, cardData } = this.props;
        const { nextRecharges } = cardData;
        const isFetching = nextRecharges ? nextRecharges.isFetching : false;
        const error = nextRecharges ? nextRecharges.error : '';
        const nextRechargesList = nextRecharges ? nextRecharges.list : [];

        return (
            <View style={styles.mainContainer}>
                <Header
                    title="Próximas recargas"
                    left={{
                        type: headerActionTypes.CLOSE,
                        onPress: () => dispatch(NavigationActions.back())
                    }}
                    right={{
                        type: headerActionTypes.HELP,
                        icon: 'help-outline',
                        onPress: this._viewHelp
                    }}
                />                
                { !nextRecharges.isFetching && nextRechargesList && nextRechargesList[0] ?
                    this._renderMainContent() 
                    : 
                    <ScrollView style={styles.scrollView} contentContainerStyle={styles.requestFeedbackContainer}>
                        <RequestFeedback
                            loadingMessage="Carregando próximas recargas..."
                            errorMessage={error}
                            emptyMessage="Não foram encontradas próximas recargas"
                            retryMessage="Tentar novamente"
                            isFetching={isFetching}
                            onRetry={this._fetchNextRecharges}
                        />
                    </ScrollView>
                }
            </View>
        );
    }
}

// Styles
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    requestFeedbackContainer: {
        flexGrow: 1,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    scrollView: {
        flex: 1
    },
    cardInfoContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 16,
        marginTop: 8
    },
    cardInfoLabel: {
        flex: 1,
        marginRight: 8,
        fontSize: 16,
        lineHeight: 20,
        color: colors.GRAY_DARKER
    },
    cardInfoValue: {
        padding: 8,
        borderWidth: 1,
        borderRadius: 2,
        fontSize: 14,
        lineHeight: 16,
        fontWeight: '500',
        color: colors.GRAY_DARKER
    },
    detailSubheader: {
        padding: 12,
        paddingHorizontal: 16,
        backgroundColor: colors.GRAY_LIGHT2
    },
    cardTypeLabel: {
        fontSize: 14,
        fontWeight: 'bold',
        lineHeight: 20,
        color: colors.GRAY_DARKER
    },
    detailSubheaderLabels: {
        flexDirection: 'row'
    },
    subheaderInfoLabelL: {
        flex: 1,
        fontSize: 12,
        fontWeight: 'bold',
        lineHeight: 16,
        color: colors.GRAY
    },
    subheaderInfoLabelR: {
        fontSize: 12,
        fontWeight: 'bold',
        lineHeight: 16,
        color: colors.GRAY
    },
    groupInfoContainer: {
        paddingTop: 8,
        paddingBottom: 16
    },    
});

// redux connect and export
const mapStateToProps = state => {
    return {
        cardData: getCurrentTransportCard(state),
        nextRechargesHelpId: getNextRechargesHelpId(state)
    };
};

export const NextRecharges = connect(mapStateToProps)(NextRechargesView);
