// NPM imports
import React from 'react';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';

import {
  Animated,
  Image,
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';

// VouD imports
import Header, { headerActionTypes } from '../../components/Header';
import ScreenWithCardHeader from '../../components/ScreenWithCardHeader';
import TransportCard from '../../components/TransportCard';
import BrandText from '../../components/BrandText';
import Button from '../../components/Button';
import { hasNotch } from '../../utils/hasNotch';

import { fetchCardStatement, fetchNextRecharges, transportCardTypes } from '../../redux/transport-card';
import { setPurchaseTransportCard } from '../../redux/financial';
import { viewHelpDetails } from '../../redux/help';
import { routeNames } from '../../shared/route-names';
import { configErrorHandler } from '../../shared/config-error-handler';
import { GAEventParams, GATrackEvent } from '../../shared/analytics';
import { FBEventsConstants, FBLogEvent } from '../../shared/facebook';
import { colors, dimensions } from '../../styles';
import {
  getBomEscolarQuoteValueAvailable,
  cardHasSufficientQuota,
  insufficientQuotaErrorHandler,
} from '../../shared/insufficient-quota';
import {
  getCurrentTransportCard,
  getStatementUI,
  filterCurrentCardStatementBySupportedCharacteristics,
  getCardHelpId,
  getHasConfigError,
  getBOMCreditValueRange,
  getIssuerConfig,
} from '../../redux/selectors';

// Group imports
import CardStatement from './CardStatement';
import { navigateToRoute } from '../../redux/nav';
import { checkIssuerEnabled } from '../../utils/issuer-config';

// Screen component
const emptyImage = require('../../images/empty.png');

class CardDetailsView extends ScreenWithCardHeader {

  constructor(props) {
    super(props);
  }

  componentDidMount() {    
    this._loadNextRecharges();
    this._loadCardStatement();
  }

  _openNextRechargesView = () => {
    const { dispatch } = this.props;
    dispatch(navigateToRoute(routeNames.NEXT_RECHARGES));
  };

  _openSchoolCardDetailsView = () => {
    const { dispatch } = this.props;
    dispatch(navigateToRoute(routeNames.SCHOOL_CARD_DETAILS));
  };

  _loadNextRecharges = () => {
    const { dispatch, cardData } = this.props;
    dispatch(fetchNextRecharges(cardData.uuid));
  };

  _loadCardStatement = () => {
    const { dispatch, cardData } = this.props;
    dispatch(fetchCardStatement(cardData.uuid));
  };

  _viewHelp = () => {
    const { dispatch, cardHelpId } = this.props;
    dispatch(viewHelpDetails(cardHelpId));
    dispatch(navigateToRoute(routeNames.HELP_DETAILS));
  };

  _buyCredits = () => {
    const { dispatch, cardData, hasConfigError, bomCreditValueRange: { minCreditValue }, issuerConfig } = this.props;

    if (!cardData) return;
    
    if (hasConfigError) {
      configErrorHandler();
    }
    else if (!cardHasSufficientQuota(cardData, minCreditValue / 100)) {
      const quotaValue = getBomEscolarQuoteValueAvailable(cardData);
      insufficientQuotaErrorHandler(quotaValue, minCreditValue / 100);
    }
    else {

      if (checkIssuerEnabled(cardData.issuerType, issuerConfig)) {
        const { categories: { BUTTON }, actions: { CLICK }, labels: { BUY } } = GAEventParams;
  
        GATrackEvent(BUTTON, CLICK, BUY);
        FBLogEvent(FBEventsConstants.INITIATED_CHECKOUT);
        
        dispatch(setPurchaseTransportCard(cardData.uuid));
        dispatch(navigateToRoute(routeNames.BUY_CREDIT));
      }
    }
  };

  _hasNextRecharges = (cardData) => cardData.layoutType !== transportCardTypes.CITY_PLUS_ESCOLAR_GRATUIDADE && cardData.nextRecharges && cardData.nextRecharges.list && cardData.nextRecharges.list.length > 0;

  _isSchool = (cardData) => cardData.layoutType === transportCardTypes.CITY_PLUS_ESCOLAR || cardData.layoutType === transportCardTypes.CITY_PLUS_ESCOLAR_GRATUIDADE;

  _isRecharchable = (cardData) => cardData.layoutType !== transportCardTypes.CITY_PLUS_ESCOLAR_GRATUIDADE;

  _isVT = (cardData) => cardData.layoutType === transportCardTypes.CITY_PLUS_VT;

  _renderButtons = () => {
    const { cardData } = this.props;

    if (!cardData)
      return null;

    const hasNextRecharges = this._hasNextRecharges(cardData);
    const isSchool = this._isSchool(cardData);
    const isRecharchable = this._isRecharchable(cardData);

    const hasInfoButton = hasNextRecharges || isSchool;
    const hasAnyButton = hasInfoButton || isRecharchable;

    if (hasAnyButton)
      return (
        <View style={styles.buttonsContainer}>
          {
            hasInfoButton &&
            <View style={styles.infoButtonContainer}>
              {
                hasNextRecharges &&
                <Button
                  style={styles.infoButton}
                  sm gray icon='validate-bom-card'
                  onPress={this._openNextRechargesView}
                >
                  Próximas recargas
                </Button>
              }
              {
                isSchool &&
                <Button
                  style={StyleSheet.flatten([styles.infoButton, hasNextRecharges ? { marginLeft: 8 } : {}])}
                  sm gray icon='school'
                  onPress={this._openSchoolCardDetailsView}
                >
                  Detalhe escolar
                </Button>
              }
            </View>
          }
          {
            isRecharchable &&
            <Button
              style={hasInfoButton ? { marginTop: 8 } : {}}
              sm
              onPress={this._buyCredits}
            >
              Comprar créditos
            </Button>
          }
        </View>
      );

    return null;
  };

  render() {
    const { dispatch, cardData } = this.props;

    return (
      <View style={styles.mainContainer}>
        <Header
          title="Detalhes do cartão"
          left={{
            type: headerActionTypes.BACK,
            onPress: () => dispatch(NavigationActions.back())
          }}
          right={{
            type: headerActionTypes.EDIT,
            onPress: () => dispatch(navigateToRoute(routeNames.EDIT_CARD))
          }}
          renderExtension={this._renderExtension}
        />
        <TransportCard
          data={cardData}
          collapse={this._collapse}
          style={StyleSheet.flatten([styles.card, { marginTop: this._cardMargin }])}
          onHelp={this._viewHelp}
        />
        <ScrollView
          scrollEventThrottle={16}
          onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this._scrollY } } }])}
          style={styles.scrollView}
          contentContainerStyle={styles.list}
        >
          {this._renderButtons()}
          
          <CardStatement
            cardData={this.props.cardData}
            itemList={this.props.cardStatement}
            statementUI={this.props.statementUi}
            onReload={this._loadCardStatement}
            onPress={() => { }}
            showEntryType={cardData && this._isVT(cardData)}
          />
        </ScrollView>
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingBottom: hasNotch() ? dimensions.notchSpace.top : 0,
    backgroundColor: 'white'
  },
  card: {
    zIndex: 20,
    marginHorizontal: 16
  },
  scrollView: {
    flex: 1
  },
  buttonsContainer: {
    alignItems: 'stretch',
    padding: 16
  },
  infoButtonContainer: {
    flexDirection: 'row'
  },
  infoButton: {
    flex: 1
  },
  list: {},
  firstButton: {
    flex: 1,
    marginRight: 16
  },
  secondButton: {
    flex: 1
  },
  singleButton: {
    margin: 16,
    marginTop: 0
  },
  buStatementMessage: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
  },
  emptyIcon: {
    height: 40,
    width: 40,
    marginRight: 16,
  },
  buStatementText: {
    flex: 1,
    fontSize: 14,
    lineHeight: 24,
    color: colors.GRAY,
  },
});

// redux connect and export
const mapStateToProps = state => {
  return {
    hasConfigError: getHasConfigError(state),
    cardData: getCurrentTransportCard(state),
    bomCreditValueRange: getBOMCreditValueRange(state),
    cardStatement: filterCurrentCardStatementBySupportedCharacteristics(state),
    statementUi: getStatementUI(state),
    cardHelpId: getCardHelpId(state),
    issuerConfig: getIssuerConfig(state)
  };
};

export const CardDetails = connect(mapStateToProps)(CardDetailsView);

export * from './EditCard';
export * from './NextRecharges';
export * from './SchoolCardDetails';
