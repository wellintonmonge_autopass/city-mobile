// NPM imports
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Image, StyleSheet, View} from 'react-native';

// VouD imports
import BrandText from '../../components/BrandText';
import SelectionButton from '../../components/SelectionButton';
import FadeInView from '../../components/FadeInView';
import Header, {headerActionTypes} from '../../components/Header';
import {colors, dimensions} from '../../styles';
import {setCurrentView, servicePointViews} from '../../redux/service-point';
import {openMenu} from '../../redux/menu';
import {hasNotch} from '../../utils/hasNotch';

// Group imports
import ServicePointsCityPlus from './ServicePointsCityPlus';

// Images
const cityPlusSmImg = require('../../images/transport-cards/city-plus-sm.png');

// Screen component
class ServicePointsView extends Component {
  _openMenu = () => {
    const {dispatch} = this.props;
    dispatch(openMenu());
  };

  _setCurrentView = view => {
    const {dispatch} = this.props;
    dispatch(setCurrentView(view));
  };

  _renderTab = (text, value, image, isLast = false) => {
    const {currentView} = this.props;
    const isSelected = currentView === value;
    const tabStyle = StyleSheet.flatten([styles.tab, isLast ? styles.mr0 : {}]);
    const imageStyle = StyleSheet.flatten([
      styles.tabIcon,
      isSelected ? styles.tabIconActive : {},
    ]);
    const textStyle = StyleSheet.flatten([
      styles.tabText,
      isSelected ? styles.tabTextActive : {},
    ]);

    return (
      <SelectionButton
        onPress={() => {
          this._setCurrentView(value);
        }}
        selected={isSelected}
        style={tabStyle}>
        <View style={styles.tabContent}>
          <Image source={image} style={imageStyle} />
          <BrandText style={textStyle}>{text}</BrandText>
        </View>
      </SelectionButton>
    );
  };

  _renderTabs = () => {
    return (
      <View style={styles.tabContainer}>
        {this._renderTab(
          'Cartão City +',
          servicePointViews.CITY_PLUS,
          cityPlusSmImg,
          true,
        )}
      </View>
    );
  };

  _renderContent = () => {
    return (
      <FadeInView key={servicePointViews.CITY_PLUS} style={styles.content}>
        <ServicePointsCityPlus />
      </FadeInView>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          title="Pontos de recarga"
          left={{
            type: headerActionTypes.MENU,
            onPress: this._openMenu,
          }}
        />
        {/* {this._renderTabs()} */}
        {this._renderContent()}
      </View>
    );
  }
}

// Styles

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  initialSelectContainer: {
    paddingVertical: 24,
    paddingHorizontal: 16,
  },
  labelText: {
    fontSize: 14,
    lineHeight: 20,
    color: colors.GRAY,
  },
  initialSelectTabsContainer: {
    flexDirection: 'row',
    marginTop: 16,
  },
  initialSelectTab: {
    flex: 1,
    height: 72,
    marginRight: 16,
  },
  buImg: {
    width: 48,
    height: 48,
  },
  bomImg: {
    width: 72,
    height: 32,
  },
  tabContainer: {
    flexDirection: 'row',
    padding: 16,
    borderBottomWidth: 1,
    borderColor: colors.GRAY_LIGHTER,
  },
  tab: {
    flex: 1,
    marginRight: 8,
  },
  tabContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tabText: {
    color: colors.GRAY_LIGHT,
    fontSize: 14,
    lineHeight: 20,
  },
  tabTextActive: {
    color: colors.BRAND_PRIMARY_LIGHTER,
  },
  tabIcon: {
    width: 24,
    height: 24,
    marginRight: 8,
    opacity: 0.3,
  },
  tabIconActive: {
    opacity: 1,
  },
  mr0: {
    marginRight: 0,
  },
  content: {
    paddingBottom: hasNotch() ? dimensions.notchSpace.top : 0,
    flex: 1,
  },
});

// Redux

const mapStateToProps = state => ({});

export const ServicePoints = connect(mapStateToProps)(ServicePointsView);

// Export other screens from group

export * from './ServicePointDetails';
