// NPM imports
import React, { Component } from 'react';
import { FlatList } from 'react-native';
import PropTypes from 'prop-types';

// VouD imports
import ServicePointsListItem from './Item';

class ServicePointsList extends Component {

  _renderServicePointItem = ({ item }) => {
    const { onPress } = this.props;

    return (
      <ServicePointsListItem
        itemData={item}
        onPress={onPress}
      />
    );
  };

  _matchSearchTerm = (item, searchTerm) =>
    item.name.toLowerCase().indexOf(searchTerm) !== -1 ||
    item.address.main.toLowerCase().indexOf(searchTerm) !== -1 ||
    item.address.number.toLowerCase().indexOf(searchTerm) !== -1;

  _filterItemList = () => {
    const { itemList } = this.props;
    const searchTerm = searchTerm ? searchTerm : '';

    return searchTerm === '' ? itemList : 
      itemList.filter((item) => this._matchSearchTerm(item, searchTerm.toLowerCase()));
  }

  render() {
    return (
      <FlatList
        contentContainerStyle={styles.mainContainer}
        data={this._filterItemList()}
        renderItem={this._renderServicePointItem}
        keyExtractor={(item) => item.id}
      />
    );
  }
}

const styles = {
  mainContainer: {
    paddingVertical: 8
  }
}

// prop types
ServicePointsList.propTypes = {
  itemList: PropTypes.array.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default ServicePointsList;
