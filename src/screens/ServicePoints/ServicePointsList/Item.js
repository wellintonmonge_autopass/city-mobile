import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

// VouD imports
import TouchableNative from '../../../components/TouchableNative';
import BrandText from '../../../components/BrandText';
import SystemText from '../../../components/SystemText';
import Icon from '../../../components/Icon';
import { colors } from '../../../styles';

// Component
const ServicePointsListItem = ({ itemData, onPress }) => {

  const renderIcon = () => {
    let Component, iconName;

    switch (itemData.type) {
      case 'STORE':
        Component = Icon;
        iconName = 'store';
        break;
      case 'KIOSK':
        Component = Icon;
        iconName = 'kiosk';
        break;
      case 'POUPATEMPO':
        Component = Icon;
        iconName = 'poupa-tempo';
        break;
      case 'VALIDATOR':
      default:
        Component = Icon;
        iconName = 'validator';
        break;
    }

    return (
      <Component
        name={iconName}
        style={styles.icon}
      />
    );
  };

  const renderDistance = () => {
    if (itemData.distance)
      return (
        <View style={styles.pointDistanceContainer}>
          <SystemText style={styles.pointDistance}>{itemData.distance} km</SystemText>
        </View>
      );

    return null;
  };

  return (
    <TouchableNative
      style={styles.mainContainer}
      key={itemData.id}
      onPress={() => onPress(itemData.id, itemData.name)}
    >
      <View style={styles.iconCircle}>
        {renderIcon()}
      </View>
      <View style={styles.nameAddressContainer}>
        <BrandText style={styles.pointName} numberOfLines={1} ellipsizeMode={'tail'}>{itemData.name}</BrandText>
        <BrandText style={styles.pointAddress} numberOfLines={1} ellipsizeMode={'tail'}>{itemData.shortAddress}</BrandText>
      </View>
      {renderDistance()}
    </TouchableNative>
  );
}

// prop types
ServicePointsListItem.propTypes = {
  itemData: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
};

// Styles
const styles = {
  mainContainer: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'stretch'
  },
  iconCircle: {
    height: 40,
    width: 40,
    borderRadius: 20,
    backgroundColor: colors.BRAND_PRIMARY,
    marginRight: 16,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: {
    fontSize: 24,
    color: colors.BRAND_SECONDARY,
  },
  nameAddressContainer: {
    flex: 1,
    flexDirection: 'column'
  },
  pointName: {
    color: colors.GRAY_DARKER,
    fontSize: 16,
    lineHeight: 20,
    marginBottom: 4
  },
  pointAddress: {
    fontSize: 14,
    color: colors.GRAY,
    lineHeight: 16
  },
  pointDistanceContainer: {
    marginLeft: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  pointDistance: {
    color: colors.BRAND_PRIMARY,
    fontSize: 16,
    fontWeight: 'bold',
    lineHeight: 20
  }
}

export default ServicePointsListItem;