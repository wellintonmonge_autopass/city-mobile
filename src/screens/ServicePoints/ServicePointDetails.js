// NPM imports
import React, {Component} from 'react';
import {NavigationActions} from 'react-navigation';
import {connect} from 'react-redux';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';

import {
  View,
  ScrollView,
  StyleSheet,
  Linking,
  Platform,
  PixelRatio,
} from 'react-native';

// VouD imports
import Header, {headerActionTypes} from '../../components/Header';
import Button from '../../components/Button';
import Loader from '../../components/Loader';
import InfoList from '../../components/InfoList';
import BrandText from '../../components/BrandText';
import SystemText from '../../components/SystemText';
import Icon from '../../components/Icon';
import {colors} from '../../styles';
import {getServicePointDetails} from '../../redux/selectors';
import {GAEventParams, GATrackEvent} from '../../shared/analytics';
import mapStyle from '../../google-maps/map-style';
import {
  getDefaultRequestPositionConfig,
  getCurrentPosition,
} from '../../utils/geolocation';
import {setPosition} from '../../redux/profile';
import UserMarker from '../../google-maps/UserMarker';
import StopMarker from '../../google-maps/StopMarker';
import {pointsOfInterestTypes} from '../../flows/bus-lines/screens/details';

// Screen component
class ServicePointDetailsView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shouldRenderMap: false,
    };

    this._mapRef = null;
    this._stopMarkerRef = null;
  }

  componentDidMount() {
    // wait 1s before render map
    // this.delay = setTimeout(() => {

    // }, 1000);

    this._getUserLocation(
      getDefaultRequestPositionConfig(true),
      true,
      getDefaultRequestPositionConfig(false),
      () => {
        this.setState({
          shouldRenderMap: true,
        });
      },
    );
  }

  componentWillUnmount() {
    // clearTimeout(this.delay);
  }

  _getUserLocation = (config, shouldRetry = false, retryConfig, callback) => {
    const {dispatch} = this.props;

    getCurrentPosition(config, shouldRetry, retryConfig)
      .then(({latitude, longitude}) => {
        dispatch(setPosition(latitude, longitude));
        callback();
      })
      .catch(() => {});
  };

  _fitToCoordinates = (pos, otherPos) => {
    let edgePadding = {
      top: 125,
      bottom: 20,
      left: 75,
      right: 75,
    };

    if (Platform.OS === 'android') {
      edgePadding = {
        top: PixelRatio.getPixelSizeForLayoutSize(edgePadding.top),
        right: PixelRatio.getPixelSizeForLayoutSize(edgePadding.right),
        left: PixelRatio.getPixelSizeForLayoutSize(edgePadding.left),
        bottom: PixelRatio.getPixelSizeForLayoutSize(edgePadding.bottom),
      };
    }

    if (this._mapRef) {
      this._mapRef.fitToCoordinates([pos, otherPos], {
        edgePadding,
        animated: true,
      });
    }
  };

  _centerLocation = () => {
    const {userPosition, servicePoint} = this.props;

    this._fitToCoordinates(userPosition, servicePoint);
  };

  _back = () => {
    this.props.dispatch(NavigationActions.back());
  };

  _openRouteOnMaps = () => {
    const {servicePoint} = this.props;
    const queryUrl = `https://www.google.com/maps/dir/?api=1&destination=${encodeURIComponent(
      servicePoint.latitude,
    )},${encodeURIComponent(servicePoint.longitude)}`;

    Linking.canOpenURL(queryUrl)
      .then(supported => {
        if (!supported) {
          if (__DEV__) console.tron.log("Can't handle url: " + queryUrl, true);
        } else {
          const {
            categories: {BUTTON},
            actions: {CLICK},
            labels: {SERVICE_POINT_ROUTE},
          } = GAEventParams;
          GATrackEvent(
            BUTTON,
            CLICK,
            `${SERVICE_POINT_ROUTE} ${servicePoint.name}`,
          );
          return Linking.openURL(queryUrl);
        }
      })
      .catch(err => {
        if (__DEV__)
          console.tron.log('An error occurred: ' + err.message, true);
      });
  };

  _renderInfo = () => {
    if (
      this.props.servicePoint.info &&
      Array.isArray(this.props.servicePoint.info)
    ) {
      return this.props.servicePoint.info.map(infoGroup => {
        if (infoGroup.info)
          return (
            <InfoList
              listTitle={infoGroup.header}
              itemList={infoGroup.info}
              renderItem={item => item}
              dense={true}
              key={infoGroup.id}
            />
          );

        return null;
      });
    }

    return null;
  };

  _onMapReady = () => {
    this._centerLocation();
  };

  _renderUserMarker = () => {
    return <UserMarker coordinate={this.props.userPosition} />;
  };

  render() {
    const {servicePoint, userPosition} = this.props;

    return (
      <View style={styles.mainContainer}>
        <Header
          title={servicePoint.name}
          left={{
            type: headerActionTypes.CLOSE,
            onPress: this._back,
          }}
        />
        <ScrollView>
          {this.state.shouldRenderMap ? (
            <MapView
              style={styles.mapView}
              ref={ref => {
                this._mapRef = ref;
              }}
              provider={PROVIDER_GOOGLE}
              minZoomLevel={9}
              maxZoomLevel={18}
              customMapStyle={mapStyle}
              moveOnMarkerPress={false}
              // onMapReady={this._onMapReady}
              onRegionChangeComplete={() => {
                if (this._stopMarkerRef) {
                  this._stopMarkerRef.showCallout();
                }
              }}
              initialRegion={{
                latitude: servicePoint.latitude,
                longitude: servicePoint.longitude,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
              }}>
              {this._renderUserMarker()}
              <StopMarker
                markerRef={ref => {
                  this._stopMarkerRef = ref;
                }}
                coordinate={servicePoint}
                userPosition={userPosition}
                type={pointsOfInterestTypes.RECHARGE}
                pointName={servicePoint.name || ''}
                pointAddress={servicePoint.shortAddress}
              />
            </MapView>
          ) : (
            <View style={styles.mapView}>
              <Loader text="Carregando mapa..." />
            </View>
          )}
          <View style={styles.main}>
            {servicePoint.name ? (
              <BrandText style={styles.name}>{servicePoint.name}</BrandText>
            ) : (
              <BrandText style={styles.namme}>Endereço não informado</BrandText>
            )}
            {servicePoint.address ? (
              <BrandText style={styles.addressStreet}>
                {servicePoint.address.main}, {servicePoint.address.number}
              </BrandText>
            ) : (
              <BrandText style={styles.addressStreet}>
                Endereço não informado
              </BrandText>
            )}
            {servicePoint.distance && (
              <View style={styles.distanceInfo}>
                <Icon name="distance" style={styles.distanceInfoIcon} />
                <SystemText style={styles.distanceInfoIconText}>
                  Aprox. {servicePoint.distance} km de distância
                </SystemText>
              </View>
            )}
          </View>
          <View style={styles.addressContainer}>
            {servicePoint.address && (
              <BrandText style={styles.addressCityNeighborhood}>
                {servicePoint.address.district} - {servicePoint.address.city}
              </BrandText>
            )}
            {servicePoint.address && servicePoint.address.reference && (
              <BrandText style={styles.addressReference}>
                Referência: {servicePoint.address.reference}
              </BrandText>
            )}
          </View>
          {this._renderInfo()}
        </ScrollView>
        <View style={styles.traceRouteContainer}>
          <Button
            onPress={this._openRouteOnMaps}
            icon="md-map"
            outline
            outlineText="white"
            style={styles.traceRouteBtn}>
            Traçar rota
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  mapView: {
    justifyContent: 'center',
    height: 315,
    backgroundColor: colors.GRAY_LIGHTER,
  },
  main: {
    paddingHorizontal: 16,
    paddingVertical: 24,
    borderBottomWidth: 1,
    borderColor: colors.GRAY_LIGHTER,
  },
  name: {
    color: colors.GRAY_DARKER,
    fontSize: 16,
    fontWeight: 'bold',
    lineHeight: 20,
    marginBottom: 12,
  },
  addressStreet: {
    color: colors.GRAY,
    fontSize: 14,
    lineHeight: 16,
  },
  distanceInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
  },
  distanceInfoIcon: {
    marginRight: 8,
    fontSize: 24,
    color: colors.BRAND_SECONDARY,
  },
  distanceInfoIconText: {
    fontSize: 14,
    color: colors.BRAND_PRIMARY,
  },
  addressContainer: {
    paddingHorizontal: 16,
    paddingVertical: 24,
  },
  addressCityNeighborhood: {
    color: colors.GRAY_DARKER,
    fontSize: 16,
    lineHeight: 20,
    marginBottom: 8,
  },
  addressReference: {
    fontSize: 14,
    lineHeight: 16,
    color: colors.GRAY,
  },
  traceRouteContainer: {
    backgroundColor: colors.BRAND_PRIMARY,
  },
  traceRouteBtn: {
    margin: 16,
  },
});

// Redux

const mapStateToProps = state => {
  return {
    servicePoint: getServicePointDetails(state),
    userPosition: {
      latitude:
        state.profile.position.latitude || state.profile.position.defaultLat,
      longitude:
        state.profile.position.longitude || state.profile.position.defaultLng,
    },
  };
};

export const ServicePointDetails = connect(mapStateToProps)(
  ServicePointDetailsView,
);
