// NPM imports
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {StyleSheet, View} from 'react-native';

// VouD imports
import RequestFeedback from '../../components/RequestFeedback';
import BrandText from '../../components/BrandText';
import FadeInView from '../../components/FadeInView';
import Progress from '../../components/Progress';
import {colors} from '../../styles';
import {fetchServicePoints, viewDetails} from '../../redux/service-point';
import {openMenu} from '../../redux/menu';
import {setPosition} from '../../redux/profile';
import {navigateToRoute} from '../../redux/nav';
import {
  getServicePointsList,
  getServicePointsListUI,
} from '../../redux/selectors';
import {routeNames} from '../../shared/route-names';
import {GAEventParams, GATrackEvent} from '../../shared/analytics';

// Group imports
import ServicePointsList from './ServicePointsList';
import {
  getDefaultRequestPositionConfig,
  getCurrentPosition,
} from '../../utils/geolocation';

// Screen component
class ServicePointsCityPlus extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isGettingLocation: false,
    };
  }

  componentDidMount() {
    const {requestUi} = this.props;

    // wait 1 second to get user position, avoiding
    // alert over half closed menu
    this.setState({isGettingLocation: true});

    this._delay = setTimeout(() => {
      // try get user location with high accuracy,
      // falls back to low accuracy if error
      this._getUserLocation(
        getDefaultRequestPositionConfig(true),
        true,
        getDefaultRequestPositionConfig(false),
      );
    }, 1000);

    this._fetchServicePoints();
  }

  componentWillUnmount() {
    clearTimeout(this._delay);
  }

  _getUserLocation = (config, shouldRetry = false, retryConfig) => {
    const {dispatch} = this.props;

    getCurrentPosition(config, shouldRetry, retryConfig)
      .then(({latitude, longitude}) => {
        this.setState({isGettingLocation: false});
        dispatch(setPosition(latitude, longitude));
      })
      .catch(error => {
        this.setState({isGettingLocation: false});
      });
  };

  _fetchServicePoints = () => {
    const {dispatch, position} = this.props;
    dispatch(fetchServicePoints(position));
  };

  _openMenu = () => {
    const {dispatch} = this.props;
    dispatch(openMenu());
  };

  _viewDetails = (id, name) => {
    const {dispatch} = this.props;
    const {
      categories: {BUTTON},
      actions: {CLICK},
      labels: {SERVICE_POINT},
    } = GAEventParams;

    GATrackEvent(BUTTON, CLICK, `${SERVICE_POINT} ${name}`);
    dispatch(viewDetails(id));
    dispatch(navigateToRoute(routeNames.SERVICE_POINTS_DETAILS));
  };

  _renderContent = () => {
    const {servicePoints} = this.props;
    const {error, isFetching} = this.props.requestUi;

    return servicePoints[0] ? (
      <ServicePointsList itemList={servicePoints} onPress={this._viewDetails} />
    ) : (
      <View style={styles.requestFeedbackContainer}>
        <RequestFeedback
          loadingMessage="Carregando pontos de recarga..."
          errorMessage={error}
          emptyMessage="Não foram encontrados pontos de recarga"
          retryMessage="Tentar novamente"
          isFetching={isFetching}
          onRetry={this._fetchServicePoints}
        />
      </View>
    );
  };

  render() {
    const {error, isFetching} = this.props.requestUi;

    return (
      <View style={styles.container}>
        {this._renderContent()}
        {this.state.isGettingLocation && !error && !isFetching && (
          <FadeInView style={styles.searchingLocation}>
            <BrandText style={styles.searchingLocationText}>
              Aguarde enquanto ordenamos a lista de pontos de recarga por
              proximidade.
            </BrandText>
            <Progress withHeight />
          </FadeInView>
        )}
      </View>
    );
  }
}

// Styles

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  requestFeedbackContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  searchingLocation: {
    position: 'absolute',
    bottom: 8,
    left: 8,
    right: 8,
    padding: 8,
    borderRadius: 2,
    backgroundColor: colors.GRAY_LIGHTER,
  },
  searchingLocationText: {
    marginBottom: 8,
    fontSize: 14,
    color: colors.GRAY,
    textAlign: 'center',
  },
});

// Redux

const mapStateToProps = state => {
  return {
    servicePoints: getServicePointsList(state),
    requestUi: getServicePointsListUI(state),
    isMenuOpen: state.menu.isOpen,
    position: state.profile.position,
  };
};

export default connect(mapStateToProps)(ServicePointsCityPlus);
