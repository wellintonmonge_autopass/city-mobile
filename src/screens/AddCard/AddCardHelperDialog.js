// NPM imports
import React, { Component } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import { formValueSelector } from 'redux-form';

// VouD imports
import BrandText from '../../components/BrandText';
import TouchableText from '../../components/TouchableText';
import Dialog from '../../components/Dialog';
import { colors } from '../../styles/constants';
import { transportCardTypeValues } from './TransportCardTypeSelectField';

// Screen component
const cityPlusHelperImg = require('../../images/transport-cards/city-plus-number-helper.png');
const cityPlusHelperPicImg = require('../../images/transport-cards/city-plus-number-helper-pic.png');
const cityPlusHelperPlusImg = require('../../images/transport-cards/city-plus-number-helper-plus.png');

const propTypes = {};

class AddCardHelperDialogView extends Component {

  _dismiss = () => {
    const { dispatch } = this.props;
    dispatch(NavigationActions.back());
  };

  _renderContent = () => {
    return (
      <View style={styles.content}>
        <BrandText style={styles.title}>
          Número do Cartão City +
        </BrandText>
        <BrandText style={styles.text}>
          O número do Cartão City + começa com 13 e encontra-se em um dos locais indicados abaixo:
        </BrandText>
        <View style={styles.cardImages}>
          <Image
            source={cityPlusHelperImg}
          />
          <Image
            source={cityPlusHelperPlusImg}
            style={styles.ml16}
          />
        </View>
        <BrandText style={styles.sub}>
          Modelos sem foto
        </BrandText>
        <View style={styles.cardImages}>
          <Image
            source={cityPlusHelperPicImg}
          />
        </View>
        <BrandText style={styles.sub}>
          Modelos com foto
        </BrandText>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <Dialog
          onDismiss={this._dismiss}
          noPadding
        >
          {this._renderContent()}
          <View style={styles.actions}>
            <TouchableText
              onPress={this._dismiss}
              color={colors.GRAY}
            >
              Fechar
            </TouchableText>
          </View>
        </Dialog>
      </View>
    );
  }
}

AddCardHelperDialogView.propTypes = propTypes;

// Styles
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  content: {
    paddingHorizontal: 16,
    paddingVertical: 24,
  },
  title: {
    marginBottom: 24,
    fontSize: 14,
    lineHeight: 20,
    color: colors.BRAND_PRIMARY,
  },
  text: {
    fontSize: 14,
    lineHeight: 20,
    color: colors.GRAY,
  },
  sub: {
    marginTop: 8,
    fontSize: 12,
    lineHeight: 16,
    textAlign: 'center',
    color: colors.GRAY,
  },
  cardImages: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 24,
  },
  ml16: {
    marginLeft: 16,
  },
  mt32: {
    marginTop: 32,
  },
  actions: {
    paddingVertical: 24,
    paddingHorizontal: 16,
    borderTopWidth: 1,
    borderColor: colors.GRAY_LIGHTER,
  },
});

const mapStateToProps = state => ({
  cardType: formValueSelector('addCard')(state, 'cardType'),
});

export const AddCardHelperDialog = connect(mapStateToProps)(AddCardHelperDialogView);
