// NPM imports
import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  View
} from 'react-native';

// VouD imports
import BrandText from '../../components/BrandText';
import { colors } from '../../styles';
import { getStatusBarHeight } from '../../styles/util';

// images TODO: remove static images
const onboardingImg = require('../../images/logo.png');
const onboardingMobilityServices = require('../../images/onboarding-mobility-services.png');
const bgImg = require('../../images/bg.png');

const images = {
  voud: onboardingImg,
  mobilityServices: onboardingMobilityServices,
  // phoneRecharge: onboardingPhoneRecharge
};

class OnboardingSlide extends Component {
  render() {
    const { content, image } = this.props;

    return (
      <View style={styles.container}>
        <Image
          source={bgImg}
          style={StyleSheet.flatten([styles.brandBg])}
          resizeMode="stretch"
        />
        <View style={styles.wrapper}>
          <Image
            source={images[image]}
            style={styles.image}
            resizeMode="contain"
          />
          <BrandText style={styles.text}>
            {content}
          </BrandText>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  wrapper: {
    paddingHorizontal: 40,
    paddingTop: getStatusBarHeight() + 56,
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-start',
  },
  image: {
    height: 240,
    marginBottom: 32
  },
  text: {
    alignSelf: 'stretch',
    fontSize: 16,
    lineHeight: 24,
    textAlign: 'center',
    color: colors.BRAND_PRIMARY,
  },
  brandBg: {
    width: '100%',
    position: 'absolute',
    top: 0,
    transform: [
      {
        rotate: '180deg',
      }
    ],
  },
});

export default OnboardingSlide;
