import React, {Component} from 'react';
import {View, StyleSheet, Image, Linking, Platform} from 'react-native';
import {connect} from 'react-redux';

import {colors} from '../../styles';
import TouchableNative from '../../components/TouchableNative';
import {navigateToRoute} from '../../redux/nav';
import BrandText from '../../components/BrandText';
import {routeNames} from '../../shared/route-names';

const ultraLogoImg = require('../../images/ultra-logo.png');
const saudeLogoImg = require('../../images/saude-controle-logo.png');
const veevasaudeLogoImg = require('../../images/veeva-saude.png');

const saudeControleUrl = 'https://veevasaude.com.br/';
const saudeControleDeeplink =
  'https://play.google.com/store/apps/details?id=br.com.trampo.saudecontrole';
const viacaoUltraUrl = 'https://www.viacaoultra.com.br';
const viacaoUltraDeeplink =
  'https://play.google.com/store/apps/details?id=br.com.rjconsultores.Ultra';

class Banner extends Component {
  constructor(props) {
    super(props);
  }

  _goToSaudeControle = async () => {
    if (Platform.OS === 'android') {
      const canOpen = await Linking.canOpenURL(saudeControleDeeplink);

      if (canOpen) {
        Linking.openURL(saudeControleDeeplink);
      } else {
        Linking.openURL(saudeControleUrl);
      }
    } else {
      Linking.openURL(saudeControleUrl);
    }
  };

  _goToViacaoUltra = async () => {
    if (Platform.OS === 'android') {
      const canOpen = await Linking.canOpenURL(viacaoUltraDeeplink);

      if (canOpen) {
        Linking.openURL(viacaoUltraDeeplink);
      } else {
        Linking.openURL(viacaoUltraUrl);
      }
    } else {
      Linking.openURL(viacaoUltraUrl);
    }
  };

  render() {
    return (
      <View style={styles.bannerContainer}>
        <TouchableNative
          style={StyleSheet.flatten([styles.bannerButton, styles.mr8])}
          onPress={this._goToSaudeControle}>
          <Image
            style={styles.saudeLogoImg}
            source={veevasaudeLogoImg}
            resizeMode="contain"
          />
          <BrandText style={styles.bannerButtonText}>
            {'Conheça o\nVeeva Saúde'}
          </BrandText>
        </TouchableNative>
        <TouchableNative
          style={StyleSheet.flatten([styles.bannerButton, styles.ml8])}
          onPress={this._goToViacaoUltra}>
          <Image
            style={styles.ultraLogoImg}
            source={ultraLogoImg}
            resizeMode="contain"
          />
          <BrandText style={styles.bannerButtonText}>
            {'Viaje com\nViação Ultra'}
          </BrandText>
        </TouchableNative>
      </View>
    );
  }
}

// component styles
const styles = {
  bannerContainer: {
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingBottom: 16,
  },
  bannerButton: {
    height: 84,
    padding: 8,
    flex: 1,
    borderWidth: 1,
    borderColor: colors.BRAND_PRIMARY,
    borderRadius: 4,
    backgroundColor: 'rgba(255, 255, 255, 0.75);',
  },
  mr8: {
    marginRight: 8,
  },
  ml8: {
    marginLeft: 8,
  },
  bannerTextContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  bannerButtonText: {
    color: colors.BRAND_PRIMARY,
    fontSize: 12,
    fontWeight: 'bold',
  },
  bannerButtonIcon: {
    fontSize: 24,
    color: colors.BRAND_SECONDARY,
    marginBottom: 8,
  },
  saudeLogoImg: {
    height: 40,
    width: 80,
    paddingHorizontal: 6,
    alignSelf: 'flex-end',
  },
  ultraLogoImg: {
    height: 40,
    width: 96,
    marginTop: -2,
    marginRight: -6,
    alignSelf: 'flex-end',
  },
};

export default connect()(Banner);
