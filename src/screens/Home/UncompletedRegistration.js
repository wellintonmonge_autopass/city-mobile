// NPM imports
import React, { Component } from 'react';
import { StyleSheet, Image, View } from 'react-native';

// VouD Imports
import Button from '../../components/Button';

// Images
const uncompletedRegistration = require('../../images/uncompleted-registration.png');
const CARDS_PLACEHOLDER_RATIO = 0.99425287356;

class UncompletedRegistration extends Component {

  constructor(props) {
    super(props);

    this.state = {
      marginTopOffset: 0,
    }
  }

  render() {
    const { onPress } = this.props;

    return (
      <View style={styles.container}>
        <View>
          <Image
            style={StyleSheet.flatten([styles.image, { marginTop: -this.state.marginTopOffset }])}
            source={uncompletedRegistration}
            resizeMode="contain"
            onLayout={(event) => {
              const { width, height } = event.nativeEvent.layout;
              const realHeight = width / CARDS_PLACEHOLDER_RATIO;
              const diffHeight = height - realHeight;

              this.setState({
                marginTopOffset: diffHeight / 2
              });
            }}
          />
          <Button
            buttonStyle={styles.button}
            onPress={onPress}
          >
            COMPLETAR CADASTRO
          </Button>
        </View>
      </View>
    )
  }
}

// styles
const styles = StyleSheet.create({
  container: {
    marginTop: 16,
  },
  image: {
    width: '100%',
  },
  button: {
    position: 'absolute',
    bottom: -23,
    minHeight: 48,
    right: 48,
    left: 48,
  },
});

export default UncompletedRegistration;