// NPM imports
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View} from 'react-native';

// VouD imports
import Header, {headerActionTypes} from '../../components/Header';
import BrandContainer from '../../components/BrandContainer';
import FadeInView from '../../components/FadeInView';
import Loader from '../../components/Loader';
import RequestFeedback from '../../components/RequestFeedback';
import {colors} from '../../styles';
import {openMenu} from '../../redux/menu';
import {viewHelpDetails} from '../../redux/help';
import {
  getIssuerConfig,
  getCurrentTransportCard,
  getTransportCards,
  getRemoveCardUI,
  getIsLoadingConfig,
  getHasProfileAlerts,
} from '../../redux/selectors';
import {setPurchaseTransportCard} from '../../redux/financial';
import {navigateToRoute} from '../../redux/nav';
import {GAEventParams, GATrackEvent} from '../../shared/analytics';
import {FBEventsConstants, FBLogEvent} from '../../shared/facebook';
import {routeNames} from '../../shared/route-names';
import {configErrorHandler} from '../../shared/config-error-handler';
import {
  getBomEscolarQuoteValueAvailable,
  cardHasSufficientQuota,
  insufficientQuotaErrorHandler,
} from '../../shared/insufficient-quota';
import {
  viewDetails,
  changeCurrentCardId,
  fetchCardList,
  fetchNextRecharges,
  transportCardTypes,
} from '../../redux/transport-card';
import {
  getIsLoggedIn,
  getCardListUI,
  getCardHelpId,
  getCurrentCardNextRecharges,
  getHasConfigError,
  getBOMCreditValueRange,
} from '../../redux/selectors';
import {getNotifications} from '../../redux/notifications';

// Group imports
import CardsCarousel from './CardsCarousel';
import HomeEmptyState from './EmptyState';
import UncompletedRegistration from './UncompletedRegistration';
import {checkIssuerEnabled} from '../../utils/issuer-config';
import Banner from './Banner';
import LoadMask from '../../components/LoadMask';
import {authSteps} from '../../redux/auth';
import {dispatchCheckLastTermsAccepted} from '../../flows/usage-terms/utils';
import HeaderSearch from './Search/HeaderSearch';
import {
  getDefaultRequestPositionConfig,
  getCurrentPosition,
} from '../../utils/geolocation';
import {setPosition} from '../../redux/profile';

class HomeView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageContainerHeight: null,
      isGettingLocation: false,
    };
  }

  componentWillMount() {
    // const {dispatch} = this.props;
    // wait 1 second to get user position, avoiding
    // alert over half closed menu
    this.setState({isGettingLocation: true});

    this._getUserLocation(
      getDefaultRequestPositionConfig(true),
      true,
      getDefaultRequestPositionConfig(false),
    );
    this._refreshCardList();
  }

  componentDidUpdate(prevProps) {
    const {isLoggedIn, dispatch} = this.props;
    const prevIsLoggedIn = prevProps.isLoggedIn;

    if (
      (isLoggedIn && !prevIsLoggedIn) ||
      prevProps.currentStep === authSteps.REGISTER_SUCCESS
    ) {
      this._forceRefreshCardList();
      dispatchCheckLastTermsAccepted(dispatch);
    }

    // fetch next recharges

    const {cards, currentCardId, currentCardIdNextRecharges} = this.props;
    const currentCard =
      cards && cards.find(card => card.uuid === currentCardId);

    if (
      !currentCardIdNextRecharges &&
      currentCard &&
      currentCard.layoutType !== transportCardTypes.BU
    ) {
      this.props.dispatch(fetchNextRecharges(currentCard.uuid));
    }
  }

  _getUserLocation = (config, shouldRetry = false, retryConfig) => {
    const {dispatch} = this.props;

    return new Promise(async (resolve, reject) => {
      try {
        const position = await getCurrentPosition(
          config,
          shouldRetry,
          retryConfig,
        );
        dispatch(setPosition(position.latitude, position.longitude));
        this.setState({isGettingLocation: false});
        resolve(true);
      } catch (error) {
        this.setState({isGettingLocation: false});
        reject(false);
      }
    });
  };

  _forceRefreshCardList = () => {
    const {cardListUi, dispatch} = this.props;

    if (!cardListUi.isFetching) {
      dispatch(fetchCardList());
    }
  };

  _refreshCardList = () => {
    const {isLoggedIn, cardListUi, dispatch} = this.props;

    if (isLoggedIn && !cardListUi.isFetching) {
      dispatch(fetchCardList());
    }
  };

  _refreshNotifications = force => {
    const {isLoggedIn, notificationsUi, dispatch} = this.props;

    if (!notificationsUi.isFetching && (isLoggedIn || force)) {
      dispatch(getNotifications());
    }
  };

  _openMenu = () => {
    const {dispatch} = this.props;
    dispatch(openMenu());
  };

  _goToNotificationCenter = () => {
    this.props.dispatch(navigateToRoute(routeNames.NOTIFICATION_CENTER));
  };

  _goToAddCard = () => {
    const {
      categories: {BUTTON},
      actions: {CLICK},
      labels: {ADD_CARD},
    } = GAEventParams;
    GATrackEvent(BUTTON, CLICK, ADD_CARD);
    this.props.dispatch(navigateToRoute(routeNames.ADD_CARD));
  };

  _goToAuth = () => {
    const {
      categories: {BUTTON},
      actions: {CLICK},
      labels: {LOGIN},
    } = GAEventParams;
    GATrackEvent(BUTTON, CLICK, LOGIN);
    this.props.dispatch(navigateToRoute(routeNames.AUTH));
  };

  _goToProfile = () => {
    const {
      categories: {BUTTON},
      actions: {CLICK},
      labels: {MY_PROFILE},
    } = GAEventParams;
    GATrackEvent(BUTTON, CLICK, MY_PROFILE);
    this.props.dispatch(navigateToRoute(routeNames.MY_PROFILE));
  };

  _goToReport = () => {
    const {
      categories: {BUTTON},
      actions: {CLICK},
      labels: {REPORT},
    } = GAEventParams;
    GATrackEvent(BUTTON, CLICK, REPORT);
    this.props.dispatch(navigateToRoute(routeNames.REPORTS));
  };

  _viewCardDetails = cardId => {
    const {dispatch} = this.props;
    const {
      categories: {BUTTON},
      actions: {CLICK},
      labels: {TRANSPORT_CARD_DETAILS},
    } = GAEventParams;

    GATrackEvent(BUTTON, CLICK, TRANSPORT_CARD_DETAILS);
    dispatch(viewDetails(cardId));
    dispatch(navigateToRoute(routeNames.CARD_DETAILS));
  };

  _viewHelp = () => {
    const {dispatch, cardHelpId} = this.props;
    dispatch(viewHelpDetails(cardHelpId));
    dispatch(navigateToRoute(routeNames.HELP_DETAILS));
  };

  _buyCredits = () => {
    const {
      dispatch,
      hasConfigError,
      bomCreditValueRange: {minCreditValue},
      issuerConfig,
      cardData,
    } = this.props;

    if (!cardData) {
      return;
    }

    if (hasConfigError) {
      configErrorHandler();
    } else if (!cardHasSufficientQuota(cardData, minCreditValue / 100)) {
      const quotaValue = getBomEscolarQuoteValueAvailable(cardData);
      insufficientQuotaErrorHandler(quotaValue, minCreditValue / 100);
    } else {
      if (checkIssuerEnabled(cardData.issuerType, issuerConfig)) {
        const {
          categories: {BUTTON},
          actions: {CLICK},
          labels: {BUY},
        } = GAEventParams;
        GATrackEvent(BUTTON, CLICK, BUY);
        FBLogEvent(FBEventsConstants.INITIATED_CHECKOUT);

        dispatch(setPurchaseTransportCard(cardData.uuid));
        dispatch(navigateToRoute(routeNames.BUY_CREDIT));
      }
    }
  };

  _changeCurrentCard = index => {
    const {cards, dispatch} = this.props;
    dispatch(changeCurrentCardId(cards[index].id));
  };

  _renderHeader = () => {
    const {isLoggedIn, isLoadingConfig, isInitialized} = this.props;

    // app initialization
    if (!isInitialized || isLoadingConfig) {
      return <Header withLogo />;
    } else if (isLoggedIn) {
      return (
        <Header
          withLogo
          left={{
            type: headerActionTypes.MENU,
            onPress: this._openMenu,
          }}
        />
      );
    } else {
      return (
        <Header
          withLogo
          left={{
            type: headerActionTypes.MENU,
            onPress: this._openMenu,
          }}
        />
      );
    }
  };
  _renderSearchRoutes = () => {
    const {geolocPermGranted, isLoggedIn} = this.props;
    if (!isLoggedIn) {
      return null;
    }

    return (
      <HeaderSearch
        style={{paddingBottom: 16}}
        geolocPermGranted={geolocPermGranted}
      />
    );
  };

  _renderMainContent = () => {
    const {
      cards,
      currentCardId,
      isInitialized,
      isLoadingConfig,
      isLoggedIn,
      hasConfigError,
      cardListUi,
      hasProfileAlert,
    } = this.props;

    // app initialization
    if (!isInitialized || isLoadingConfig) {
      return (
        <FadeInView style={styles.main} key="1">
          <Loader text="Carregando..." style={styles.loader} />
        </FadeInView>
      );
    }

    if (isLoggedIn && hasProfileAlert) {
      return (
        <FadeInView style={styles.container} key="4">
          <View style={styles.mainEmptyState}>
            <UncompletedRegistration onPress={this._goToProfile} />
          </View>
        </FadeInView>
      );
    }

    // failed to load card(s)
    if (isLoggedIn && !hasConfigError && cardListUi.error !== '') {
      return (
        <RequestFeedback
          key="2"
          style={styles.requestFeedbackContainer}
          errorMessage={cardListUi.error}
          onRetry={this._refreshCardList}
          retryMessage="Tentar novamente"
        />
      );
    }

    // user logged, with registered card(s)
    if (isLoggedIn && cards[0]) {
      return (
        <FadeInView style={styles.main} key="3">
          <CardsCarousel
            cards={cards}
            currentCardId={currentCardId}
            onAddCard={this._goToAddCard}
            isUpdating={cardListUi.isFetching}
            onChangeIndex={this._changeCurrentCard}
            onBuy={this._buyCredits}
            onViewDetails={this._viewCardDetails}
            onCardHelp={this._viewHelp}
            onRefresh={this._refreshCardList}
          />
        </FadeInView>
      );
    }

    // user logged, no cards
    if (isLoggedIn) {
      return (
        <FadeInView style={styles.container} key="4">
          <View style={styles.mainEmptyState}>
            <HomeEmptyState
              isLoggedIn
              isUpdating={cardListUi.isFetching}
              onAddCard={this._goToAddCard}
              onAuth={this._goToAuth}
            />
          </View>
        </FadeInView>
      );
    }

    // user not logged
    return (
      <FadeInView style={styles.mainEmptyState} key="5">
        <HomeEmptyState onAddCard={this._goToAddCard} onAuth={this._goToAuth} />
      </FadeInView>
    );
  };

  render() {
    const {hasConfigError, isLoggedIn, removeCardUi} = this.props;

    return (
      <BrandContainer bottomPos={0}>
        {this._renderHeader()}
        {this._renderSearchRoutes()}
        {this._renderMainContent()}
        {isLoggedIn && !hasConfigError && <Banner style={{paddingTop: 16}} />}
        {removeCardUi.isFetching && <LoadMask />}
      </BrandContainer>
    );
  }
}

// component styles
const styles = {
  container: {
    flex: 1,
  },
  main: {
    flex: 1,
    justifyContent: 'center',
  },
  mainEmptyState: {
    flex: 1,
    paddingHorizontal: 8,
    paddingTop: 8,
    paddingBottom: 36,
  },
  loader: {
    alignSelf: 'center',
  },
  loginInfo: {
    paddingHorizontal: 16,
    marginTop: -24,
  },
  loginText: {
    marginTop: 16,
    fontSize: 14,
    lineHeight: 20,
    textAlign: 'center',
    color: 'white',
  },
  bottom: {
    padding: 16,
    backgroundColor: colors.BRAND_PRIMARY_LIGHTER,
    shadowColor: 'black',
    shadowOffset: {width: -4, height: 0},
    shadowOpacity: 0.25,
    elevation: 8,
  },
  requestFeedbackContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 8,
  },
};

// redux connect and export
const mapStateToProps = state => ({
  isLoadingConfig: getIsLoadingConfig(state),
  hasConfigError: getHasConfigError(state),
  isInitialized: state.init,
  isLoggedIn: getIsLoggedIn(state),
  cards: getTransportCards(state),
  cardData: getCurrentTransportCard(state),
  cardListUi: getCardListUI(state),
  cardHelpId: getCardHelpId(state),
  bomCreditValueRange: getBOMCreditValueRange(state),
  fcmToken: state.profile.fcmToken,
  currentCardId: state.transportCard.currentDetailId,
  hasProfileAlert: getHasProfileAlerts(state),
  currentCardIdNextRecharges: getCurrentCardNextRecharges(state),
  issuerConfig: getIssuerConfig(state),
  removeCardUi: getRemoveCardUI(state),
  currentStep: state.auth.currentStep,
  geolocPermGranted: state.profile.position.geolocPermGranted,
});

export const Home = connect(mapStateToProps)(HomeView);
