// NPM imports
import React, {Component} from 'react';
import {Dimensions, Platform, StyleSheet, View} from 'react-native';
import Carousel from 'react-native-snap-carousel';

// VouD imports
import TransportCard from '../../components/TransportCard';
import PullToRefreshContainer from '../../components/TransportCard/PullToRefreshContainer';
import Button from '../../components/Button';
import Dots from '../../components/Dots';
import {transportCardTypes} from '../../redux/transport-card';

// utils
function getIndexForCurrentProps({cards, currentCardId}) {
  // return 0 if currentCardId is not defined or cards is empty
  if (!currentCardId || !(cards && cards[0])) return 0;

  for (let i = 0; i < cards.length; i += 1) {
    const card = cards[i];
    if (card.uuid === currentCardId) return i;
  }

  // return 0 if id not found
  return 0;
}

function defineItemWidth(screenWidth) {
  if (screenWidth <= 360) return screenWidth - 40;
  return screenWidth - 80;
}

class CardsCarousel extends Component {
  constructor(props) {
    super(props);

    this.carouselRef = null;

    const screenHeight = Dimensions.get('window').height;
    this.screenHasLowHeight =
      Platform.OS === 'ios' ? screenHeight < 600 : screenHeight < 500;
  }

  _viewDetails = cardId => {
    this.props.onViewDetails(cardId);
  };

  _buyCredits = () => {
    this.props.onBuy();
  };

  _renderCard = ({item, index}) => {
    return (
      <PullToRefreshContainer
        cardHeight={200}
        onRefresh={this.props.onRefresh}
        style={styles.cardContainer}>
        <TransportCard
          data={item}
          isUpdating={this.props.isUpdating}
          onHelp={this.props.onCardHelp}
          style={styles.cardStyleSmall}
          onPress={() => {
            const currentIndex = getIndexForCurrentProps(this.props);
            if (index === currentIndex) {
              this._viewDetails(item.id);
            } else {
              this.carouselRef.snapToItem(index);
            }
          }}
          showSpinner
          small
        />
      </PullToRefreshContainer>
    );
  };

  _renderMultipleCards = cards => {
    const currentIndex = getIndexForCurrentProps(this.props);
    return (
      <View style={{paddingVertical: 16}}>
        <Carousel
          key={cards.length}
          ref={el => {
            this.carouselRef = el;
          }}
          data={cards}
          keyExtractor={item => item.id}
          renderItem={this._renderCard}
          firstItem={currentIndex}
          sliderWidth={Dimensions.get('window').width}
          itemWidth={defineItemWidth(Dimensions.get('window').width)}
          inactiveSlideScale={1}
          inactiveSlideOpacity={Platform.OS === 'ios' ? 0.5 : 1}
          onSnapToItem={this.props.onChangeIndex}
        />
        <Dots
          index={currentIndex}
          total={cards.length}
          style={styles.dotsContainer}
          onDotPress={index => {
            this.carouselRef && this.carouselRef.snapToItem(index);
          }}
        />
      </View>
    );
  };

  _renderSingleCard = card => {
    return (
      <PullToRefreshContainer
        onRefresh={this.props.onRefresh}
        style={styles.singleCardContainer}>
        <TransportCard
          data={card}
          isUpdating={this.props.isUpdating}
          onHelp={this.props.onCardHelp}
          onPress={() => this._viewDetails(card.uuid)}
          showSpinner
        />
      </PullToRefreshContainer>
    );
  };

  _renderCards = () => {
    const {cards} = this.props;

    if (cards.length > 1) return this._renderMultipleCards(cards);

    return this._renderSingleCard(cards[0]);
  };

  _canBuy = () => {
    const {cards} = this.props;
    const currentIndex = getIndexForCurrentProps(this.props);
    const layout = cards[currentIndex] ? cards[currentIndex].layoutType : '';

    return layout !== transportCardTypes.CITY_PLUS_ESCOLAR_GRATUIDADE;
  };

  render() {
    const {onAddCard} = this.props;

    return (
      <View style={styles.container}>
        {this._renderCards()}
        <View style={styles.buttonContainer}>
          <Button
            onPress={this._buyCredits}
            style={styles.button}
            disabled={!this._canBuy()}
            sm={this.screenHasLowHeight}>
            Comprar Créditos
          </Button>
          <Button
            onPress={onAddCard}
            style={styles.button}
            buttonStyle={styles.addCardButton}
            outline
            sm={this.screenHasLowHeight}>
            Incluir cartão City +
          </Button>
        </View>
      </View>
    );
  }
}

// styles
const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    alignItems: 'stretch',
  },
  cardContainer: {
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  singleCardContainer: {
    paddingHorizontal: 16,
  },
  buttonContainer: {
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  button: {
    marginTop: 16,
  },
  addCardButton: {},
  dotsContainer: {
    marginTop: 16,
    marginBottom: 16,
    marginLeft: 16,
    marginRight: 16,
  },
  cardStyleSmall: {
    height: 200,
  },
});

export default CardsCarousel;
