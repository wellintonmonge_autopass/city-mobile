// NPM imports
import React, {Component} from 'react';
import {StyleSheet, Image, View} from 'react-native';

// VouD Imports
import Button from '../../components/Button';
import Loader from '../../components/Loader';

// Images
const cardsPlaceholderLogged = require('../../images/cards-logged.png');
const cardsPlaceholderAnonymous = require('../../images/cards-anonymous.png');
const CARDS_PLACEHOLDER_RATIO = 0.99425287356;

class HomeEmptyState extends Component {
  constructor(props) {
    super(props);

    this.state = {
      marginTopOffset: 0,
    };
  }

  _onButtonPress = () => {
    const {isLoggedIn, onAddCard, onAuth} = this.props;

    if (isLoggedIn) {
      onAddCard();
    } else {
      onAuth();
    }
  };

  render() {
    const {isLoggedIn, isUpdating} = this.props;

    return (
      <View style={styles.container}>
        <View>
          <Image
            style={StyleSheet.flatten([
              styles.emptyStateImage,
              // {marginTop: -this.state.marginTopOffset},
            ])}
            source={
              isLoggedIn ? cardsPlaceholderLogged : cardsPlaceholderAnonymous
            }
            resizeMode="contain"
            opacity={isLoggedIn && isUpdating ? 0.2 : 1}
            onLayout={event => {
              const {width, height} = event.nativeEvent.layout;
              const realHeight = width / CARDS_PLACEHOLDER_RATIO;
              const diffHeight = height - realHeight;

              this.setState({
                // marginTopOffset: diffHeight / 2,
              });
            }}
          />
          {isLoggedIn && isUpdating && (
            <View style={styles.loaderContainer}>
              <Loader text="Atualizando cartões..." style={styles.loader} />
            </View>
          )}
          <Button
            buttonStyle={styles.emptyaddCardButton}
            onPress={this._onButtonPress}>
            {isLoggedIn
              ? 'Incluir cartão City +'
              : 'Cadastre-se ou faça o login'}
          </Button>
        </View>
      </View>
    );
  }
}

// styles
const styles = StyleSheet.create({
  container: {
    marginTop: 16,
  },
  emptyStateImage: {
    width: '100%',
  },
  emptyaddCardButton: {
    position: 'absolute',
    bottom: -23,
    minHeight: 48,
    right: 48,
    left: 48,
  },
  loaderContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default HomeEmptyState;
