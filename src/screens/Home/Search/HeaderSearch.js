// NPM imports
import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';

// VouD imports
import ButtonField from './ButtonField';
import {navigateToRoute} from '../../../redux/nav';
import {showLocationWithoutPermissionAlert} from '../../../utils/geolocation';
import {routeNames} from '../../../shared/route-names';

export const HEADER_HEIGHT = 140;

class HeaderSearch extends Component {
  constructor(props) {
    super(props);
  }

  _openSearch = async () => {
    const {dispatch, geolocPermGranted} = this.props;
    if (geolocPermGranted) {
      dispatch(navigateToRoute(routeNames.SEARCH_POINTS_OF_INTEREST));
    } else {
      showLocationWithoutPermissionAlert();
    }
  };

  render() {
    return (
      <View style={styles.wrapper}>
        <ButtonField onPress={this._openSearch} icon="md-search" />
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
});

export default connect()(HeaderSearch);
