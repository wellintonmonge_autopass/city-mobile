// NPM imports
import React, { Component } from 'react';
import { View, BackHandler, StyleSheet, Alert } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

// VouD imports
import Dialog from '../../../components/Dialog';
import { getBUSupportedCreditTypesSelector, getPaymentTransactionUI, getSaveSmartPurchaseUI } from '../../../redux/selectors';
import PurchaseConfirmationForm from './PurchaseConfirmationForm';
import { GATrackEvent, GAEventParams } from '../../../shared/analytics';
import { fetchPaymentTransaction, paymentCardTypes, productTypes, requestPaymentTransactionClear } from '../../../redux/financial';
import { routeNames } from '../../../shared/route-names';
import { navigateToRoute } from '../../../redux/nav';
import { voudErrorCodes } from '../../../shared/services';
import LoadMask from '../../../components/LoadMask';
import { fetchSaveSmartPurchase } from '../../../redux/smart-purchase';
import KeyboardDismissView from '../../../components/KeyboardDismissView';

// Screen component
class PurchaseConfirmationDialogView extends Component {

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler);
    this.props.dispatch(requestPaymentTransactionClear());
  }

  _backHandler = () => {
    this._dismiss();
    return true;
  };

  _dismiss = () => {
    const { dispatch } = this.props;
    dispatch(NavigationActions.back());
  };

  _handlePaymentError = error => {
    const { dispatch, navigation: { state: { params: { paymentData } } } } = this.props;
    const paymentCardType = paymentData.paymentCardType;

    if (error.statusCode === voudErrorCodes.EXCEEDED_DAILY_PURCHASE_LIMIT) {
      Alert.alert(
        'Limite diário excedido',
        'Você atingiu o limite de compras permitidas para o dia. Por favor, tente novamente amanhã.',
        [
          {
            text: 'OK, entendi',
            onPress: this._dismissAlert
          }
        ],
        {
          onDismiss: this._dismissAlert
        }
      );
      return;
    }

    if (paymentCardType === paymentCardTypes.DEBIT) {
      dispatch(navigateToRoute([NavigationActions.back(), routeNames.PAYMENT_ERROR], { paymentData, error }));
    }

    if (paymentCardType === paymentCardTypes.CREDIT) {
      dispatch(navigateToRoute([NavigationActions.back(), routeNames.PAYMENT_ERROR], { paymentData, error }));
    }
  }

  _handlePaymentSuccess = () => {
    const { dispatch, navigation: { state: { params: { paymentData, smartPurchaseFlow } } } } = this.props;
    const productType = paymentData ? paymentData.productType : '';

    if (productType === productTypes.PHONE_RECHARGE) {
      dispatch(navigateToRoute(routeNames.PHONE_RECHARGE_SUCCESSFUL, { paymentData }));
    } else {
      dispatch(navigateToRoute(routeNames.PAYMENT_SUCCESSFUL, { paymentData, smartPurchaseFlow }));
    }
  }

  _submit = ({ creditCardSecurityCode }) => {
    const { dispatch, navigation: { state: { params: { paymentData, smartPurchaseFlow } } } } = this.props;
    const { categories: { FORM }, actions: { SUBMIT }, labels: { SUBMIT_CREDIT_CARD_PAYMENT } } = GAEventParams;

    const completePaymentData = {
      ...paymentData,
      creditCardSecurityCode
    };
    const fetchAction = smartPurchaseFlow ? fetchSaveSmartPurchase : fetchPaymentTransaction;

    if (!smartPurchaseFlow) {
      GATrackEvent(FORM, SUBMIT, SUBMIT_CREDIT_CARD_PAYMENT);  
    }

    dispatch(fetchAction(completePaymentData))
      .then(this._handlePaymentSuccess)
      .catch(this._handlePaymentError);
  }

  render() {
    const { 
      navigation: { state: { params: { paymentData, smartPurchaseFlow } } },
      paymentTransactionUi,
      saveSmartPurchaseUi
    } = this.props;

    return (
      <View style={styles.mainContainer}>
        <Dialog
          noPadding
          onDismiss={this._dismiss}
        >
          <KeyboardDismissView style={styles.dialogContent}>
            <PurchaseConfirmationForm
              onSubmit={this._submit}
              paymentData={paymentData}
              smartPurchaseFlow={smartPurchaseFlow}
            />
          </KeyboardDismissView>
        </Dialog>
        {
          (paymentTransactionUi.isFetching || saveSmartPurchaseUi.isFetching) &&
            <LoadMask />
        }
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1
  },
  dialogContent: {
    flex: 0
  },
});

const mapStateToProps = state => ({
  buSupportedCreditTypes: getBUSupportedCreditTypesSelector(state),
  paymentTransactionUi: getPaymentTransactionUI(state),
  saveSmartPurchaseUi: getSaveSmartPurchaseUI(state)
});

export const PurchaseConfirmationDialog = connect(mapStateToProps)(PurchaseConfirmationDialogView);
