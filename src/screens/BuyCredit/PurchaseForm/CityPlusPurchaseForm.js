// NPM imports
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { change } from 'redux-form';

import {
  reduxForm,
  Field,
  formValueSelector,
} from 'redux-form';

import {
  StyleSheet,
  View,
  Keyboard,
} from 'react-native';

// VouD imports
import { colors } from '../../../styles';
import { required } from '../../../utils/validators';
import { productTypes } from '../../../redux/financial';
import { getCreditValueRange, getHasCurrentTransportCardActiveSmartPurchase } from '../../../redux/selectors';
import BuySummary from '../../PaymentCheckout/BuySummary';

import { getDefaultCreditValueFieldLabel } from '../../../utils/transport-card';
import CreditValueField from './CreditValueField';
import PaymentMethodField from './PaymentMethodField';
import ScheduledDayField from './ScheduledDayField';
import SmartPurchaseCheckboxField from './SmartPurchaseCheckboxField';
import { routeNames } from '../../../shared/route-names';
import { clearReduxForm } from '../../../utils/redux-form-util';

// consts
const reduxFormName = 'cityPlusPurchaseForm';

// Component
class CityPlusPurchaseForm extends Component {
  
  componentWillUnmount() {
    const { dispatch } = this.props;
    clearReduxForm(dispatch, reduxFormName);
  }

  _onPaymentMethodChange = paymentMethod => {
    if (paymentMethod && paymentMethod.isTemporaryCard && !paymentMethod.saveCreditCard) {
      this.props.dispatch(change(reduxFormName, 'enableSmartPurchase', false));
    }
  }

  _submit = () => {
    const { handleSubmit, onSubmit, valid } = this.props;
    Keyboard.dismiss();

    if (valid) {
      handleSubmit(onSubmit)();
    }
  };

  render() {
    const {
      cardData,
      creditValue,
      valid,
      style,
      enableSmartPurchase,
      paymentMethod,
      hasSmartPurchase,
      smartPurchaseFlow,
      editSmartPurchase,
      onShowLastSmartPurchaseFireAlert
    } = this.props;
    const creditValueFieldLabel = getDefaultCreditValueFieldLabel(cardData.layoutType);
    
    return (
      <View style={style}>
        <View style={styles.fields}>
          <View style={styles.creditSelectionContainer}>
            <CreditValueField
              name="creditValue"
              reduxFormName={reduxFormName}
              cardData={cardData}
            />
          </View>
          {
            smartPurchaseFlow &&
            <Field
              name="scheduledDay"
              props={{
                style: styles.scheduledDayFieldWithBorder,
                onShowLastSmartPurchaseFireAlert,
                editSmartPurchase
              }}
              component={ScheduledDayField}
              validate={[required]}
            />
          }
          <Field
            name="paymentMethod"
            props={{ 
              reduxFormName,
              purchaseFlow: !smartPurchaseFlow,
              backRouteName: routeNames.BUY_CREDIT,
            }}
            onChange={this._onPaymentMethodChange}
            component={PaymentMethodField}
            validate={[required]}
          />
          {
            !smartPurchaseFlow && !hasSmartPurchase && paymentMethod &&
            <Fragment>
              <Field
                name="enableSmartPurchase"
                props={{
                  cardData
                }}
                component={SmartPurchaseCheckboxField}
              />
              {
                enableSmartPurchase &&
                <Field
                  name="scheduledDay"
                  props={{
                    style: styles.scheduledDayField
                  }}
                  component={ScheduledDayField}
                />
              }
            </Fragment>
          }
        </View>
        <BuySummary
          rechargeValue={creditValue}
          creditValueLabel={creditValueFieldLabel}
          valid={valid}
          productType={productTypes.CITY_PLUS}
          showSubmitButton={valid}
          smartPurchaseFlow={smartPurchaseFlow}
          submit={this._submit}
        />
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  fields: {
    flexGrow: 1,
  },
  addCreditBtnContainer: {
    flexDirection: 'row'
  },
  addCreditBtn: {
    flex: 1,
    marginRight: 8
  },
  setCreditBtn: {
    flex: 1,
    marginBottom: 8
  },
  setCreditInfo: {
    fontSize: 14,
    color: colors.GRAY
  },
  temporalFieldGroup: {
    marginTop: 16
  },
  creditSelectionContainer: {
    paddingTop: 16,
    paddingHorizontal: 16
  },
  scheduledDayField: {
    marginTop: 16
  },
  scheduledDayFieldWithBorder: {
    marginTop: 16,
    paddingTop: 16,
    borderTopWidth: 1,
    borderColor: colors.GRAY_LIGHTER,
  },
});

// Redux
const mapStateToProps = (state, ownProps) => {
  const { editSmartPurchase, smartPurchaseFlow } = ownProps;
  return {
    initialValues: {
      creditValue: editSmartPurchase ? editSmartPurchase.rechargeValue * 100 : 0,
      paymentMethod: null,
      scheduledDay: editSmartPurchase ? editSmartPurchase.scheduledDay : null,
      enableSmartPurchase: smartPurchaseFlow ? true : false,
    },
    creditValue: formValueSelector(reduxFormName)(state, 'creditValue'),
    enableSmartPurchase: formValueSelector(reduxFormName)(state, 'enableSmartPurchase'),
    paymentMethod: formValueSelector(reduxFormName)(state, 'paymentMethod'),
    creditValueRange: getCreditValueRange(state),
    hasSmartPurchase: getHasCurrentTransportCardActiveSmartPurchase(state),
  };
};

export default connect(mapStateToProps)(reduxForm({ form: reduxFormName, destroyOnUnmount: false })(CityPlusPurchaseForm));