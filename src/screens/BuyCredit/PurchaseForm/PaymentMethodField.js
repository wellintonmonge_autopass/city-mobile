
// NPM imports
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Image
} from 'react-native';

// VouD imports
import { getSelectedPaymentMethod, getHasSavedPayment } from '../../../redux/selectors';
import Button from '../../../components/Button';
import { navigateToRoute } from '../../../redux/nav';
import { routeNames } from '../../../shared/route-names';
import BrandText from '../../../components/BrandText';
import { getLogoForBrand } from '../../../utils/payment-card';
import { colors } from '../../../styles';
import TouchableNative from '../../../components/TouchableNative';


// Component
const propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
};

const defaultProps = {
  style: {},
};

class PaymentMethodField extends Component {

  componentDidMount() {
    const { selectedPaymentMethod, input } = this.props;
    input.onChange(selectedPaymentMethod);
  }

  componentDidUpdate(prevProps) {
    const { selectedPaymentMethod, input } = this.props;
    const prevSelectedPaymentMethod = prevProps.selectedPaymentMethod;

    if ((prevSelectedPaymentMethod && !selectedPaymentMethod) || (!prevSelectedPaymentMethod && selectedPaymentMethod) ||
      (selectedPaymentMethod && (selectedPaymentMethod.id !== prevSelectedPaymentMethod.id ||
        selectedPaymentMethod.saveCreditCard !== prevSelectedPaymentMethod.saveCreditCard))) {

      // Note: redux-form doesn't update field value if onChange parameter is undefined
      const updateSelectedPaymentMethod = !selectedPaymentMethod ? null : selectedPaymentMethod;
      input.onChange(updateSelectedPaymentMethod);
    }
  }

  _goToAddPaymentMethod = () => {
    const { dispatch, purchaseFlow, hasSavedPayment } = this.props;

    if (hasSavedPayment) {
      this._goToSelectPaymentMethod();
    } else {
      dispatch(navigateToRoute(routeNames.ADD_PAYMENT_METHOD, { purchaseFlow }));
    }
  }

  _goToSelectPaymentMethod = () => {
    const { backRouteName, purchaseFlow } = this.props;
    this.props.dispatch(navigateToRoute(routeNames.SELECT_PAYMENT_METHOD, { backRouteName, purchaseFlow }));
  }

  render() {
    const { selectedPaymentMethod, disabled } = this.props;

    return (
      <Fragment>
        {
          selectedPaymentMethod ?
            <TouchableNative 
              style={styles.paymentMethodContainer}
              onPress={this._goToSelectPaymentMethod}
            >
              <Image 
                style={styles.cardBrandImg}
                source={getLogoForBrand(selectedPaymentMethod.items.cardFlag)}
              />
              <View style={styles.cardInfo}>
                <BrandText style={styles.cardTypeText}>
                  Cartão para crédito
                </BrandText>
                <BrandText style={styles.cardNameText}>
                  {selectedPaymentMethod.name}
                </BrandText>
              </View>
              <BrandText style={styles.changeText}>
                TROCAR
              </BrandText>
            </TouchableNative>
            :
            <Button 
              style={styles.addPaymentMethodButton}
              onPress={this._goToAddPaymentMethod}
              disabled={disabled}
            >
              Forma de pagamento
            </Button>
        }
      </Fragment>
    );
  }
}

PaymentMethodField.propTypes = propTypes;
PaymentMethodField.defaultProps = defaultProps;

// Styles
const styles = StyleSheet.create({
  addPaymentMethodButton: {
    marginVertical: 24,
    marginHorizontal: 16,
  },
  paymentMethodContainer: {
    marginTop: 16,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: colors.GRAY_LIGHTER,
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  cardBrandImg: {
    height: 32,
    width: 32,
    marginRight: 16
  },
  cardInfo: {
    flex: 1,
  },
  cardTypeText: {
    color: colors.GRAY,
    fontSize: 14,
    lineHeight: 16,
    marginBottom: 4
  },
  cardNameText: {
    color: colors.GRAY_DARKER,
    fontSize: 16,
    lineHeight: 20
  },
  changeText: {
    color: colors.BRAND_SECONDARY,
    fontWeight: 'bold',
    fontSize: 14,
    lineHeight: 20
  }
});

// Redux
const mapStateToProps = (state) => {
  return {
    selectedPaymentMethod: getSelectedPaymentMethod(state),
    hasSavedPayment: getHasSavedPayment(state)
  };
};

export default connect(mapStateToProps)(PaymentMethodField);
