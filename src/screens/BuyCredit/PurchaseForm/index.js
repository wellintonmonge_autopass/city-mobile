// NPM imports
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import CityPlusPurchaseForm from './CityPlusPurchaseForm';

const propTypes = {
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
};

const defaultProps = {
  style: {},
};

class PurchaseForm extends Component {
  render() {
    const FormComponent = CityPlusPurchaseForm;

    return (
      <FormComponent
       {...this.props}
      />
    );
  }
}

PurchaseForm.propTypes = propTypes;
PurchaseForm.defaultProps = defaultProps;


export default PurchaseForm;
