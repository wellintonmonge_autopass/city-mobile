// NPM imports
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  BackHandler,
  View,
  ScrollView,
  StyleSheet,
  AppState
} from 'react-native';
import Moment from 'moment';

// VouD imports
import Header, { headerActionTypes } from '../../components/Header';
import TransportCardSm from '../../components/TransportCardSm';
import KeyboardDismissView from '../../components/KeyboardDismissView';
import { getPurchaseTransportCard, getSavedPaymentMethodsUI, getSmartPurchaseListUI, getEditTransportCardUI, getApiStatusUI, getSelectedPaymentMethod } from '../../redux/selectors';
import { navigateToRoute } from '../../redux/nav';
import { fetchSavedPaymentMethods, selectFirstPaymentMethod, selectPaymentMethod, unselectPaymentMethod } from '../../redux/payment-method';
import { fetchSmartPurchases } from '../../redux/smart-purchase';
import { routeNames } from '../../shared/route-names';
import { showBuySessionExitAlert } from '../../shared/buy-session-timer';
// Group imports
import { productTypes, paymentCardTypes } from '../../redux/financial';
import { getStateCurrentRouteName } from '../../utils/nav-util';
import { findBUProduct, buCreditTypeLabels } from '../../utils/transport-card';
import RequestFeedback from '../../components/RequestFeedback';
import LoadMask from '../../components/LoadMask';
import BrandText from '../../components/BrandText';
import PurchaseForm from './PurchaseForm';
import { colors } from '../../styles';
import { fetchAPIStatus } from '../../redux/api-status';

// Screen component
class BuyCreditView extends Component {

  constructor(props) {
    super(props);

    this.state = {
      showSmartPurchaseAlert: false
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;
    BackHandler.addEventListener('hardwareBackPress', this._backHandler);
    this._fetchRequests();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler);
    this.props.dispatch(unselectPaymentMethod());
  }

  _fetchRequests = () => {
    const { dispatch, navigation: { state: { params } }, selectedPaymentMethod } = this.props;
    const editSmartPurchase = params && params.editSmartPurchase ? params.editSmartPurchase : null;

    dispatch(fetchSavedPaymentMethods()).then(() => {

      if (editSmartPurchase) {
        const paymentMethodId = editSmartPurchase.paymentMethod ? editSmartPurchase.paymentMethod.id : null;
        dispatch(selectPaymentMethod(paymentMethodId));
      } else {
        if (!selectedPaymentMethod) dispatch(selectFirstPaymentMethod());
      }

    });
    dispatch(fetchAPIStatus());

    if (!this._isSmartPurchaseFlow()) {
      dispatch(fetchSmartPurchases());
    }
  }

  _backHandler = () => {
    const { nav } = this.props;
    const currentRouteName = getStateCurrentRouteName(nav);

    if (currentRouteName === routeNames.BUY_CREDIT) {
      this._back();
      return true;
    }
    return false;
  }

  _getBUAdditionalData = ({ buCreditType, periodType, transportType, quotaQty }) => {
    const { cardData } = this.props;
    const selectedBUProduct = findBUProduct(cardData.wallets, buCreditType, periodType, transportType);
    return {
      idTransportCardWallet: selectedBUProduct ? selectedBUProduct.id : 0,
      productQuantity: buCreditType === buCreditTypeLabels.TEMPORAL ? quotaQty : 0,
      buCreditType,
      periodType,
      transportType
    };
  }

  _getPaymentInfo = formData => {
    const {
      cardData,
      navigation: { state: { params } }
    } = this.props;
    const {
      paymentMethod : {
        id,
        isTemporaryCard,
        saveCreditCard,
        items : { cardNumber, expirationDate, cardFlag, cardToken, cardHolder }
      },
      creditValue,
      enableSmartPurchase,
      scheduledDay,
    } = formData;
    const editSmartPurchase = params && params.editSmartPurchase ? params.editSmartPurchase : null;

    return {
      productType: productTypes.CITY_PLUS,
      transportCardNumber: cardData.cardNumber,
      transportCardId: cardData.uuid,
      transportCardIssuer: cardData.issuer,
      transportCardNick: cardData.nick,
      rechargeValue: creditValue,
      purchaseValue: Number(creditValue),
      scheduledDay: enableSmartPurchase ? scheduledDay : null,
      paymentCardType: paymentCardTypes.CREDIT,
      paymentMethodId: !isTemporaryCard ? id : null,
      creditCardNumber: cardNumber ? cardNumber : null,
      creditCardExpirationDate: expirationDate ? expirationDate : null,
      creditCardBrand: cardFlag ? cardFlag : null,
      cardToken: cardToken ? cardToken : null,
      saveCreditCard: saveCreditCard ? saveCreditCard : null,
      creditCardHolder: cardHolder ? cardHolder : null,
      ...(this._isSmartPurchaseFlow() ? { activateSmartPurchase: true } : {}),
      ...(editSmartPurchase ? { smartPurchaseId: editSmartPurchase.id } : {})
    };
  }

  _submit = formData => {
    const paymentData = this._getPaymentInfo(formData);
    this.props.dispatch(navigateToRoute(routeNames.PURCHASE_CONFIRMATION_DIALOG,
      { paymentData, smartPurchaseFlow: this._isSmartPurchaseFlow() }));
  }

  _back = () => {
    const { dispatch } = this.props;
    showBuySessionExitAlert(dispatch, this._isSmartPurchaseFlow());
  }

  _isSmartPurchaseFlow = () => {
    const { navigation: { state: { params } } } = this.props;
    return params && params.smartPurchaseFlow;
  }

  _showLastSmartPurchaseFireAlert = showSmartPurchaseAlert => {
    this.setState({ showSmartPurchaseAlert });
  }

  _renderSmartPurchaseAlert = () => {
    return (
      <View style={styles.smartPurchaseAlertContainer}>
        <BrandText style={styles.smartPurchaseAlertText}>
          <BrandText>As compras programadas foram encerradas por hoje, </BrandText>
          <BrandText style={styles.fontBold}>mas não se preocupe, ela será realizada no dia seguinte, logo pela manhã</BrandText>
          <BrandText>{`. As próximas compras serão realizadas todo dia ${Moment().format('DD')} de cada mês.`}</BrandText>
        </BrandText>
      </View>
    )
  }

  render() {
    const { 
      cardData,
      savedPaymentMethodsUi,
      smartPurchaseUi,
      removeUi,
      apiStatusUi,
      navigation: { state: { params } }
    } = this.props;
    const isSmartPurchaseFlow = this._isSmartPurchaseFlow();
    const editSmartPurchase = params && params.editSmartPurchase ? params.editSmartPurchase : null;
    const hideForm = savedPaymentMethodsUi.isFetching || savedPaymentMethodsUi.error !== '' ||
      smartPurchaseUi.isFetching || smartPurchaseUi.error !== '' || apiStatusUi.isFetching || apiStatusUi.error !== '';

    return (
      <View style={styles.container}>
        <Header
          title={isSmartPurchaseFlow ? 'Programar compra' : 'Comprar crédito'}
          left={{
            type: headerActionTypes.BACK,
            onPress: this._back
          }}
        />
        {
          this.state.showSmartPurchaseAlert &&
            this._renderSmartPurchaseAlert()
        }
        <ScrollView
          style={styles.scrollView}
          contentContainerStyle={styles.content}
          keyboardShouldPersistTaps="always"
        >
          <KeyboardDismissView>
            {
              hideForm &&
              <View style={styles.requestFeedbackContainer}>
                <RequestFeedback
                  loadingMessage="Carregando..."
                  errorMessage={savedPaymentMethodsUi.error || smartPurchaseUi.error || apiStatusUi.error}
                  retryMessage="Tentar novamente"
                  isFetching={savedPaymentMethodsUi.isFetching || smartPurchaseUi.isFetching || apiStatusUi.isFetching}
                  onRetry={this._fetchRequests}
                />
              </View>
            }
            {/* Note - This View uses hideForm styles to prevent unecessary unmounts when the request states changes */}
            <View style={hideForm ? styles.hideForm : styles.formContainer}>
              <TransportCardSm
                style={styles.transportCard}
                cardName={cardData.nick}
                cardNumber={cardData.cardNumber}
                layoutType={cardData.layoutType}
              />
              <PurchaseForm
                onSubmit={this._submit}
                cardData={cardData}
                style={styles.form}
                smartPurchaseFlow={isSmartPurchaseFlow}
                editSmartPurchase={editSmartPurchase}
                onShowLastSmartPurchaseFireAlert={this._showLastSmartPurchaseFireAlert}
              />
            </View>
          </KeyboardDismissView>
        </ScrollView>
        {
          removeUi.isFetching &&
            <LoadMask />
        }
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  transportCard: {
    marginTop: 16,
    marginHorizontal: 16
  },
  scrollView: {
    flex: 1
  },
  content: {
    flexGrow: 1
  },
  form: {
    flex: 1
  },
  hideForm: {
    width: 0,
    height: 0
  },
  formContainer: {
    flex: 1
  },
  requestFeedbackContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  smartPurchaseAlertContainer: {
    padding: 16,
    backgroundColor: colors.GRAY_DARKER,
  },
  smartPurchaseAlertText: {
    color: 'white',
    fontSize: 14,
    lineHeight: 20,
  },
  fontBold: {
    fontWeight: 'bold'
  }
});

// redux connect and export
const mapStateToProps = state => {
  return {
    cardData: getPurchaseTransportCard(state),
    nav: state.nav,
    savedPaymentMethodsUi: getSavedPaymentMethodsUI(state),
    smartPurchaseUi: getSmartPurchaseListUI(state),
    removeUi: getEditTransportCardUI(state),
    apiStatusUi: getApiStatusUI(state),
    selectedPaymentMethod: getSelectedPaymentMethod(state),
  };
};

export const BuyCredit = connect(mapStateToProps)(BuyCreditView);

// export other screens from group
export * from './TransportCardRechargeSuccessful';
