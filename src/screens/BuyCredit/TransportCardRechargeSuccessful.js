// NPM imports
import React, {Component, Fragment} from 'react';
import {ScrollView, StyleSheet, Image, View, BackHandler} from 'react-native';
import {connect} from 'react-redux';

// VouD imports
import BrandText from '../../components/BrandText';
import SystemText from '../../components/SystemText';
import Icon from '../../components/Icon';
import Button from '../../components/Button';
import {backToHome, navigateFromHome, backToRoute} from '../../redux/nav';
import {fetchCardList} from '../../redux/transport-card';
import {colors, dimensions} from '../../styles';
import {routeNames} from '../../shared/route-names';
import {getProductTypeLabel} from '../../redux/financial';
import {formatCurrencyFromCents} from '../../utils/parsers-formaters';
import {hasNotch} from '../../utils/hasNotch';

const validator = require('../../images/validator-green.png');
const smartPurchaseCalendar = require('../../images/smart-purchase-calendar.png');

// Screen component
class TransportCardRechargeSuccessfulView extends Component {
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this._backHandler);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this._backHandler);
  }

  _backHandler = () => {
    this._finish();
    return true;
  };

  _finish = () => {
    const {
      dispatch,
      navigation: {
        state: {
          params: {smartPurchaseFlow},
        },
      },
    } = this.props;
    dispatch(fetchCardList());

    if (smartPurchaseFlow) {
      dispatch(backToRoute(routeNames.SMART_PURCHASE));
    } else {
      dispatch(backToHome());
    }
  };

  _goToServicePoints = () => {
    const {dispatch} = this.props;
    dispatch(navigateFromHome(routeNames.SERVICE_POINTS));
  };

  _getPurchaseValueText = () => {
    const {
      navigation: {
        state: {
          params: {paymentData},
        },
      },
    } = this.props;
    const purchaseValue =
      paymentData && paymentData.purchaseValue ? paymentData.purchaseValue : 0;
    return `R$ ${formatCurrencyFromCents(purchaseValue)}`;
  };

  _hasScheduledDay = () => {
    const {
      navigation: {
        state: {
          params: {paymentData},
        },
      },
    } = this.props;
    return paymentData && paymentData.scheduledDay ? true : false;
  };

  _renderPurchaseImage = () => {
    if (this._hasScheduledDay())
      return (
        <Image
          style={styles.smartPurchaseCalendarImage}
          source={smartPurchaseCalendar}
        />
      );

    return <Image style={styles.validatorImage} source={validator} />;
  };

  _getPurchaseOrderText = () => {
    const {
      navigation: {
        state: {
          params: {paymentData},
        },
      },
    } = this.props;

    const rechargeValue =
      paymentData && paymentData.rechargeValue ? paymentData.rechargeValue : 0;
    return `R$ ${formatCurrencyFromCents(rechargeValue)}`;
  };

  _getCardNameText = () => {
    const {
      navigation: {
        state: {
          params: {paymentData},
        },
      },
    } = this.props;
    const transportCardNick =
      paymentData && paymentData.transportCardNick
        ? paymentData.transportCardNick
        : '';
    const productType = paymentData && paymentData.productType;
    return `cartão ${getProductTypeLabel(productType)} ${transportCardNick}`;
  };

  _renderPurchaseValidationText = () => {
    const {
      navigation: {
        state: {
          params: {smartPurchaseFlow},
        },
      },
    } = this.props;
    if (this._hasScheduledDay()) {
      const {
        navigation: {
          state: {
            params: {paymentData},
          },
        },
      } = this.props;
      const scheduledDay =
        paymentData && paymentData.scheduledDay ? paymentData.scheduledDay : 0;

      return (
        <SystemText style={styles.purchaseValidationText}>
          {smartPurchaseFlow ? (
            <Fragment>
              <SystemText>{'Mensalmente todo o dia'}</SystemText>
              <SystemText
                style={styles.fontBold}>{` ${scheduledDay} `}</SystemText>
            </Fragment>
          ) : (
            <Fragment>
              <SystemText>
                {'Pedido realizado com sucesso. E a partir do próximo dia'}
              </SystemText>
              <SystemText
                style={styles.fontBold}>{` ${scheduledDay}`}</SystemText>
              <SystemText>{', mensalmente, nós '}</SystemText>
            </Fragment>
          )}
          <SystemText>{'efetuaremos um pedido de compra de '}</SystemText>
          <SystemText
            style={
              styles.fontBold
            }>{`${this._getPurchaseOrderText()} `}</SystemText>
          <SystemText>{'para o '}</SystemText>
          <SystemText style={styles.fontBold}>
            {this._getCardNameText()}
          </SystemText>
          <SystemText>{`. Não se esqueça de validar os seus créditos nos ${
            smartPurchaseFlow ? '' : 'pontos de recarga'
          }`}</SystemText>
          {smartPurchaseFlow && (
            <SystemText
              style={styles.servicePointsText}
              onPress={this._goToServicePoints}>
              pontos de recarga
            </SystemText>
          )}
          <SystemText>{' após a confirmação do pagamento.'}</SystemText>
        </SystemText>
      );
    }
    return (
      <SystemText style={styles.purchaseValidationText}>
        <SystemText>{`Seu pedido de recarga de `}</SystemText>
        <SystemText style={styles.fontBold}>
          {this._getPurchaseOrderText()}
        </SystemText>
        <SystemText>{` está sendo processado para o `}</SystemText>
        <SystemText style={styles.fontBold}>
          {this._getCardNameText()}
        </SystemText>
        <SystemText>{`. Após a confirmação do pagamento, você poderá validar seus créditos nos `}</SystemText>
        <SystemText
          style={styles.servicePointsText}
          onPress={this._goToServicePoints}>
          pontos de recarga
        </SystemText>
        <SystemText>{` dos terminais ou em até 3 dias nos validadores dos ônibus municipais.`}</SystemText>
      </SystemText>
    );
  };

  _getSuccessTitle = () => {
    const {
      navigation: {
        state: {
          params: {smartPurchaseFlow},
        },
      },
    } = this.props;

    if (smartPurchaseFlow) {
      return 'A sua compra programada foi agendada com sucesso!';
    }

    if (this._hasScheduledDay()) {
      return 'Pedido e programação de compra realizados com sucesso!';
    }

    return 'Pedido realizado\ncom sucesso!';
  };

  render() {
    const {
      navigation: {
        state: {
          params: {smartPurchaseFlow},
        },
      },
    } = this.props;

    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <Icon style={styles.closeIcon} name="close" onPress={this._finish} />
          <Icon style={styles.successIcon} name="checkmark-circle-outline" />
          <SystemText style={styles.purchaseValue}>
            {this._getPurchaseValueText()}
          </SystemText>
          <BrandText
            style={
              this._hasScheduledDay()
                ? styles.successTitleSmartPurchase
                : styles.successTitle
            }>
            {this._getSuccessTitle()}
          </BrandText>
        </View>
        <View style={styles.purchaseImageContainer}>
          {this._renderPurchaseImage()}
        </View>
        <ScrollView
          style={styles.scrollView}
          contentContainerStyle={styles.scrollViewContainer}>
          {this._renderPurchaseValidationText()}
        </ScrollView>
        <View style={styles.actionBtnContainer}>
          {smartPurchaseFlow ? (
            <Button onPress={this._finish}>OK</Button>
          ) : (
            <Button onPress={this._goToServicePoints}>
              Ver pontos de recarga
            </Button>
          )}
        </View>
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerContainer: {
    paddingTop: (hasNotch() ? dimensions.notchSpace.top : 0) + 48,
    backgroundColor: colors.BRAND_SUCCESS,
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  scrollView: {
    flex: 1,
  },
  scrollViewContainer: {
    flexGrow: 1,
    padding: 16,
  },
  closeIcon: {
    color: 'white',
    width: 24,
    fontSize: 24,
    position: 'absolute',
    top: 40,
    left: 16,
  },
  successIcon: {
    color: 'white',
    width: 72,
    fontSize: 72,
    marginBottom: 16,
  },
  purchaseValue: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 32,
    lineHeight: 32,
    marginBottom: 8,
  },
  purchaseValueNormal: {
    fontWeight: 'normal',
  },
  successTitle: {
    color: 'white',
    fontSize: 20,
    lineHeight: 28,
    textAlign: 'center',
    marginBottom: 87,
  },
  successTitleSmartPurchase: {
    color: 'white',
    fontSize: 20,
    lineHeight: 28,
    textAlign: 'center',
    marginBottom: 80,
  },
  purchaseValidationText: {
    color: colors.GRAY,
    fontSize: 16,
    lineHeight: 24,
  },
  fontBold: {
    fontWeight: 'bold',
  },
  purchaseImageContainer: {
    alignItems: 'center',
  },
  validatorImage: {
    marginTop: -63,
    width: 114,
    height: 126,
  },
  smartPurchaseCalendarImage: {
    marginTop: -56,
    width: 112,
    height: 112,
  },
  actionBtnContainer: {
    padding: 16,
  },
  servicePointsText: {
    color: colors.BRAND_SECONDARY,
    fontWeight: 'bold',
  },
});

export const TransportCardRechargeSuccessful = connect()(
  TransportCardRechargeSuccessfulView,
);
