/* eslint-disable no-undef */
import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { mount } from 'enzyme';
import { change } from 'redux-form';
import toJson from 'enzyme-to-json';

import { AddPaymentMethod } from '..';
import { reduxFormName } from '../AddPaymentMethodForm';
import AppReducer from '../../../../redux';


describe('AddPaymentMethod integration', () => {
  const origConsole = console.error;
  const store = createStore(AppReducer);
  const navigation = {
    state: {
      params: {}
    }
  };

  beforeEach(() => {
    console.error = () => {};
  });
  afterEach(() => {
    console.error = origConsole;
  });

  it('should render correctly', async () => {
    const wrapper = mount(
      <Provider store={store}>
        <AddPaymentMethod
          navigation={navigation}
        />
      </Provider>
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should render validation error messages if fields are invalid', async () => {
    const wrapper = mount(
      <Provider store={store}>
        <AddPaymentMethod
          navigation={navigation}
        />
      </Provider>
    );

    // Simulate invalid fields
    store.dispatch(change(reduxFormName, 'creditCardNumber', '4111111111111113'));
    store.dispatch(change(reduxFormName, 'creditCardBrand', ''));
    store.dispatch(change(reduxFormName, 'creditCardHolder', ''));
    store.dispatch(change(reduxFormName, 'creditCardExpirationDate', '00/00'));
    store.dispatch(change(reduxFormName, 'creditCardSecurityCode', ''));

    expect(toJson(wrapper.mount())).toMatchSnapshot();
  });

});