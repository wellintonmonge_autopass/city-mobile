/* eslint-disable no-undef */
import React from 'react';
import configureMockStore from "redux-mock-store";
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import { AddPaymentMethodForm, AddPaymentMethodFormView } from '../AddPaymentMethodForm';
import { shallowUntilTarget, findButtonWithLabel } from '../../../../test-utils/enzyme-helper';
import { reduxFormName } from '../AddPaymentMethodForm';

describe('AddPaymentMethodForm', () => {

  // Initialize Redux Store
  const mockStore = configureMockStore();
  const store = mockStore({
    config: {},
  });

  afterAll(() => {
    store.clearActions();
  });

  it('should render the correct fields if save card unchecked and it\'s purchase flow', () => {
    const wrapper = shallowUntilTarget(
      <AddPaymentMethodForm 
        store={store}
        ui={{}}
        onSubmit={jest.fn()}
        purchaseFlow
      />,
      AddPaymentMethodFormView
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should render the correct fields if save card checked and it\'s purchase flow', () => {
    const storeWithSaveCardChecked = mockStore({
      config: {},
      form: {
        [reduxFormName]: {
          values: {
            saveCreditCard: true
          }
        }
      }
    });
    const wrapper = shallowUntilTarget(
      <AddPaymentMethodForm 
        store={storeWithSaveCardChecked}
        ui={{}}
        onSubmit={jest.fn()}
        purchaseFlow
      />,
      AddPaymentMethodFormView
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should render the correct fields and ghost transaction message if it isn\'t purchase flow', () => {
    const storeWithSaveCardChecked = mockStore({
      config: {},
      form: {
        [reduxFormName]: {
          values: {
            saveCreditCard: true
          }
        }
      }
    });
    const wrapper = shallowUntilTarget(
      <AddPaymentMethodForm 
        store={storeWithSaveCardChecked} 
        ui={{}}
        onSubmit={jest.fn()}
      />,
      AddPaymentMethodFormView
    );

    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should disable submit button if form is invalid or save request is fetching', () => {
    let wrapper = shallow(
      <AddPaymentMethodFormView 
        store={store}
        valid={false}
        ui={{}}
        onSubmit={jest.fn()} 
      />,
    );
    expect(toJson(wrapper)).toMatchSnapshot();

    wrapper = shallow(
      <AddPaymentMethodFormView 
        store={store}
        valid={true}
        ui={{
          isFetching: true
        }}
        onSubmit={jest.fn()} 
      />,
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should submit the form data when submit button is pressed', () => {
    const formData = { 
      creditCardBrand: 'visa',
      creditCardHolder: 'teste',
      creditCardNumber: '4111111111111111',
      creditCardExpirationDate: '12/20',
      creditCardSecurityCode: '123',
      saveCreditCard: true
    };
    const storeWithFormData = mockStore({
      config: {},
      form: {
        [reduxFormName]: {
          values: {
            ...formData
          }
        }
      }
    });
    const onSubmitMock = jest.fn();
    const wrapper = shallowUntilTarget(
      <AddPaymentMethodForm 
        store={storeWithFormData} 
        ui={{}}
        onSubmit={onSubmitMock}
      />,
      AddPaymentMethodFormView
    );

    const submitButton = findButtonWithLabel(wrapper, 'Salvar forma de pagamento');
    submitButton.props().onPress();

    // Test if form data is submitted
    expect(onSubmitMock).toHaveBeenCalledTimes(1);
    expect(onSubmitMock.mock.calls[0][0]).toMatchObject(formData);
  });

  it('should render a message box if save request returned a error', () => {
    const errorMessage = 'Test error';
    const wrapper = shallowUntilTarget(
      <AddPaymentMethodForm 
        store={store}
        ui={{
          error: errorMessage
        }}
        onSubmit={jest.fn()} 
      />,
      AddPaymentMethodFormView
    );
    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('should load form data if it\'s edit payment method flow', () => {
    const editFormData = {
      items: {
        cardFlag: 'visa',
        cardHolder: 'teste',
        cardNumber: '4111111111111111',
        expirationDate: '12/20',
        securityCode: '123',
      }
    };
    const formData = {
      creditCardBrand: editFormData.items.cardFlag,
      creditCardHolder: editFormData.items.cardHolder,
      creditCardNumber: editFormData.items.cardNumber,
      creditCardExpirationDate: editFormData.items.expirationDate,
      creditCardSecurityCode: editFormData.items.securityCode,
      saveCreditCard: true
    };
    const onSubmitMock = jest.fn();

    const wrapper = shallowUntilTarget(
      <AddPaymentMethodForm 
        store={store}
        ui={{}}
        onSubmit={onSubmitMock}
        editPaymentMethod={editFormData}
      />,
      AddPaymentMethodFormView
    );

    const submitButton = findButtonWithLabel(wrapper, 'Salvar forma de pagamento');
    submitButton.props().onPress();

    // Test if form data is submitted with the data to edit
    expect(onSubmitMock).toHaveBeenCalledTimes(1);
    expect(onSubmitMock.mock.calls[0][0]).toMatchObject(formData);
  });

});