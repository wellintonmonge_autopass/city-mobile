// NPM imports
import React, {Component, Fragment} from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';

// VouD imports
import Header, {headerActionTypes} from '../../components/Header';
import RequestFeedback from '../../components/RequestFeedback';
import PaymentMethodList from './PaymentMethodList';

import {openMenu} from '../../redux/menu';
import {showToast, toastStyles} from '../../redux/toast';

import {
  getSavedPaymentMethodsUI,
  getSavedPaymentList,
} from '../../redux/selectors';
import {routeNames} from '../../shared/route-names';

import {
  fetchSavedPaymentMethods,
  requestSavedPaymentMethodsClear,
  fetchRemovePaymentMethod,
  removePaymentMethodClear,
} from '../../redux/payment-method';
import {fetchSmartPurchases} from '../../redux/smart-purchase';
import {showRemovePaymentMethodAlert} from '../../utils/payment-method';
import {navigateToRoute} from '../../redux/nav';

// Screen component
class PaymentMethodsView extends Component {
  componentDidMount() {
    this._fetchPaymentMethods();
  }

  componentWillUnmount() {
    const {dispatch} = this.props;
    dispatch(requestSavedPaymentMethodsClear());
    dispatch(removePaymentMethodClear());
  }

  openMenu = () => {
    const {dispatch} = this.props;
    dispatch(openMenu());
  };

  _fetchPaymentMethods = () => {
    const {dispatch} = this.props;
    dispatch(fetchSavedPaymentMethods());
    dispatch(fetchSmartPurchases());
  };

  _goToAddPaymentMethod = () => {
    const {dispatch} = this.props;
    dispatch(navigateToRoute(routeNames.ADD_PAYMENT_METHOD));
  };

  _removePaymentMethod = id => {
    const {dispatch} = this.props;
    dispatch(fetchRemovePaymentMethod(id))
      .then(() => {
        dispatch(removePaymentMethodClear());
        dispatch(fetchSavedPaymentMethods());
        dispatch(showToast('Forma de pagamento removida com sucesso!'));
      })
      .catch(error => {
        dispatch(showToast(error.message, toastStyles.ERROR));
      });
  };

  _showRemoveAlert = paymentMethod => {
    showRemovePaymentMethodAlert(paymentMethod, () =>
      this._removePaymentMethod(paymentMethod.id),
    );
  };

  _renderMainContent = () => {
    const {
      list,
      ui: {error, isFetching},
      removingMethodId,
    } = this.props;

    return list && list[0] ? (
      <PaymentMethodList
        itemList={list}
        onRemove={this._showRemoveAlert}
        removingMethodId={removingMethodId}
      />
    ) : (
      <Fragment>
        <View style={styles.requestFeedbackContainer}>
          <RequestFeedback
            loadingMessage="Carregando formas de pagamento..."
            errorMessage={error}
            emptyMessage="Não foram encontradas formas de pagamento salvas"
            retryMessage="Tentar novamente"
            isFetching={isFetching}
            onRetry={this._fetchPaymentMethods}
          />
        </View>
      </Fragment>
    );
  };

  _renderContainerStyleSheet = () => {
    // If payment method list has 1+ credit cards,
    // remove flex: 1 from container to prevent active
    // discount from staying at the bottom of the screen.
    // If payment method list has 0 itens, add flex: 1
    // to move active discount to bottom + empty state
    const {list} = this.props;

    if (list.length === 0) return {flex: 1};

    return {};
  };

  render() {
    return (
      <View style={this._renderContainerStyleSheet()}>
        <Header
          title="Formas de pagamento"
          left={{
            type: headerActionTypes.MENU,
            onPress: this.openMenu,
          }}
          right={{
            type: headerActionTypes.CUSTOM,
            icon: 'add',
            onPress: this._goToAddPaymentMethod,
          }}
        />
        {this._renderMainContent()}
      </View>
    );
  }
}

// Styles
const styles = StyleSheet.create({
  requestFeedbackContainer: {
    flex: 1,
    justifyContent: 'center',
    padding: 24,
  },
  discountContainer: {
    padding: 16,
  },
});

function mapStateToProps(state) {
  return {
    ui: getSavedPaymentMethodsUI(state),
    list: getSavedPaymentList(state),
    removingMethodId: state.paymentMethod.remove.id,
  };
}

export const PaymentMethods = connect(mapStateToProps)(PaymentMethodsView);

export * from './AddPaymentMethod';
export * from './SelectPaymentMethod';
