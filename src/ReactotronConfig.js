// NPM imports
import Reactotron from 'reactotron-react-native';
import {reactotronRedux} from 'reactotron-redux';

// Reactotron config
Reactotron.configure({host: '192.168.88.61'}) // controls connection & communication settings
  .use(reactotronRedux())
  .useReactNative();

if (__DEV__) {
  Reactotron.connect();
  console.tron = Reactotron;
}
