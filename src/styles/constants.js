import {Dimensions} from 'react-native';
import CONFIG from '../../config/config.js';

export const colors = CONFIG.colors;

export const dimensions = {
  screen: Dimensions.get('window'),
  notchSpace: {
    top: 16,
    bottom: 16,
  },
};