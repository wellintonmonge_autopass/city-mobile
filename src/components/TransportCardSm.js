// NPM imports
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Image,
  Platform,
  StyleSheet,
  View,
} from 'react-native';

// VouD imports
import BrandText from './BrandText';
import SystemText from './SystemText';
import { colors } from '../styles/constants';
import { formatBomCardNumber } from '../utils/parsers-formaters';
import { getColorForLayoutType, getLogoSmForLayoutType } from '../utils/transport-card';

// Component
const propTypes = {
  cardName: PropTypes.string.isRequired,
  cardNumber: PropTypes.string.isRequired,
  layoutType: PropTypes.string.isRequired,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
};

const defaultProps = {
  style: {},
};

class TransportCardSm extends Component {
  render() {
    const { cardName, cardNumber, layoutType, style } = this.props;

    return (
      <View style={StyleSheet.flatten([styles.container, style])}>
        <Image
          style={styles.logo}
          source={getLogoSmForLayoutType(layoutType)}
        />
        <BrandText
          style={StyleSheet.flatten([styles.nameText, { color: getColorForLayoutType(layoutType) }])}
          numberOfLines={1}
          ellipsizeMode={'tail'}
        >
          {cardName}
        </BrandText>
        <SystemText style={styles.numberText}>
          {formatBomCardNumber(cardNumber)}
        </SystemText>
      </View>
    );
  }
}

TransportCardSm.propTypes = propTypes;
TransportCardSm.defaultProps = defaultProps;

// Styles
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'stretch',
    borderRadius: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowOffset: { width: 0, height: 4 },
    elevation: 8,
    padding: 16,
    backgroundColor: 'white',
    borderColor: Platform.OS === 'android' && Platform.Version < 21? colors.OVERLAY_LIGHT : 'transparent',
    borderWidth: Platform.OS === 'android' && Platform.Version < 21? 1 : 0,
  },
  logo: {
    width: 21,
    height: 5,
  },
  nameText: {
    flex: 1,
    marginHorizontal: 8,
    fontSize: 16,
    lineHeight: 20,
    fontWeight: 'bold',
  },
  numberText: {
    fontSize: 14,
    lineHeight: 20,
    fontWeight: 'bold',
    color: colors.GRAY_DARKER,
  },
});

export default TransportCardSm;
