// NPM imports
import React, { Component } from 'react';
import { StyleSheet, View, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';

// VouD imports
import TextInput from './TextInput';
import { colors } from '../styles';

// Component
const propTypes = {
  isPrimary: PropTypes.bool,
  textFieldRef: PropTypes.func,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  onPressOverlay: PropTypes.func,
};

const defaultProps = {
  isPrimary: false,
  textFieldRef: () => {},
  style: {},
  onPressOverlay: null,
};

class TextFieldNoRF extends Component {
  render() {
    const { isPrimary, textFieldRef, style, onPressOverlay, ...props } = this.props;

    const styleProps = isPrimary ? formPrimaryProps : formDefaultProps;
    
    return (
      <View style={style}>
        <TextInput
          {...props}
          {...styleProps}
          textFieldRef={textFieldRef}
        />
        {onPressOverlay && (
          <TouchableWithoutFeedback onPress={onPressOverlay}>
            <View style={styles.actionOverlay} />
          </TouchableWithoutFeedback>
        )}
      </View>
    );
  }
}

TextFieldNoRF.propTypes = propTypes;
TextFieldNoRF.defaultProps = defaultProps;

// Styles

const styles = StyleSheet.create({
  messageContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  messageIcon: {
    width: 16,
    marginRight: 8,
    fontSize: 16,
    textAlign: 'center',
    color: colors.GRAY,
    backgroundColor: 'transparent'
  },
  messageText: {
    flex: 1,
    fontSize: 12,
    color: colors.GRAY
  },
  actionOverlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0
  },
  // error modifier
  error: {
    color: colors.BRAND_ERROR
  },
  // light modifier
  light: {
    color: 'white'
  },
  // actionable modifier
  actionable: {
    color: colors.BRAND_PRIMARY_LIGHTER,
  },
});

const formDefaultProps = {
  textColor: colors.GRAY_DARKER,
  tintColor: colors.BRAND_PRIMARY_LIGHTER,
  baseColor: colors.GRAY
};

const formPrimaryProps = {
  textColor: 'white',
  tintColor: colors.BRAND_SECONDARY,
  baseColor: 'rgba(255,255,255,0.5)'
};

export default TextFieldNoRF;
