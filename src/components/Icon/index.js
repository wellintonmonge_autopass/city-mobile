// NPM imports
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';

// IcoMoon config
import icoMoonConfig from './selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig);

export default Icon;
