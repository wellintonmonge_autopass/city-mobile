// NPM imports
import React     from 'react';
import PropTypes from 'prop-types';
import {
    View,
    StyleSheet
} from 'react-native';

// group imports
import InfoListItem from './InfoListItem';

// component
const InfoList = ({ listTitle, itemList, renderItem, dense }) => {

    const getContainerStyle = () => {
        let containerStyles = [styles.container];
        if (dense) containerStyles.push(styles.denseContainer);
        return StyleSheet.flatten(containerStyles);
    }

    const renderList = () => {
        return itemList.map((item, i) => {
            return (
                <InfoListItem 
                    key={i}
                    itemContent={renderItem(item, i)}
                    dense={dense}/>
            )
        });
    };

    return (
        <View>
            <InfoListItem 
                itemContent={listTitle}
                isHeader
            />
            <View style={getContainerStyle()}>
                {renderList()}
            </View>
        </View>
    );
}

// prop types
InfoList.propTypes = {
    listTitle: PropTypes.string.isRequired,
    itemList: PropTypes.array.isRequired,
    renderItem: PropTypes.func.isRequired,
};

// styles
const styles = {
    container: {
		paddingTop: 8,
        paddingBottom: 16
    },
    denseContainer: {
		paddingTop: 4,
    }
};

export default InfoList;
