// NPM imports
import React, {Component} from 'react';
import {Animated, StyleSheet, View, Image} from 'react-native';

// VouD imports
import {colors} from '../../styles';
import {transportCardTypes} from '../../redux/transport-card';

// Component imports
import TransportCardHeader from './Header';
import TransportCardFooter from './Footer';
import CityPlusComumContent from './CityPlusComumContent';
import CityPlusEscolarContent from './CityPlusEscolarContent';
import CityPlusEscolarGratuidadeContent from './CityPlusEscolarGratuidadeContent';
import CityPlusVTContent from './CityPlusVTContent';

import Loader from '../Loader';
import VoudTouchableOpacity from '../TouchableOpacity';
import BlockTag from './BlockTag';
import {
  isTransportCardScholarNotRevalidated,
  getColorForLayoutType,
} from '../../utils/transport-card';

// const definitions
export const TRANSPORT_CARD_HEIGHT = 226;
export const TRANSPORT_CARD_COLLAPSED_HEIGHT = 128;

// component
const cityPlusComumBgImg = require('../../images/transport-cards/city-plus-comum-bg.png');
const cityPlusEscolarBgImg = require('../../images/transport-cards/city-plus-escolar-bg.png');
const cityPlusVtBgImg = require('../../images/transport-cards/city-plus-vt-bg.png');

const logo = require('../../images/logo.png');

class TransportCard extends Component {
  _renderContent = () => {
    const {data, collapse, small} = this.props;

    if (data.layoutType == transportCardTypes.CITY_PLUS_COMUM) {
      return (
        <CityPlusComumContent data={data} collapse={collapse} small={small} />
      );
    } else if (data.layoutType == transportCardTypes.CITY_PLUS_ESCOLAR) {
      return (
        <CityPlusEscolarContent data={data} collapse={collapse} small={small} />
      );
    } else if (
      data.layoutType == transportCardTypes.CITY_PLUS_ESCOLAR_GRATUIDADE
    ) {
      return (
        <CityPlusEscolarGratuidadeContent
          data={data}
          collapse={collapse}
          small={small}
        />
      );
    } else if (data.layoutType == transportCardTypes.CITY_PLUS_VT) {
      return (
        <CityPlusVTContent data={data} collapse={collapse} small={small} />
      );
    }

    return (
      <CityPlusComumContent data={data} collapse={collapse} small={small} />
    );
  };

  _renderFooter = () => {
    const {data, collapse, onHelp, small} = this.props;

    return (
      <TransportCardFooter
        data={data}
        collapse={collapse}
        onHelp={onHelp}
        small={small}
      />
    );
  };

  _renderBgImage = () => {
    const {data, collapse, small} = this.props;

    let bgImg;

    if (data.layoutType == transportCardTypes.CITY_PLUS_COMUM)
      bgImg = cityPlusComumBgImg;
    else if (
      data.layoutType == transportCardTypes.CITY_PLUS_ESCOLAR ||
      data.layoutType == transportCardTypes.CITY_PLUS_ESCOLAR_GRATUIDADE
    )
      bgImg = cityPlusEscolarBgImg;
    else if (data.layoutType == transportCardTypes.CITY_PLUS_VT)
      bgImg = cityPlusVtBgImg;
    else return null;

    return (
      <Animated.Image
        source={bgImg}
        resizeMode="stretch"
        style={StyleSheet.flatten([
          styles.bomBgImg,
          small && styles.bomBgImgSmall,
        ])}
      />
    );
  };

  _renderSpinner = () => {
    const {isUpdating, showSpinner, small} = this.props;

    if (!isUpdating) {
      return null;
    }

    return showSpinner ? (
      <View style={styles.loaderContainer}>
        <Loader
          text="Atualizando cartões..."
          style={StyleSheet.flatten([
            styles.loader,
            small ? styles.loaderSmall : {},
          ])}
          isLight
        />
      </View>
    ) : (
      <View style={styles.loaderContainer} />
    );
  };

  render() {
    const {data, onPress, collapse, style, small, onLayout} = this.props;

    const cardHeight = collapse
      ? collapse.interpolate({
          inputRange: [0, 1],
          outputRange: [TRANSPORT_CARD_HEIGHT, TRANSPORT_CARD_COLLAPSED_HEIGHT],
        })
      : TRANSPORT_CARD_HEIGHT;
    // no data
    if (!data)
      return (
        <Animated.View
          onLayout={onLayout}
          style={StyleSheet.flatten([
            styles.container,
            {height: cardHeight},
            style,
          ])}
        />
      );

    const showBlockTag = isTransportCardScholarNotRevalidated(data);
    const CardContainer =
      onPress && !showBlockTag ? VoudTouchableOpacity : View;

    // Card with action
    return (
      <Animated.View
        onLayout={onLayout}
        style={StyleSheet.flatten([
          styles.container,
          {
            height: cardHeight,
            borderColor: getColorForLayoutType(data.layoutType),
          },
          style,
        ])}>
        <CardContainer
          onPress={onPress}
          style={StyleSheet.flatten([
            styles.cardContainer,
            showBlockTag ? styles.blockedStyle : {},
          ])}>
          <TransportCardHeader small={small} data={data} />
          <View style={styles.main}>
            {this._renderBgImage()}
            {this._renderContent()}
            {this._renderFooter()}
          </View>
        </CardContainer>
        {showBlockTag && <BlockTag style={styles.blockTag} cardData={data} />}
        {this._renderSpinner()}
      </Animated.View>
    );
  }
}

// styles
const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    borderRadius: 4,
    shadowColor: 'black',
    shadowOpacity: 0.25,
    shadowRadius: 8,
    shadowOffset: {width: 0, height: 4},
    elevation: 8,
    backgroundColor: 'white',
    borderColor: colors.BRAND_PRIMARY,
    borderWidth: 1,
  },
  cardContainer: {
    flex: 1,
  },
  main: {
    flex: 1,
    padding: 3,
    
  },
  bomBgImgSmall: {
    position: 'absolute',
    right: 0,
    top: -8,
    left: 0,
    width: null,
    borderRadius: 4,
  },
  bomBgImg: {
    position: 'absolute',
    right: 0,
    top: -14,
    left: 0,
    width: null,
    borderRadius: 4,
  },
  buBgImg: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    width: null,
    height: null,
    borderRadius: 4,
  },
  loaderContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    borderRadius: 4,
    backgroundColor: colors.OVERLAY,
  },
  loader: {
    top: 40,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  loaderSmall: {
    height: 110,
  },
  blockedStyle: {
    opacity: 0.3,
  },
  blockTag: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 40,
    width: '100%',
  },
  logo: {
    width: 48,
    marginLeft: 8,
  },
});

export default TransportCard;
