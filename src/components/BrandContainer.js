// NPM imports
import React from 'react';
import {Image, StyleSheet, View, KeyboardAvoidingView} from 'react-native';

// VouD imports
import {hasNotch} from '../utils/hasNotch';
import {dimensions} from '../styles/constants';
import {ScrollView} from 'react-native-gesture-handler';

// image
const bgImg = require('../images/bg.png');

// component
const BrandContainer = ({bottomPos, style, children}) => {
  return (
    <View style={styles.container}>
      <Image
        source={bgImg}
        style={StyleSheet.flatten([styles.brandBg, {bottom: bottomPos || 0}])}
        resizeMode="stretch"
      />
      <ScrollView
        style={StyleSheet.flatten([styles.children, style])}
        scrollEnabled={true}>
        {children}
      </ScrollView>
    </View>
  );
};

// styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'flex-end',
    backgroundColor: '#FFFFFF',
  },
  contentContainer: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'flex-end',
  },
  brandBg: {
    width: null,
  },
  children: {
    paddingTop: hasNotch() ? dimensions.notchSpace.top : 0,
    paddingBottom: hasNotch() ? dimensions.notchSpace.bottom : 0,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});

export default BrandContainer;
