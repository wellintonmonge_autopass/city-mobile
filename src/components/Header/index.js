// NPM imports
import React from 'react';
import {connect} from 'react-redux';
import {Platform} from 'react-native';

// VouD imports
import HeaderIOS from './HeaderIOS';
import HeaderMD from './HeaderMD';
import {getHasProfileAlerts} from '../../redux/selectors';

// Action types
export const headerActionTypes = {
  BACK: 'voud/headerActions/BACK',
  MENU: 'voud/headerActions/MENU',
  CLOSE: 'voud/headerActions/CLOSE',
  MORE: 'voud/headerActions/MORE',
  EDIT: 'voud/headerActions/EDIT',
  SEARCH: 'voud/headerActions/SEARCH',
  HELP: 'voud/headerActions/HELP',
  CUSTOM: 'voud/headerActions/CUSTOM',
};

// Component
const Header = ({
  title,
  hasAlert,
  left,
  right,
  borderless,
  withLogo,
  renderExtension,
  barStyle,
  backTextWhite,
}) => {
  return Platform.OS === 'ios' ? (
    <HeaderIOS
      withLogo={withLogo}
      title={title}
      hasAlert={hasAlert}
      left={left}
      right={right}
      borderless={borderless}
      backTextWhite={backTextWhite}
      renderExtension={renderExtension}
      barStyle={barStyle}
    />
  ) : (
    <HeaderMD
      withLogo={withLogo}
      title={title}
      hasAlert={hasAlert}
      left={left}
      right={right}
      borderless={borderless}
      renderExtension={renderExtension}
      barStyle={barStyle}
    />
  );
};

// Redux
const mapStateToProps = state => {
  return {
    hasAlert: getHasProfileAlerts(state),
  };
};

export default connect(mapStateToProps)(Header);
